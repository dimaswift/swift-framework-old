﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using UnityEngine.Networking;
using System;

namespace SwiftFramework.Network
{
    public class NetworkManager : Module, INetworkManager
    {
        public IPromise<string> Get(string url, int timeoutSeconds = 5)
        {
            Promise<string> result = Promise<string>.Create();

            UnityWebRequest request = UnityWebRequest.Get(url);

            request.timeout = timeoutSeconds;

            var operation = request.SendWebRequest();

            operation.completed += response => 
            {
                if (request.isHttpError || request.isNetworkError)
                {
                    result.Reject(new Exception(request.error));
                }
                else
                {
                    if (string.IsNullOrEmpty(request.downloadHandler.text))
                    {
                        result.Reject(new Exception("Empty response"));
                    }
                    else
                    {
                        result.Resolve(request.downloadHandler.text);
                    }
                }
            };

            return result;
        }

    }

}
