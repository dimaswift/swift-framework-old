﻿using UnityEngine;
using SwiftFramework.Core;
using System;

namespace SwiftFramework.LocalCacheManager
{
    public class JsonSerializator : Module, ISerializator
    {
        public T Deserialize<T>()
        {
            return (T)Deserialize(typeof(T));
        }

        public void Serialize<T>(T data, bool overwrite = true)
        {
            string key = typeof(T).Name;
            if (overwrite == false && PlayerPrefs.HasKey(key))
            {
                Debug.LogError($"Cannot serialize object of type <b>{key}</b>. Already exists!");
                return;
            }
            string json = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(key, json);
            PlayerPrefs.Save();
        }

        public bool Exists<T>()
        {
            return PlayerPrefs.HasKey(typeof(T).Name);
        }

        public void Delete<T>()
        {
            PlayerPrefs.DeleteKey(typeof(T).Name);
        }

        public object Deserialize(Type type)
        {
            object result = default;
            string key = type.Name;

            if (PlayerPrefs.HasKey(key))
            {
                try
                {
                    string json = PlayerPrefs.GetString(key);
                    result = JsonUtility.FromJson(json, type);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Cannot deserialize object of type <b>{key}</b>: {e.Message}");
                }
            }
            return result;

        }
    }
}
