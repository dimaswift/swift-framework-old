﻿using NUnit.Framework;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Tests
{
    public class LinksTest 
    {
        [Test]
        public void Run()
        {
            var test = Resources.Load<TestLinkConfig>("TestLinks/TestLinkConfig");

            var json = JsonUtility.ToJson(test.validLink);

            var linkFromJson = JsonUtility.FromJson<Link>(json);

            Assert.AreEqual(typeof(TestLinkConfig), linkFromJson.ValueType);
          
            Assert.IsTrue(test.validLink == linkFromJson);

            TestLinkConfig value = test.validLink.Value;

            Assert.IsTrue(test.validLink == value);

            Link linkToConfig = test.validLink;

            Link linkToConfigOther = test.validLinkOther;

            Link linkToConfigSame = test.sameValidLink;

            Assert.NotNull(test);

            Assert.IsTrue(test.emptyLink.IsEmpty);

            Assert.IsFalse(test.emptyLink.HasValue);

            Assert.IsTrue(test.validLink.HasValue);

            Assert.IsFalse(test.validLink.IsEmpty);

            Assert.IsFalse(test.validLink == test.emptyLink);

            Assert.IsTrue(test.sameValidLink == test.validLink);

            Assert.IsFalse(test.sameValidLink != test.validLink);

            Assert.IsTrue(test.validLink.Equals(test.sameValidLink));

            Assert.IsFalse(linkToConfig == linkToConfigOther);

            Assert.IsTrue(linkToConfigSame == linkToConfig);


            if (test.validLink.HasValue)
            {
                Assert.NotNull(test.validLink.Value);
            }
            else
            {
                Assert.IsNull(test.validLink.Value);
            }
        }
    }
}
