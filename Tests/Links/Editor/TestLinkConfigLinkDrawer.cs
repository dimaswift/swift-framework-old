using UnityEditor;
using SwiftFramework.Core.Editor;

namespace SwiftFramework.Tests.Editor
{
    [CustomPropertyDrawer(typeof(TestLinkConfigLink), true)]
    public class TestLinkConfigLinkDrawer : LinkPropertyDrawer<TestLinkConfig, TestLinkConfigLink> { }

    [CustomPropertyDrawer(typeof(TestLinkConfigOtherLink))]
    public class TestLinkConfigOtherLinkDrawer : LinkPropertyDrawer<TestLinkConfigOther, TestLinkConfigOtherLink> { }
}
 