﻿using UnityEngine;


namespace SwiftFramework.Tests
{
    [CreateAssetMenu(menuName = "SwiftFramework/Tests/Links/TestLinkConfig")]
    public class TestLinkConfig : ScriptableObject
    {
        public TestLinkConfigLink[] linkArray;
        public TestLinkConfigLink validLink;
        public TestLinkConfigOtherLink validLinkOther;
        public TestLinkConfigLink emptyLink;
        public TestLinkConfigLink sameValidLink;

    }
}