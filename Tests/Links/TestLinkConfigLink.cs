using UnityEngine;
using SwiftFramework.Core;
using System;

namespace SwiftFramework.Tests
{
    [Serializable] public class TestLinkConfigLink : LinkTo<TestLinkConfig> { }

    [Serializable] public class TestLinkConfigOtherLink : LinkTo<TestLinkConfigOther> { }
}
