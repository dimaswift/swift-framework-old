﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using SwiftFramework.Core;

namespace SwiftFramework.Tests
{
    public class PromisesTest
    {
        [Test]
        public void Run()
        {
            bool value1 = false;
            bool value2 = false;

            int intValue1 = 0;
            int intValue2 = 0;

            Promise pooledPromise = null;

            using (Promise testPromise = Promise.Create())
            {
                testPromise.Done(() => value1 = true);
                testPromise.Resolve();
                pooledPromise = testPromise;
            }

            using (Promise testPromise = Promise.Create())
            {
                testPromise.Done(() => value2 = true);
                testPromise.Resolve();
                Assert.AreEqual(pooledPromise, testPromise);
            }

            Assert.True(value2);
            Assert.True(value1);

            using (Promise<int> testPromise = Promise<int>.Create())
            {
                testPromise.Done((i) => intValue1 = i);
                testPromise.Resolve(1);
            }

            using (Promise<int> testPromise = Promise<int>.Create())
            {
                testPromise.Done((i) => intValue2 = i);
                testPromise.Resolve(2);
            }

            Assert.True(value2);
            Assert.True(value1);

            Assert.AreEqual(intValue1, 1);
            Assert.AreEqual(intValue2, 2);
        }


    }
}
