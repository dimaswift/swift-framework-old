﻿using NUnit.Framework;
using SwiftFramework.Core;
using SwiftFramework.LocalCacheManager;

namespace SwiftFramework.Tests
{
    public class JsonSerializatorTest : ModuleTest<ISerializator, JsonSerializator>
    {
        [Test]
        public void RunJsonSerializator()
        {
            var data = new TestData(); 
            data.Field = "Test";
            Begin().Done(m => 
            {
                Assert.NotNull(m);
                m.Serialize(data);

                Assert.AreEqual(data.Field, m.Deserialize<TestData>().Field);

                m.Delete<TestData>();

                Assert.IsNull(m.Deserialize<TestData>());

                Assert.IsFalse(m.Exists<TestData>());

                m.Serialize(data);

                Assert.IsTrue(m.Exists<TestData>());

                Assert.IsNotNull(m.Deserialize<TestData>());
            });
        }

        [System.Serializable]
        public class TestData
        {
            public string Field;
        }
    }
}
