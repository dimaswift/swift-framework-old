﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.Tests
{
    public class NetworkManagerTest : ModuleTest<INetworkManager, Network.NetworkManager>
    {
        [UnityTest]
        public IEnumerator Run()
        {
            INetworkManager manager = null;
            Begin().Done(m =>
            {
                manager = m;
            });

            while(manager == null)
            {
                yield return null;
            }

            manager.Get("https://jsonplaceholder.typicode.com/todos/1").Done(res => 
            {
                Assert.NotNull(res);
                ToDo toDo = JsonUtility.FromJson<ToDo>(res);
                Assert.NotNull(toDo);

                Assert.AreEqual(1, toDo.userId);
                Assert.AreEqual(false, toDo.completed);
                Assert.AreEqual(1, toDo.id);
            });
        }



        [System.Serializable]
        public class ToDo
        {
            public int userId;
            public int id;
            public string title;
            public bool completed;
        }
    
    }
}
