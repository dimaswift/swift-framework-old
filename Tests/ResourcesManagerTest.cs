﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using SwiftFramework.Core;

namespace SwiftFramework.Tests
{
    public class ResourcesManagerTest : ModuleTest<IResourcesManager, ResourcesManager.ResourcesManager>
    {
        [UnityTest]
        public IEnumerator ResourcesManagerTestWithEnumeratorPasses()
        {
            IResourcesManager manager = null;

            const string testPrefab = "TestPrefab";
            Begin().Done(m => 
            {
                manager = m;
            });

            while (manager == null)
                yield return null;

            Assert.True(manager.Exists<GameObject>(testPrefab));

            Assert.NotNull(manager.Load<GameObject>(testPrefab));

            List<GameObject> prefabs = new List<GameObject>();

            manager.LoadAll(testPrefab, prefabs);

            Assert.GreaterOrEqual(prefabs.Count, 1);

            IPromise<GameObject> promise = manager.LoadAsync<GameObject>(testPrefab);

            promise.Done(p => Assert.NotNull(p));

            yield return null;
        }
    }
}
