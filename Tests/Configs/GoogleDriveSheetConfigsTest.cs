﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using SwiftFramework.Core;
using UnityEngine;
using UnityEngine.TestTools;

namespace SwiftFramework.Tests
{
    public class GoogleDriveSheetConfigsTest : ModuleTest<ICloudConfigManager, GoogleSheetConfigs.GoogleSheetConfigManager>
    {
        protected override IEnumerable<(Type interfaceType, Type implementationType)> GetDependencies()
        {
            yield return (typeof(IResourcesManager), typeof(ResourcesManager.ResourcesManager));
        }

        [UnityTest]
        public IEnumerator RunGoogleDriveSheetConfigsTest()
        {
            ExampleGoogleSheetConfig config = null;

            bool loaded = false;

            Begin().Done(m =>
            {
                m.LoadConfig<ExampleGoogleSheetConfig>().Then(c =>
                {
                    config = c;
                    loaded = true;
                })
                .Catch(e =>
                {
                    Debug.LogException(e);
                    loaded = true;
                });
            });

            while (loaded == false)
                yield return null;
            Assert.NotNull(config);
            Assert.AreEqual(config.Test, "hello world");

            yield return null;
        }
    }
}
