﻿using UnityEngine;
using SwiftFramework.GoogleSheetConfigs;

namespace SwiftFramework.Tests
{
    [CreateAssetMenu(menuName = "SwiftFramework/Tests/Configs/TestGoogleSheetConfigList")]
    public class TestGoogleSheetConfigList : ConfigList<ExampleGoogleSheetConfigList>
    {
       
    }

    [System.Serializable]
    public class ExampleGoogleSheetConfigList
    {
        public string Name;
        public string Value;
    }
}
