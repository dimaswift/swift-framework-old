﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.LocalConfigs;

namespace SwiftFramework.Tests
{
    [System.Serializable]
    public class ExampleLocalConfig
    {
        public string hello = "default hi";
    }

    [CreateAssetMenu(menuName = "SwiftFramework/Tests/Configs/TestConfig")]
    public class TestLocalConfig : LocalConfig<ExampleLocalConfig>
    {

    }

}
