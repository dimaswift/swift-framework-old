﻿using UnityEngine;
using SwiftFramework.GoogleSheetConfigs;

namespace SwiftFramework.Tests
{
    [CreateAssetMenu(menuName = "SwiftFramework/Tests/Configs/TestGoogleSheetConfigDict")]
    public class TestGoogleSheetConfigDict : ConfigDictionary<ExampleGoogleSheetConfigDict>
    {
       
    }

    [System.Serializable]
    public class ExampleGoogleSheetConfigDict : IUniqueItem
    {
        public string Id => id;
        public string id;
        public string Value;
    }
}
