﻿using UnityEngine;
using SwiftFramework.GoogleSheetConfigs;

namespace SwiftFramework.Tests
{
    [CreateAssetMenu(menuName = "SwiftFramework/Tests/Configs/TestGoogleSheetConfig")]
    public class TestGoogleSheetConfig : Config<ExampleGoogleSheetConfig>
    {
       
    }

    [System.Serializable]
    public class ExampleGoogleSheetConfig
    {
        public string Test;
    }
}
