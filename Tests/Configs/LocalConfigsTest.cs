﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using SwiftFramework.Core;
using SwiftFramework.LocalConfigs;

namespace SwiftFramework.Tests
{
    public class LocalConfigsTest : ModuleTest<IConfigManager, LocalConfigManager>
    {
        protected override IEnumerable<(Type interfaceType, Type implementationType)> GetDependencies()
        {
            yield return (typeof(IResourcesManager), typeof(ResourcesManager.ResourcesManager));
        }

        [Test]
        public void RunLocalConfigTest()
        {
            Begin().Done(m =>
            {
                var localConfig = m.GetPureConfig<ExampleLocalConfig>();
                Assert.NotNull(localConfig);
            });
        }
    }
}
