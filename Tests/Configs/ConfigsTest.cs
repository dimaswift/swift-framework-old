﻿using System;
using System.Collections.Generic;
using SwiftFramework.Core;

namespace SwiftFramework.Tests
{
    public abstract class ModuleTest<I, M> where I : IModule
    {
        private readonly RuntimeModuleFactory factory = new RuntimeModuleFactory((typeof(I), typeof(M)));

        protected virtual IEnumerable<(Type interfaceType, Type implementationType)> GetDependencies()
        {
            yield break;
        }

        public Promise<I> Begin()
        {
            foreach (var dep in GetDependencies())
            {
                factory.Add(dep);
            }

            Promise<I> result = Promise<I>.Create();
            TestApp.Unload();
            TestApp.Create(factory).Done(app =>
            {
                result.Resolve(app.GetModule<I>());
            });

            return result;
        }

    }
}
