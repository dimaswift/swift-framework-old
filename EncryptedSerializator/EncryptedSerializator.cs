﻿using UnityEngine;
using SwiftFramework.Core;
using System;

namespace SwiftFramework.EncryptedSerializator
{
    public class EncryptedSerializator : Module, ISerializator
    {
        public T Deserialize<T>()
        {
            return (T)Deserialize(typeof(T));
        }

        public void Serialize<T>(T data, bool overwrite = true)
        {
            string key = typeof(T).Name;
            if (overwrite == false && App.SecurePrefs.Exists(key))
            {
                Debug.LogError($"Cannot serialize object of type <b>{key}</b>. Already exists!");
                return;
            }
            string json = JsonUtility.ToJson(data);
            App.SecurePrefs.SetValue(key, json);
            App.SecurePrefs.Save();
        }

        public bool Exists<T>()
        {
            return App.SecurePrefs.Exists(typeof(T).Name);
        }

        public void Delete<T>()
        {
            App.SecurePrefs.Delete(typeof(T).Name);
        }

        public object Deserialize(Type type)
        {
            object result = default;
            string key = type.Name;

            if (App.SecurePrefs.Exists(key))
            {
                try
                {
                    string json = App.SecurePrefs.GetValue(key);
                    result = JsonUtility.FromJson(json, type);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Cannot deserialize object of type <b>{key}</b>: {e.Message}");
                }
            }
            return result;

        }
    }
}
