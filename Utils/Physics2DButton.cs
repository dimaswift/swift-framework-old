﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils
{
    public class Physics2DButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private UnityEvent onClick = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            onClick.Invoke();
        }

        public void AddListener(UnityAction action)
        {
            onClick.AddListener(action);
        }

        public void RemoveListener(UnityAction action)
        {
            onClick.RemoveListener(action);
        }

        public void RemoveAllListeners()
        {
            onClick.RemoveAllListeners();
        }
    }

}
