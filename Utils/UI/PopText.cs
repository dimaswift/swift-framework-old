﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using System.Collections;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils
{
    public class PopText : MonoBehaviour, IView
    {
        [SerializeField] private TMP_Text text = null;
        [SerializeField] private Gradient gradient = new Gradient() {  };
        [SerializeField] private AnimationCurve flyUpCurve = new AnimationCurve();
        [SerializeField] private AnimationCurve scaleCurve = new AnimationCurve();
        [SerializeField] private float duration = 1;
        [SerializeField] private float speed = 1;

        public int InitialPoolCapacity => 100;

        public bool Active { get; set; }

        private IPool pool;

        public void Init(IPool pool)
        {
            this.pool = pool;
        }

        public void Process(float deltaTime)
        {
           
        }

        public void ReturnToPool()
        {
            gameObject.SetActive(false);
            pool.Return(this);
        }

        public void SetUp(IViewFactory viewFactory)
        {
            
        }

        public void Show(string txt, Vector3 point, Quaternion rotation)
        {
            gameObject.SetActive(true);
            text.text = txt;
            transform.position = point;
            transform.rotation = rotation;
            StartCoroutine(AnimationRoutine(duration, speed));
        }

        public void TakeFromPool()
        {
            
        }

        private IEnumerator AnimationRoutine(float duration, float speed)
        {
            float time = 0f;
            float startYPos = transform.position.y;
            Vector3 pos = transform.position;
            while (time < duration)
            {
                time += Time.deltaTime;
                float v = time / duration;
                pos.y = Mathf.Lerp(startYPos, startYPos + speed, flyUpCurve.Evaluate(v));
                text.color = gradient.Evaluate(v);
                transform.localScale = Vector3.one * scaleCurve.Evaluate(v);
                transform.position = pos;
                yield return null;
            }
            ReturnToPool();
        }

        public GameObject GetRoot()
        {
            return gameObject;
        }
    }

}
