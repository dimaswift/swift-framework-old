﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class ElementSet : MonoBehaviour
    {
        [SerializeField] private Element element = null;

        private void OnValidate()
        {
            if(element == null && transform.childCount > 0)
            {
                element = transform.GetChild(0).GetComponent<Element>();
            }
        }

        private readonly List<Element> elements = new List<Element>();

        public void SetUp<E, T>(IEnumerable<T> objects, Action<T, E> initMethod) where E : Element
        {
            if (IsElementOfType<E>() == false)
            {
                return;
            }
            if (elements.Count == 0)
            {
                elements.Add(element);
            }
            Clear();
            int i = 0;
            foreach (var o in objects)
            {
                E e;
                if (i < elements.Count)
                {
                    e = elements[i] as E;
                }
                else
                {
                    e = Instantiate(element, transform).GetComponent<E>();
                    e.Init();
                    elements.Add(e);
                }
                e.Show();
                initMethod(o, e);
                i++;
            }
        }

        private bool IsElementOfType<E>()
        {
            if (element == null)
            {
                Debug.LogError($"Cannot init ElementSet <b>{name}</b>, element source is null!");
                return false;
            }

            if (element is E == false)
            {
                Debug.LogError($"Cannot init ElementSet <b>{name}</b> for type <b>{typeof(E).Name}</b>!  <b>{element.name}</b> does not have this component attached!");
                return false;
            }
            return true;
        }

        public void SetUp<E, T>(IEnumerable<T> objects) where E : ElementFor<T>
        {
            SetUp<E, T>(objects, (o, e) => e.SetUp(o));
        }

        public void SetUp<E, T>(IEnumerable<T> objects, Action<ElementFor<T>> onClick) where E : ElementFor<T>
        {
            SetUp<E, T>(objects, (o, e) => e.SetUp(o));
            foreach (E e in elements)
            {
                e.OnClick -= onClick;
                e.OnClick += onClick;
            }
        }

        public void Clear()
        {
            foreach (var e in elements)
            {
                e.gameObject.SetActive(false);
                e.Hide();
            }
        }
    }
}
