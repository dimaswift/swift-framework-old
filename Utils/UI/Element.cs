﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SwiftFramework.Utils.UI
{
    public class Element : MonoBehaviour
    {
        public virtual void Clear()
        {

        }

        public virtual void Init()
        {

        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
