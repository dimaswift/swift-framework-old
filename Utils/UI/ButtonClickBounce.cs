﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils.UI
{
    public class ButtonClickBounce : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerExitHandler
    {
        [SerializeField] private float animationDuration = .15f;
        [SerializeField] private float scaleDownPercent = .9f;

        private Coroutine currentAnimation;
        private bool clicked;
        private Vector3 startScale;

        private void Awake()
        {
            startScale = transform.localScale;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (currentAnimation != null)
            {
                StopCoroutine(currentAnimation);
            }
            currentAnimation = StartCoroutine(PressAnimationRoutine());
            clicked = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Release();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Release();
        }

        private void Release()
        {
            if (clicked == false)
            {
                return;
            }
            if (currentAnimation != null)
            {
                StopCoroutine(currentAnimation);
            }
            currentAnimation = StartCoroutine(BounceAnimationRoutine());
            clicked = false;
        }

        private IEnumerator BounceAnimationRoutine()
        {
            float t = 0f;
            while (t < 1)
            {
                transform.localScale = Vector3.Lerp(startScale * scaleDownPercent, startScale, t);
                t += Time.unscaledDeltaTime / animationDuration;
                yield return null;
            }
            transform.localScale = startScale;
        }

        private IEnumerator PressAnimationRoutine()
        {
            float t = 0f;
            while(t < 1)
            {
                transform.localScale = Vector3.Lerp(startScale, startScale * scaleDownPercent, t);
                t += Time.unscaledDeltaTime / animationDuration;
                yield return null;
            }
        }

    }

}
