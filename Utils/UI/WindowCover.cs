﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils
{
    public class WindowCover : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            GetComponentInParent<IWindow>()?.Hide();
        }
    }

}
