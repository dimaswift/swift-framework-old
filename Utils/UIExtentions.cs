﻿using SwiftFramework.Core;
using System;
using System.Numerics;
using TMPro;

namespace SwiftFramework.Utils
{
    public static class UIExtentions
    {
        public static void SubscribeToEvent(this TMP_Text text, IStatefulEvent<BigNumber> statefulEvent)
        {
            text.text = statefulEvent.ToString();
            statefulEvent.OnValueChanged += v => text.text = v.ToString();
        }

        public static string ToTimerString(this float seconds)
        {
            TimeSpan timeLeft = TimeSpan.FromSeconds(seconds);
            return timeLeft.ToTimerString();
        }

        public static string ToDurationString(this float seconds, ILocalizationManager localization)
        {
            TimeSpan timeLeft = TimeSpan.FromSeconds(seconds);
            if (timeLeft.TotalDays >= 1)
            {
                return localization.GetText("days", timeLeft.Days);
            }
            else if (timeLeft.TotalHours >= 1)
            {
                return localization.GetText("hours", timeLeft.Hours);
            }
            else if(timeLeft.TotalMinutes >= 1)
            {
                return localization.GetText("minutes", timeLeft.Minutes);
            }
            else
            {
                return localization.GetText("seconds", timeLeft.Seconds);
            }
        }


        public static string ToMultiplierString(this float multiplier)
        {
            return multiplier % 1 != 0 ? $"x {multiplier.ToString("0.0")}" : $"x {multiplier.ToString("0")}";
        }

        public static string ToTimerString(this TimeSpan timeLeft)
        {
            if (timeLeft.TotalDays >= 1)
            {
                return $"{timeLeft.Days.ToString("00")}:{timeLeft.Hours.ToString("00")}:{timeLeft.Minutes.ToString("00")}:{timeLeft.Seconds.ToString("00")}";
            }
            else if (timeLeft.TotalHours >= 1)
            {
                return $"{timeLeft.Hours.ToString("00")}:{timeLeft.Minutes.ToString("00")}:{timeLeft.Seconds.ToString("00")}";
            }
            else
            {
                return $"{timeLeft.Minutes.ToString("00")}:{timeLeft.Seconds.ToString("00")}";
            }
        }
       
    }
}


