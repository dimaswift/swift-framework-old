﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;

public class DummyPurchaser : Module, IInAppPurchaseManager
{
    public event Action<string> OnItemPurchased;

    public IPromise<bool> Buy(string productId)
    {
        return Promise<bool>.Resolved(true);
    }

    public string GetCurrencyCode(string productId)
    {
        return "USD";
    }

    public decimal GetPrice(string productId)
    {
        return 0.01m;
    }

    public string GetPriceString(string productId)
    {
        return "$0.01";
    }

    public bool IsAlreadyPurchased(string productId)
    {
        return false;
    }

    public bool IsSubscribed(string productId)
    {
        return false;
    }

    public IPromise<bool> RestorePurchases()
    {
        return Promise<bool>.Resolved(true);
    }
}
