﻿using SwiftFramework.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.AdsManager
{
    public class DummyAdsManager : Module, IAdsManager
    {
        public event Action<string> OnRewardedShowStarted = placement => { };
        public event Action<string> OnRewardedSuccessfully = placement => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedClosed = placement => { };

        private string currentPlacement;

        private bool ready = true;

        public IPromise<bool> ShowRewarded(string placementId)
        {
            currentPlacement = placementId;

            OnRewardedShowStarted(placementId);

            Promise<bool> result = Promise<bool>.Create();

            PromiseTimer.WaitForNextFrame().Done(() => 
            {
                result.Resolve(true);
                OnRewardedSuccessfully(currentPlacement);
                OnRewardedClosed(currentPlacement);
            });

            return result; 
        }

        protected override IPromise GetLoadPromise()
        {
            OnRewardedAdLoaded();
            return base.GetLoadPromise();
        }

        public bool IsRewardedReady()
        {
            return ready;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                ready = !ready;
            }
        }

        public bool IsShowingRewardedAd()
        {
            return false;
        }

        public float GetTimeSinceAdClosed()
        {
            return float.MaxValue;
        }

        public bool ShowInterstitial(string placementId)
        {
            return false;
        }

        public bool IsInterstitialReady()
        {
            return false;
        }

        public void TryShowInterstitial()
        {
            
        }

        public void ResetInterstitialCounter()
        {
     
        }
    }

}
