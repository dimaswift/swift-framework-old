﻿using SwiftFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.FirebaseWrapper
{
    public class FirebaseWrapper : Module, IFirebase
    {
        protected override IPromise GetLoadPromise()
        {
            Promise loadPromise = Promise.Create();

#if USE_FIREBASE

            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => 
            {
                PromiseTimer.WaitForNextFrame().Done(() =>
                {
                    var dependencyStatus = task.Result;
                    if (dependencyStatus == Firebase.DependencyStatus.Available)
                    {
                        Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                        Debug.Log($"Firebase app initialized: {app.Name}");
                        loadPromise.Resolve();
                    }
                    else
                    {
                        loadPromise.Resolve();
                        Debug.LogError(string.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    }
                });
            });

#else
            LogNotEnabledWarning("USE_FIREBASE");
            loadPromise.Resolve();
#endif
            return Promise.Race(PromiseTimer.WaitFor(5), loadPromise);
        }
    }

}
