﻿using SwiftFramework.IdleGame.Model;
using SwiftFramework.IdleGame.Controller;
using SwiftFramework.Core;

namespace SwiftFramework.IdleGame
{
    public class IdleGameController 
    {
        private GameState gameState;

        private GlobalConfig globalConfig;

        public SimulationController Simulation { get; private set; }

        public IdleGameController(GameState gameState, GlobalConfig globalConfig, IViewFactory viewFactory)
        {
            this.globalConfig = globalConfig;
            this.gameState = gameState;

            Simulation = new SimulationController(gameState, globalConfig, viewFactory);
        }

        

        public void Update(float delta)
        {
            if(Simulation == null)
            {
                return;
            }

            Simulation.Update(delta);
        }
    }
}
