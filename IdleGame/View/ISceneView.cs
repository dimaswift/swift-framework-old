﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Controller;
using UnityEngine;

namespace SwiftFramework.IdleGame.View
{
    public interface ISceneView : IView<SimulationController>
    {
        Vector3 GetFactoryPosition(int index);
        Vector3 GetPlanetPosition();
        Vector3 GetTransporterPosition();
        void ShowPopText(string message, Vector3 position);
        float GetFactoriesBottomPoint();
    }


}
