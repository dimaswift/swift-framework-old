﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Controller;

namespace SwiftFramework.IdleGame.View
{
    public interface ITransporterView : IView<TransporterController>
    {

    }

}
