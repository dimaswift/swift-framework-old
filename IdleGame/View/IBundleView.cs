﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Controller;

namespace SwiftFramework.IdleGame.View
{
    public interface IBundleView : IView<BundleController>
    {
        void Activate();
    }

}
