﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Controller;
using UnityEngine;

namespace SwiftFramework.IdleGame.View
{
    public interface IBotView : IView<BotController>
    {

    }
}
