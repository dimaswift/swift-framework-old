﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Controller;
using UnityEngine;

namespace SwiftFramework.IdleGame.View
{
    public interface IFactoryView : IView<FactoryController>
    {
        Vector3 GetBundleStartPosition();
        Vector3 GetBundleTargetPosition();
        Vector3 GetTransporterLoadPosition();
    }

}
