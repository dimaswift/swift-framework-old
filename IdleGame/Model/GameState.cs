﻿using SwiftFramework.Core;
using System.Collections.Generic;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class GameState
    {
        public BigNumber credits;
        public BigNumber gems;
        public Miner planet = new Miner();
        public Transporter transporter = new Transporter();
        public List<Factory> factories = new List<Factory>();

        public GameState DeepCopy()
        {
            return new GameState()
            {
                credits = credits,
                gems = gems,
                planet = planet.DeepCopy(),
                transporter = transporter.DeepCopy(),
                factories = factories.DeepCopy(),
            };
        }
    }
}
