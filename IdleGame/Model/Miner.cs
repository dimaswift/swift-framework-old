﻿using SwiftFramework.Core;
using System.Collections.Generic;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class Miner : IDeepCopy<Miner>, IManagable
    {
        public int planetIndex;
        public int totalPlanetsCleared;
        public int areaIndex;
        public BigNumber resources;

        public BigNumber level;
        public BigNumber revenueFromBots;
        public float digSpeed;
        public float movingSpeed;
        public ulong botsAmount;
        public List<Manager> managers = new List<Manager>();

        public IList<Manager> Managers => managers;

        public Miner DeepCopy()
        {
            return new Miner()
            {
                level = level,
                botsAmount = botsAmount,
                revenueFromBots = revenueFromBots,
                digSpeed = digSpeed,
                movingSpeed = movingSpeed,
                managers = managers.DeepCopy(),
                resources= resources,
                planetIndex = planetIndex,
                areaIndex = areaIndex
            };
        }
    }
}
