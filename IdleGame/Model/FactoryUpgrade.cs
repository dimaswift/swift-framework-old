using SwiftFramework.Core;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class UpgradeSettings
    {
        public BigNumber levelsNeededForExtraBundle;
        public float productionSpeedMultiplier;
        public float productionTimeMultiplier;
        public float incomePerBundleMultiplier;
        public float upgradeCostMultiplier;
    }
}
