using SwiftFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class Transporter : IManagable, IDeepCopy<Transporter>
    {
        public BigNumber level;
        public BigNumber capacity;
        public BigNumber loadingRate;
        public float moveSpeed;
       
        public List<Manager> managers = new List<Manager>();

        public IList<Manager> Managers => managers;

        public Transporter DeepCopy()
        {
            return new Transporter()
            {
                level = level,
                capacity = capacity,
                loadingRate = loadingRate,
                moveSpeed = moveSpeed,
                managers = managers.DeepCopy()
            };
        }
    }
}
