﻿using SwiftFramework.Core;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class Manager : IDeepCopy<Manager>
    {
        public Manager DeepCopy()
        {
            return new Manager();
        }
    }
}
