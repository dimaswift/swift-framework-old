﻿using SwiftFramework.Core;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class GlobalConfig
    {
        public GameState initialState;

        public MinerConfig bots = new MinerConfig();

        public TransporterConfig transporter = new TransporterConfig();

        public int planetsAmountInArea = 10;

        public float planetResourcesMultiplier = 10;

        public BigNumber startPlanetResourcesAmount;

        [System.Serializable]
        public class MinerConfig
        {
            public ulong maximumMiningBotsAmount = 30;
            public float botTimeToReachMine = 2;
            public float turnAroundTime = 2;
            public float botDiggingTime = 3;
            public float botReturnToStorageTime = 3;
            public float botUnloadingTime = 2;
            public float maxBotStartDelay = 1f;
            public float planetSwitchDuration = 3;
        }

        [System.Serializable]
        public class TransporterConfig
        {
            public float travelTimeBetweenFactories = 2;
            public float turnAroundTime = 2;
            public float goBackToPortalTime = 2;
            public float loadTime = .25f;
            public float travelTime = 3;
            public float portalTime = 2;
            public int travelToPlanetIterations = 3;

            public float unloadingTime;
            public int boxesAmount;

        }
    }
}
