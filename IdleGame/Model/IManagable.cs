﻿using System.Collections.Generic;

namespace SwiftFramework.IdleGame.Model
{
    public interface IManagable
    {
        IList<Manager> Managers { get; }
    }


}
