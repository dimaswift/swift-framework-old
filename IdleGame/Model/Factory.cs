using SwiftFramework.Core;
using SwiftFramework.IdleGame.Controller;
using System.Collections.Generic;

namespace SwiftFramework.IdleGame.Model
{
    [System.Serializable]
    public class Factory : IUpgradable, IManagable, IDeepCopy<Factory>
    {
        public BigNumber level;
        public BigNumber incomePerBundle;
        public ulong batchSize;
        public BigNumber cost;
        public BigNumber upgradeCost;
        public BigNumber creditsInStash;
        public float productionSpeed;
        public float productionTime;
        public float bundleTransportTime;
        public List<Manager> managers = new List<Manager>();

        public IList<Manager> Managers => managers;

        public Factory DeepCopy()
        {
            return new Factory()
            {
                level = level,
                incomePerBundle = incomePerBundle,
                batchSize = batchSize,
                cost = cost,
                upgradeCost = upgradeCost,
                productionSpeed = productionSpeed,
                productionTime = productionTime,
                managers = managers.DeepCopy(),
                bundleTransportTime = bundleTransportTime,
                creditsInStash = creditsInStash,
            };
        }
    }
}
