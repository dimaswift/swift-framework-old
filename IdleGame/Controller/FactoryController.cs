﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using SwiftFramework.IdleGame.Model;
using SwiftFramework.IdleGame.View;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace SwiftFramework.IdleGame.Controller
{
    public class FactoryController : ManagedController<Factory>
    {
        public IStatefulEvent<BigInteger> CreditsInStash => creditsInStash;
        private readonly StatefulEvent<BigInteger> creditsInStash = new StatefulEvent<BigInteger>(0);

        public IStatefulEvent<State> CurrentState => currentState;
        private readonly StatefulEvent<State> currentState = new StatefulEvent<State>(State.Idle);

        public event Action<BigInteger> OnTakenFromStash = a => { };

        public event Action OnStashEmptied = () => { };

        public enum State { Idle, Producing }

        private float producingTimer;

        private readonly SimplePool<BundleController> bundlePool;

        private readonly List<BundleController> producedBundles = new List<BundleController>();

        public int BundlesAmount => producedBundles.Count;

        public IFactoryView View { get; private set; }

        public int OrderIndex { get; private set; }

        public float BundleTransportTime => Model.bundleTransportTime;

        private readonly Factory factory;

        public FactoryController(Factory factory, SimulationController simulation, int orderIndex) : base(factory, simulation)
        {
            OrderIndex = orderIndex;
            View = Simulation.ViewFactory.CreateView<IFactoryView>();
            View.Init(this);
            bundlePool = new SimplePool<BundleController>(CreateNewBundle);
            creditsInStash.SetValue(factory.creditsInStash.Value);
            creditsInStash.OnValueChanged += v => factory.creditsInStash = v;
        }

        public override void ActivateManually()
        {
            if(currentState == State.Idle)
            {
                StartProducing();
            }
        }

        public void AddCreadits(BigInteger amount)
        {
            SetCreaditsInStash(creditsInStash.Value + amount);
        }

        public void StartProducing()
        {
            currentState.SetValue(State.Producing);
            producingTimer = 0;
        }

        public BigInteger TakeFromStash(BigInteger amountToTake)
        {
       
            if (amountToTake <= 0)
            {
                OnStashEmptied();
                return 0;
            }

            SetCreaditsInStash(creditsInStash.Value - amountToTake);

            OnTakenFromStash(amountToTake);

            if (creditsInStash.Value <= 0)
            {
                SetCreaditsInStash(0);
                OnStashEmptied();
            }
            return amountToTake;
        }

        private void SetCreaditsInStash(BigInteger value)
        {
            Model.creditsInStash.Value = value;
            creditsInStash.SetValue(value);
        }

        private BundleController CreateNewBundle()
        {
            BundleController bundle = new BundleController(Simulation.ViewFactory, this);
            return bundle;
        }

        public void Process(float delta)
        {
            for (int i = producedBundles.Count - 1; i >= 0; i--)
            {
                producedBundles[i].Process(delta, out bool finished);
                if (finished)
                {
                    producedBundles.RemoveAt(i);
                }
            }
            if (currentState == State.Idle)
            {
                if (ReadyToWork)
                {
                    StartProducing();
                }
            }
            else if (currentState == State.Producing)
            {
                producingTimer += delta * Model.productionSpeed;
                if (producingTimer >= Model.productionTime)
                {
                    float singleBundleTransportationTime = Model.bundleTransportTime / Model.batchSize;
                    float transportDelayPerBundle = Model.bundleTransportTime / Model.batchSize;
                    for (ulong i = 0; i < Model.batchSize; i++)
                    {
                        BundleController bundleController = bundlePool.Take();
                        bundleController.BeginWork(i * (transportDelayPerBundle / 2), Model.incomePerBundle.Value, Model.bundleTransportTime, producedBundles.Count);
                        producedBundles.Add(bundleController);
                    }

                    if (ReadyToWork == false)
                    {
                        currentState.SetValue(State.Idle);
                    }
                    else
                    {
                        producingTimer = 0;
                    }
                }
            }
        }
    }
}