﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.IdleGame.Model;
using System.Numerics;

namespace SwiftFramework.IdleGame.Controller
{
    public abstract class UpgradeController<T> : IUpgradeController where T : IUpgradable
    {
        protected UpgradeSettings upgradeSettings;
        protected BankController bank;

        public UpgradeController(UpgradeSettings upgradeSettings, BankController bank)
        {
            this.upgradeSettings = upgradeSettings;
            this.bank = bank;
        }

        public abstract bool TryLevelUp(T upgradable, ulong levels);

        protected abstract void LevelUp(T upgradable, ulong levels);

        public abstract T GetLeveledUp(T upgradable, ulong levels);

        protected abstract IEnumerable<UpgradedValue> GetUpgradedValues(T upgradable, ulong levels);

        bool IUpgradeController.TryLevelUp(IUpgradable upgradable, ulong levels)
        {
            return TryLevelUp((T)upgradable, levels);
        }

        void IUpgradeController.LevelUp(IUpgradable upgradable, ulong levels)
        {
            LevelUp((T)upgradable, levels);
        }

        IUpgradable IUpgradeController.GetNextLevel(IUpgradable upgradable, ulong levels)
        {
            return GetLeveledUp((T)upgradable, levels);
        }

        IEnumerable<UpgradedValue> IUpgradeController.GetUpgradedValues(IUpgradable upgradable, ulong levels)
        {
            foreach (var item in GetUpgradedValues((T)upgradable, levels))
            {
                yield return item;
            }
        }
    }

    public interface IUpgradeController
    {
        bool TryLevelUp(IUpgradable upgradable, ulong levels);

        void LevelUp(IUpgradable upgradable, ulong levels);

        IUpgradable GetNextLevel(IUpgradable upgradable, ulong levels);

        IEnumerable<UpgradedValue> GetUpgradedValues(IUpgradable upgradable, ulong levels);
}

    public struct UpgradedValue
    {
        public BigInteger baseValue;
        public BigInteger upgradedValue;
        public float baseFloatValue;
        public float upgradedFloatValue;

        public string name;
    }

    public interface IUpgradable
    {
       
    }
}


