﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Model;
using SwiftFramework.IdleGame.View;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace SwiftFramework.IdleGame.Controller
{
    public class SimulationController
    {
        public IStatefulEvent<BigInteger> SoftCurrency => credits;
        private readonly StatefulEvent<BigInteger> credits = new StatefulEvent<BigInteger>();

        public IStatefulEvent<BigInteger> HardCurrency => gems;
        private readonly StatefulEvent<BigInteger> gems = new StatefulEvent<BigInteger>();

        public IStatefulEvent<BigInteger> ProductionSpeed => productionSpeed;
        private readonly StatefulEvent<BigInteger> productionSpeed = new StatefulEvent<BigInteger>();

        private readonly List<FactoryController> factories;

        public MinerController Miner { get; }

        public TransporterController Transporter { get; }

        public GlobalConfig Config { get; private set; }

        public IViewFactory ViewFactory { get; private set; }

        public ISceneView SceneView { get; private set; }

        public FactoryController GetFactory(int index)
        {
            if (index >= factories.Count)
            {
                return null;
            }
            return factories[index];
        }

        public int FactoryCount => factories.Count;

        public SimulationController(GameState state, GlobalConfig globalConfig, IViewFactory viewFactory)
        {
            Config = globalConfig;

            ViewFactory = viewFactory;

            SceneView = ViewFactory.CreateView<ISceneView>();

            Transporter = new TransporterController(state.transporter, this);

            Miner = new MinerController(state.planet, this, ViewFactory.CreateView<IMinerView>());

            Miner.OnCreditsProduced += Planet_OnCreditsProduced;

            factories = new List<FactoryController>(state.factories.Count);
          

            for (int i = 0; i < state.factories.Count; i++)
            {
                factories.Add(new FactoryController(state.factories[i], this, i));
            }

            credits.SetValue(state.credits.Value);
            credits.OnValueChanged += v => state.credits = v;

            gems.SetValue(state.gems.Value);
            gems.OnValueChanged += v => state.gems = v;
        }

        private void Planet_OnCreditsProduced(BigInteger amount)
        {
            credits.SetValue(credits.Value + amount);
        }

        public void Update(float delta)
        {
            ProcessFactories(delta);
            Transporter.Process(delta);
            Miner.Process(delta);
        }

        private void ProcessFactories(float delta)
        {
            for (int i = 0; i < factories.Count; i++)
            {
                FactoryController factory = factories[i];
                factory.Process(delta);
            }
        }
    }

    public interface IContoller
    {
        void ActivateManually();
    }
}