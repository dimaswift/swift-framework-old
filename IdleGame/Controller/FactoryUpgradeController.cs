﻿using SwiftFramework.IdleGame.Model;
using System.Collections.Generic;
using System.Numerics;
using SwiftFramework.Core;

namespace SwiftFramework.IdleGame.Controller
{
    public class FactoryUpgradeController : UpgradeController<Factory>
    {
        public FactoryUpgradeController(UpgradeSettings upgradeSettings, BankController bank) : base(upgradeSettings, bank)
        {
        }

        public override bool TryLevelUp(Factory factory, ulong levels)
        {
            BigInteger cost = factory.upgradeCost.Value;

            for (ulong i = 0; i < levels; i++)
            {
                cost += cost.MultiplyByFloat(upgradeSettings.upgradeCostMultiplier);
            }

            if (bank.TryToWithdraw(cost) == false)
            {
                return false;
            }

            LevelUp(factory, levels);

            return true;
        }

        protected override void LevelUp(Factory factory, ulong levels)
        {
            for (ulong i = 0; i < levels; i++)
            {
                factory.level.Value++;
                if (factory.level.Value % upgradeSettings.levelsNeededForExtraBundle.Value == 0)
                {
                    factory.batchSize++;
                }
                factory.incomePerBundle.Value += factory.incomePerBundle.Value.MultiplyByFloat(upgradeSettings.incomePerBundleMultiplier);
                factory.productionSpeed += factory.productionSpeed * upgradeSettings.productionSpeedMultiplier;
                factory.productionTime += factory.productionTime * upgradeSettings.productionTimeMultiplier;
            }
        }

        public override Factory GetLeveledUp(Factory factory, ulong levels)
        {
            Factory factoryCopy = factory.DeepCopy();
            
            LevelUp(factoryCopy, levels);

            return factoryCopy;
        }

        protected override IEnumerable<UpgradedValue> GetUpgradedValues(Factory upgradable, ulong levels)
        {
            Factory upgradedCopy = upgradable.DeepCopy();

            LevelUp(upgradedCopy, levels);
            
            yield return new UpgradedValue()
            {
                baseValue = upgradable.batchSize,
                upgradedValue = upgradedCopy.batchSize,
                name = "#bundles_amount"
            };

            yield return new UpgradedValue()
            {
                baseValue = upgradable.incomePerBundle.Value,
                upgradedValue = upgradedCopy.incomePerBundle.Value,
                name = "#bundles_size"
            };

            yield return new UpgradedValue()
            {
                baseValue = upgradable.incomePerBundle.Value,
                upgradedValue = upgradedCopy.incomePerBundle.Value,
                name = "#income"
            };

            yield return new UpgradedValue()
            {
                baseFloatValue = upgradable.productionSpeed,
                upgradedFloatValue = upgradedCopy.productionSpeed,
                name = "#production_speed"
            };

            yield return new UpgradedValue()
            {
                baseFloatValue = upgradable.productionTime,
                upgradedFloatValue = upgradedCopy.productionTime,
                name = "#production_time"
            };

            yield return new UpgradedValue()
            {
                baseValue = upgradable.cost.Value,
                upgradedValue = upgradedCopy.cost.Value,
                name = "#cost"
            };
        }
    }
}

