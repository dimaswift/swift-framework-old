﻿using System;

namespace SwiftFramework.IdleGame.Controller
{
    public struct Phase<T>
    {
        public T state;
        public float speed;
        public float time;
        public int iterations;
        public Action onComplete;
        public Func<bool> completeCondition;
        public Func<bool> startCondition;

    }
}