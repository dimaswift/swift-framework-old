﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Model;
using SwiftFramework.IdleGame.View;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace SwiftFramework.IdleGame.Controller
{
    public class TransporterController : ManagedController<Transporter>
    {
        public event Action<BigInteger> OnUnloadedCredits = v => { };
        public event Action<BigInteger> OnLoadedCredits = v => { };


        public IStatefulEvent<State> CurrentState => phaseLoop.State;

        public IStatefulEvent<BigInteger> LoadedAmount => loadedAmount;
        private readonly StatefulEvent<BigInteger> loadedAmount = new StatefulEvent<BigInteger>(0);

        public int TargetFactoryIndex { get; private set; }

        public float TimeNormalized => phaseLoop.TimeNormalized;

        public int CurrentIteration => phaseLoop.CurrentIteration;

        public ITransporterView View { get; private set; }

        public BigInteger LastLoadedAmount { get; private set; }
        public BigInteger Capacity => Model.capacity.Value;

        public enum State
        {
            Idle,
            MovingToFactory,
            Loading,
            GoingToPlanet,
            GoingToPortal,
            GoingInsideOfPortal,
            TurningAround,
            Unloading,
            ReturningFromPlanet,
            GoingOutOfPortal,
        }

       
        private readonly PhaseLoop<State> phaseLoop = new PhaseLoop<State>();


        public TransporterController(Transporter transporter, SimulationController simulation) : base(transporter, simulation)
        {
            loadedAmount.SetValue(0);
            View = simulation.ViewFactory.CreateView<ITransporterView>();
            View.Init(this);
            phaseLoop.State.OnValueChangedDelta += State_OnValueChangedDelta;
            phaseLoop.SetLoop(GetPhases(), () => ReadyToWork);
        }

        private int GetNextFactoryIndex(int current)
        {
            while (current < Simulation.FactoryCount && Simulation.GetFactory(current).CreditsInStash.Value == 0)
            {
                current++;
            }
            return current;
        }

        private void State_OnValueChangedDelta(State oldValue, State newValue)
        {
            if(newValue == State.MovingToFactory)
            {
                LastLoadedAmount = 0;
            }
        }


        private void OnLoaded()
        {
            FactoryController factory = Simulation.GetFactory(TargetFactoryIndex);

            BigInteger amountToTake = BigInteger.Min(factory.CreditsInStash.Value, Model.loadingRate.Value);

            amountToTake = BigInteger.Min(amountToTake, Model.capacity.Value - LoadedAmount.Value);

            BigInteger amount = factory.TakeFromStash(amountToTake);

            loadedAmount.SetValue(loadedAmount.Value + amount);
            LastLoadedAmount += amount;

            OnLoadedCredits(amount);

            if (factory.CreditsInStash.Value <= 0)
            {
                int nextFactoryIndex = GetNextFactoryIndex(TargetFactoryIndex);
                if (nextFactoryIndex < Simulation.FactoryCount)
                {
                    TargetFactoryIndex = nextFactoryIndex;
                    phaseLoop.ForceState(State.MovingToFactory);
                }
            }
        }

        private void OnUnloaded()
        {
            BigInteger amountToUnload = BigInteger.Min(Model.loadingRate.Value, loadedAmount.Value);
            loadedAmount.SetValue(loadedAmount.Value - amountToUnload);
            Simulation.Miner.AddRevenueFromBots(amountToUnload);

            OnUnloadedCredits(amountToUnload);
        }

        private bool CanFinishUnloading()
        {
            return loadedAmount.Value <= 0;
        }

        private bool CanFinishLoading()
        {
            if(LoadedAmount.Value >= Model.capacity.Value)
            {
                return true;
            }

            int nextFactoryIndex = GetNextFactoryIndex(TargetFactoryIndex);

            if(nextFactoryIndex >= Simulation.FactoryCount)
            {
                return true;
            }

            return false;
        }

        private IEnumerable<Phase<State>> GetPhases()
        {
            yield return new Phase<State>()
            {
                state = State.Idle,
                speed = 1,
                completeCondition = CanMoveToNextFactory
            };

            yield return new Phase<State>()
            {
                state = State.MovingToFactory,
                time = Simulation.Config.transporter.travelTimeBetweenFactories,
                speed = Model.moveSpeed,
            };

            yield return new Phase<State>()
            {
                state = State.Loading,
                time = Simulation.Config.transporter.loadTime,
                onComplete = OnLoaded,
                completeCondition = CanFinishLoading,
                speed = 1
            };

            yield return new Phase<State>()
            {
                state = State.TurningAround,
                time = Simulation.Config.transporter.turnAroundTime,
                speed = 1
            };

            yield return new Phase<State>()
            {
                state = State.GoingToPortal,
                time = Simulation.Config.transporter.goBackToPortalTime,
                speed = 1
            };

            yield return new Phase<State>()
            {
                state = State.GoingInsideOfPortal,
                time = Simulation.Config.transporter.portalTime,
                speed = 1
            };

            yield return new Phase<State>()
            {
                state = State.GoingToPlanet,
                time = Simulation.Config.transporter.travelTime,
                speed = 1,
                startCondition = () => Simulation.Miner.PlanetResources.Value > 0 && Simulation.Miner.SwitchingPlanets == false
            };

            yield return new Phase<State>()
            {
                state = State.Unloading,
                time = Simulation.Config.transporter.unloadingTime,
                completeCondition = CanFinishUnloading,
                onComplete = OnUnloaded,
                speed = 1
            };

            yield return new Phase<State>()
            {
                state = State.ReturningFromPlanet,
                time = Simulation.Config.transporter.travelTime,
                speed = 1
            };

            yield return new Phase<State>()
            {
                state = State.GoingOutOfPortal,
                time = Simulation.Config.transporter.portalTime,
                speed = 1
            };
        }

        private bool CanMoveToNextFactory()
        {
            int nextFactoryIndex = GetNextFactoryIndex(0);
            if (nextFactoryIndex >= Simulation.FactoryCount)
            {
                return false;
            }
            TargetFactoryIndex = nextFactoryIndex;
            return true;
        }

        public void Process(float delta)
        {
            phaseLoop.Update(delta);
        }

        public override void ActivateManually()
        {
            phaseLoop.Activate();
        }
    }
}