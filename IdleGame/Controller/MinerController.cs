﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.Model;
using SwiftFramework.IdleGame.View;
using System;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

namespace SwiftFramework.IdleGame.Controller
{
    public class MinerController : ManagedController<Miner>
    {
        public float MovingSpeed => Model.movingSpeed;

        public float DiggingSpeed => Model.digSpeed;

        public bool IsEmpty => PlanetResources.Value <= 0;

        public bool SwitchingPlanets => switchingPlanets;

        public float PlanetSwitchTimeNormalized => switchingPlanetsTime / Simulation.Config.bots.planetSwitchDuration;

        public IStatefulEvent<BigInteger> PlanetResources => planetResources; 
        private readonly StatefulEvent<BigInteger> planetResources = new StatefulEvent<BigInteger>();

        public IStatefulEvent<BigInteger> RevenuePerBot => revenueFromBots;
        private readonly StatefulEvent<BigInteger> revenueFromBots = new StatefulEvent<BigInteger>();

        public event Action<BigInteger> OnCreditsProduced = c => { };

        public event Action<BotController> OnBotCreated = bot => { };

        public event Action OnRanOutOfPlanetResource = () => { };

        public event Action OnSwitchToNextPlanetBegin = () => { };

        public event Action OnSwitchToNextPlanetFinished = () => { };

        private readonly List<BotController> bots = new List<BotController>();

        public IEnumerable<BotController> Bots => bots;

        public int PlanetIndex => Model.planetIndex;
        public int AreaIndex => Model.areaIndex;

        private float switchingPlanetsTime;

        private bool switchingPlanets;

        private readonly Miner miner;

        public MinerController(Miner miner, SimulationController simulation, IMinerView view) : base(miner, simulation)
        {
            this.miner = miner;
            planetResources.OnValueChanged += v => miner.resources = v;
            planetResources.SetValueWithoutNotify(miner.resources.Value);
            revenueFromBots.SetValueWithoutNotify(miner.revenueFromBots.Value);
            view.Init(this);
            simulation.Transporter.CurrentState.OnValueChangedDelta += Transporter_OnStateChanged;
            for (ulong i = 0; i < miner.botsAmount; i++)
            {
                CreateBot();
            }
        }

        private void Transporter_OnStateChanged(TransporterController.State oldState, TransporterController.State newState)
        {
            if (oldState == TransporterController.State.Unloading && newState == TransporterController.State.ReturningFromPlanet)
            {
             
                if (bots.Count < GetMaxBotsAmount())
                {
                    CreateBot();
                    miner.botsAmount++;
                }
            }
        }

        private BigInteger TakeResourcesFromPlanet(BigInteger value)
        {
            BigInteger result = BigInteger.Min(value, planetResources.Value);

            planetResources.SetValue(planetResources.Value - result);

            return result;
        }

        private void OnRanOutOfPlanetResources()
        {
            foreach (var bot in bots)
            {
                bot.Deactivate();
            }
            OnRanOutOfPlanetResource();
            switchingPlanets = false;
            switchingPlanetsTime = 0;
        }

        private void SetRevenueFromBots(BigInteger value)
        {
            revenueFromBots.SetValue(value);
            miner.revenueFromBots = value;
        }

        public void StartFromFirstArea()
        {
            Model.areaIndex = 0;
        }

        private void CreateBot()
        {
            BotController bot = new BotController(this);
            bots.Add(bot);
            bot.CurrentState.OnValueChanged += (state) => Bot_OnStateChanged(bot);
            bots.Shuffle();
            OnBotCreated(bot);
        }

        private void Bot_OnStateChanged(BotController bot)
        {
            if (bot.CurrentState.Value == BotController.State.ReturningToPosition && bot.diggedAmount > 0)
            {
                BigInteger diggedAmount = TakeResourcesFromPlanet(bot.diggedAmount);

                if(diggedAmount > 0 && PlanetResources.Value <= 0)
                {
                    OnRanOutOfPlanetResources();
                }

                if(diggedAmount > 0)
                {
                    OnCreditsProduced(diggedAmount);
                    bot.diggedAmount = 0;
                }
            }
            if (bot.CurrentState.Value == BotController.State.Digging)
            {
                bot.diggedAmount = miner.revenueFromBots.Value / miner.botsAmount;
            }
        }

        public override void ActivateManually()
        {
            if (IsEmpty)
            {
                SwitchToNextPlanet();
                switchingPlanets = true;
            }
            else
            {
                float delay = 0f;
                for (int i = 0; i < bots.Count; i++)
                {
                    if(bots[i].CurrentState.Value == BotController.State.Idle)
                    {
                        bots[i].SetStartDelay(delay);
                        bots[i].Activate();
                        delay += Simulation.Config.bots.maxBotStartDelay / bots.Count;
                    }
                }
            }
        }

        private int GetMaxBotsAmount()
        {
            return (int)Mathf.Min(miner.planetIndex + 3, Simulation.Config.bots.maximumMiningBotsAmount);
        }

        public void AddRevenueFromBots(BigInteger amount)
        {
            SetRevenueFromBots(revenueFromBots.Value + amount);
        }

        private void SwitchToNextPlanet()
        {
            miner.planetIndex++;

            if(miner.planetIndex >= Simulation.Config.planetsAmountInArea)
            {
                miner.areaIndex++;
                miner.planetIndex = 0;
            }

            miner.totalPlanetsCleared++;

            BigInteger newAmount = Simulation.Config.startPlanetResourcesAmount.Value;

            float multiplier = Simulation.Config.planetResourcesMultiplier;

            for (int i = 0; i < miner.totalPlanetsCleared; i++)
            {
                newAmount = newAmount.MultiplyByFloat(multiplier);
            }

            bots.Clear();

            revenueFromBots.SetValue(0);

            miner.botsAmount = 0;

            miner.managers.Clear();

            planetResources.SetValue(newAmount);

            OnSwitchToNextPlanetBegin();
        }

        public void Process(float delta)
        {
            if(SwitchingPlanets)
            {
                switchingPlanetsTime += delta;
                if (switchingPlanetsTime >= Simulation.Config.bots.planetSwitchDuration)
                {
                    switchingPlanets = false;
                    OnSwitchToNextPlanetFinished();
                }
            }

            foreach (var bot in bots)
            {
                bot.Update(delta);
            }
        }

    }
}