﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleGame.Controller
{
    public class PhaseLoop<S>
    {
        public bool Activated { get; private set; }
        public int CurrentIteration { get; private set; }
        public IStatefulEvent<S> State => state;

        public bool Finished => timer >= targetTime;
        public float TimeNormalized => targetTime == 0 ? 0 : timer / targetTime;


        private readonly List<Phase<S>> phases = new List<Phase<S>>();
        private readonly StatefulEvent<S> state = new StatefulEvent<S>();
        private float timer = 0;
        private float targetTime;
        private int currentPhaseIndex = 0;
        private Func<bool> autoActivate;

        public void Activate()
        {
            if (Activated)
            {
                return;
            }
            Activated = true;
            SetPhase(0);
        }

        public void Deactivate()
        {
            Activated = false;
        }

        private void SetPhase(int index)
        {
            if (index < 0)
            {
                return;
            }
            currentPhaseIndex = index;
            timer = 0;
            targetTime = phases[currentPhaseIndex].time;
            state.SetValue(phases[currentPhaseIndex].state);
            CurrentIteration = 0;
        }

        public void ForceState(S newState)
        {
            SetPhase(phases.FindIndex(p => p.state.Equals(newState)));
        }

        public void Update(float delta)
        {
            if (Activated == false)
            {
                return;
            }
            Phase<S> current = phases[currentPhaseIndex];

            targetTime = current.time;

            if (current.startCondition != null && current.startCondition() == false)
            {
                return;
            }

            timer += current.speed * delta;
           

            if (timer >= current.time)
            {
               
                timer = 0;
                CurrentIteration++;
                current.onComplete?.Invoke();

                if (CurrentIteration < current.iterations || current.completeCondition != null && current.completeCondition() == false)
                {
                    return;
                }
            

                MoveToNextPhase();
            }
        }

        private void MoveToNextPhase()
        {
            currentPhaseIndex++;
            CurrentIteration = 0;

            if (currentPhaseIndex >= phases.Count)
            {
                StartFromFirstPhase();
            }
            else
            {
                Phase<S> current = phases[currentPhaseIndex];
                state.SetValue(current.state);
            }
        }

        private void StartFromFirstPhase()
        {
            currentPhaseIndex = 0;
            Activated = false;
            if (autoActivate())
            {
                Activate();
            }
            else
            {
                state.SetValue(phases[currentPhaseIndex].state);
            }
        }

        public void SetLoop(IEnumerable<Phase<S>> phases, Func<bool> autoActivate)
        {
            this.phases.Clear();
            this.phases.AddRange(phases);
            if (this.phases.Count == 0)
            {
                return;
            }
            this.autoActivate = autoActivate;
            if (autoActivate())
            {
                Activate();
            }
        }

    }

}