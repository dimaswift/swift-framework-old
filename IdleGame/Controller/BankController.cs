﻿using SwiftFramework.IdleGame.Model;
using System.Collections.Generic;
using System.Numerics;

namespace SwiftFramework.IdleGame.Controller
{
    public class BankController
    {
        public BigInteger Credits { get; private set; }

        public System.Action<BigInteger> OnCreditsWidtdrawn = c => { };
        public System.Action<BigInteger> OnCreditsAdded = c => { };

        public bool TryToWithdraw(BigInteger amount)
        {
            if (amount > Credits)
            {
                return false;
            }

            Credits -= amount;

            OnCreditsWidtdrawn(Credits);

            return true;
        }

        public void Add(BigInteger amount)
        {
            Credits += amount;

            OnCreditsAdded(Credits);
        }

        public BankController(BigInteger amount)
        {
            Credits = amount;
        }
    }
}