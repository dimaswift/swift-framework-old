﻿using SwiftFramework.IdleGame.Model;
using System;

namespace SwiftFramework.IdleGame.Controller
{
    public abstract class ManagedController<T> : IContoller where T : IManagable
    {
        public event Action<T> OnModelChanged = m => { };

        public bool Activated { get; set; }

        public bool ReadyToWork => Activated || Model.Managers.Count > 0;

        protected T Model { get; private set; }

        public SimulationController Simulation { get; private set; }

        public ManagedController(T model, SimulationController simulation)
        {
            Model = model;
            Simulation = simulation;
        }

        public abstract void ActivateManually();

        public ManagedController()
        {

        }

        public void NotifyAbountModelChange()
        {
            OnModelChanged(Model);
        }
    }
}