﻿using SwiftFramework.Core;
using SwiftFramework.IdleGame.View;
using System;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

namespace SwiftFramework.IdleGame.Controller
{
    public class BotController
    {
        public enum State
        {
            Idle, GoingToMine, TurningAround, Digging, GoingToStorage, Unloading, ReturningToPosition
        }

        public IBotView View { get; private set; }

        public IStatefulEvent<State> CurrentState => phaseLoop.State;

        public float TimeNormalized => phaseLoop.TimeNormalized;

        public BigInteger diggedAmount;
      
        public float startDelay;

        private PhaseLoop<State> phaseLoop = new PhaseLoop<State>();

        private MinerController miner;

    


        public BotController(MinerController miner)
        {
            this.miner = miner;
            View = miner.Simulation.ViewFactory.CreateView<IBotView>();
            View.Init(this);
            phaseLoop.SetLoop(GetPhases(), () => miner.ReadyToWork);
  
        }

        public void SetStartDelay(float delay)
        {
            startDelay = delay;
        }

        private IEnumerable<Phase<State>> GetPhases()
        {
            yield return new Phase<State>()
            {
                state = State.Idle,
                completeCondition = () => miner.PlanetResources.Value > 0,
                speed = 1
            };
            yield return new Phase<State>()
            {
                state = State.GoingToMine,
                speed = miner.MovingSpeed,
                time = miner.Simulation.Config.bots.botTimeToReachMine,
                startCondition = () => miner.PlanetResources.Value > 0
            };
            yield return new Phase<State>()
            {
                state = State.Digging,
                speed = miner.DiggingSpeed,
                time = miner.Simulation.Config.bots.botDiggingTime,
             
            };
            yield return new Phase<State>()
            {
                state = State.TurningAround,
                speed = miner.MovingSpeed,
                time = miner.Simulation.Config.bots.turnAroundTime
            };
            yield return new Phase<State>()
            {
                state = State.GoingToStorage,
                speed = miner.MovingSpeed,
                time = miner.Simulation.Config.bots.botReturnToStorageTime
            };
            yield return new Phase<State>()
            {
                state = State.Unloading,
                speed = miner.DiggingSpeed,
                time = miner.Simulation.Config.bots.botUnloadingTime
            };
            yield return new Phase<State>()
            {
                state = State.ReturningToPosition,
                speed = miner.MovingSpeed,
                time = miner.Simulation.Config.bots.turnAroundTime
            };
        }


        public void Update(float delta)
        {
            if(startDelay > 0)
            {
                startDelay -= delta;
                return;
            }
            phaseLoop.Update(delta);
        }

        public void Activate()
        {
            if(CurrentState.Value == State.Idle)
            {
                phaseLoop.Activate();
            }
        }


        public void Deactivate()
        {
            if(CurrentState.Value == State.Digging)
            {
                phaseLoop.ForceState(State.GoingToStorage);
            }
        }
    }

}