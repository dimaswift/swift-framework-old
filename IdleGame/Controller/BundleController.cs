﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using SwiftFramework.IdleGame.View;
using System.Numerics;
using UnityEngine;

namespace SwiftFramework.IdleGame.Controller
{
    public class BundleController : IPooled
    {
        public FactoryController Factory { get; private set; }

        public float TimeNormalized => transportationTime > 0 ? timer / transportationTime : 0;

        public int InitialPoolCapacity => 1;

        public int Order { get; private set; }

        public IBundleView View { get; private set; }
        public bool Active { get; private set; }

        private IPool pool;
        private IViewFactory viewFactory;
        private float timer;
        private float delay;
        private float transportationTime;
        private BigInteger creditsAmount;


        public BundleController(IViewFactory viewFactory, FactoryController factory)
        {
            this.viewFactory = viewFactory;
            Factory = factory;
        }

        public void Init(IPool pool)
        {
            this.pool = pool;
        }

        public void BeginWork(float delay, BigInteger creditsAmount, float transportationTime, int order)
        {
            this.delay = delay;
            this.creditsAmount = creditsAmount;
            this.transportationTime = transportationTime;
            Order = order;
            View.Init(this);
            if (delay == 0)
            {
                View.Activate();
            }
        }

        public void Process(float delta, out bool finishedWork)
        {
            finishedWork = false;
            if (delay > 0)
            {
                delay -= delta;

                if(delay <= 0)
                {
                    View.Activate();
                }

                return;
            }

            timer += delta;

            if (timer >= transportationTime)
            {
                Factory.AddCreadits(creditsAmount);
                ReturnToPool();
                finishedWork = true;
                return;
            }
        }

        public void TakeFromPool()
        {
            Active = true;
            timer = 0;
            creditsAmount = 0;
            View = viewFactory.CreateView<IBundleView>();
        }

        public void ReturnToPool()
        {
            Active = false;
            pool.Return(this);
            if(View != null)
            {
                View.ReturnToPool();
                View = null;
            }
        }
    }
}