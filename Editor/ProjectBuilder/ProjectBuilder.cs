using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Text;

namespace SwiftFramework.EditorUtils
{
	public class ProjectBuilder : ScriptableObject
	{
		public const string kLogType = "#### [ProjectBuilder] ";

		public bool buildApplication = true;


		public BuildTarget buildTarget;


		public BuildTarget actualBuildTarget { get { return buildApplication ? buildTarget : EditorUserBuildSettings.activeBuildTarget; } }

		
		public BuildTargetGroup buildTargetGroup { get { return BuildPipeline.GetBuildTargetGroup(actualBuildTarget); } }

		public string productName;

		public string companyName;

		public string applicationIdentifier;

		public string buildName
		{
			get
			{
                if (actualBuildTarget == BuildTarget.Android && !EditorUserBuildSettings.exportAsGoogleAndroidProject)
                    return $"{PlayerSettings.productName}_{version}.{versionCode}.apk";
				else
					return "build";
			}
		}

        public string outputFullPath;

		public string version;

		
		public int versionCode = 0;

		public bool developmentBuild = false;

		public SceneSetting[] scenes = new SceneSetting[]{ };

		public string[] excludeDirectories = new string[]{ };

		public bool buildAssetBundle;

		public bool copyToStreamingAssets;

		public BundleOptions bundleOptions;

		public string bundleOutputPath { get { return "AssetBundles/" + actualBuildTarget; } }

		public BuildTargetSettings_iOS iosSettings = new BuildTargetSettings_iOS();
		public BuildTargetSettings_Android androidSettings = new BuildTargetSettings_Android();
		public BuildTargetSettings_WebGL webGlSettings = new BuildTargetSettings_WebGL();


		[System.Serializable]
		public class SceneSetting
		{
			public bool enable = true;
			public string name;
		}

		public enum BundleOptions
		{
			LZMA = BuildAssetBundleOptions.None,
			LZ4 = BuildAssetBundleOptions.ChunkBasedCompression,
			Uncompressed = BuildAssetBundleOptions.UncompressedAssetBundle,
		}

		protected virtual void OnApplySetting()
		{
		}

        private string GetOutputFolder()
        {
            string prefsKey = "build_output";
            string prevFolder = EditorPrefs.GetString(prefsKey, Util.projectDir);
            outputFullPath = EditorUtility.SaveFolderPanel("Save", prevFolder, "");
            
            if (string.IsNullOrEmpty(outputFullPath))
            {
                return null;
            }

            EditorPrefs.SetString(prefsKey, outputFullPath);

            return Path.Combine(outputFullPath, buildName);
        }

		public virtual void Reset()
		{
			buildTarget = EditorUserBuildSettings.activeBuildTarget;
			productName = PlayerSettings.productName;
			companyName = PlayerSettings.companyName;
#if UNITY_5_6_OR_NEWER
			applicationIdentifier = PlayerSettings.GetApplicationIdentifier(buildTargetGroup);
#else
			applicationIdentifier = PlayerSettings.bundleIdentifier;
#endif
			version = PlayerSettings.bundleVersion;

			androidSettings.Reset();
			iosSettings.Reset();
		}

		public void ApplySettings()
		{
#if UNITY_5_6_OR_NEWER
			PlayerSettings.SetApplicationIdentifier(buildTargetGroup, applicationIdentifier);
#else
			PlayerSettings.bundleIdentifier = applicationIdentifier;
#endif
			PlayerSettings.productName = productName;
			PlayerSettings.companyName = companyName;

			EditorUserBuildSettings.development = developmentBuild;
			EditorUserBuildSettings.allowDebugging = developmentBuild;

			PlayerSettings.bundleVersion = version;
			string buildNumber;
			if (developmentBuild && Util.executeArguments.TryGetValue(Util.OPT_DEV_BUILD_NUM, out buildNumber) && !string.IsNullOrEmpty(buildNumber))
				PlayerSettings.bundleVersion += "." + buildNumber;
			File.WriteAllText(Path.Combine(Util.projectDir, "BUILD_VERSION"), PlayerSettings.bundleVersion);

			// Scene Settings.
			EditorBuildSettingsScene[] buildSettingsScenes = EditorBuildSettings.scenes;
			for (int i = 0; i < buildSettingsScenes.Length;i++)
			{
				var scene = buildSettingsScenes[i];
				var setting = scenes.FirstOrDefault(x =>x.name == Path.GetFileName(scene.path));
				if(setting != null)
				{
					scene.enabled = setting.enable;
				}
					
				buildSettingsScenes[i] = scene;
			}
			EditorBuildSettings.scenes = buildSettingsScenes;

			// Build target settings.
			iosSettings.ApplySettings(this);
			androidSettings.ApplySettings(this);

			OnApplySetting();
			AssetDatabase.SaveAssets();
		}

		public bool BuildAssetBundles()
		{
			try
			{
				AssetBundleManifest oldManifest = null;
				var manifestPath = Path.Combine(bundleOutputPath, actualBuildTarget.ToString());
				if(File.Exists(manifestPath))
				{
					var manifestAssetBundle = AssetBundle.LoadFromFile(manifestPath);
					oldManifest = manifestAssetBundle
						? manifestAssetBundle.LoadAsset<AssetBundleManifest>("assetbundlemanifest")
						: null;
				}

				UnityEngine.Debug.Log(kLogType + "BuildAssetBundles is started.");

				Directory.CreateDirectory(bundleOutputPath);
				BuildAssetBundleOptions opt = (BuildAssetBundleOptions)bundleOptions | BuildAssetBundleOptions.DeterministicAssetBundle;
				var newManifest = BuildPipeline.BuildAssetBundles(bundleOutputPath, opt, actualBuildTarget);

				var sb = new StringBuilder(kLogType + "AssetBundle report");
				string[] array;
				if(oldManifest)
				{
					var oldBundles = new HashSet<string>(oldManifest ? oldManifest.GetAllAssetBundles() : new string[]{});
					var newBundles = new HashSet<string>(newManifest.GetAllAssetBundles());

					array = newBundles.Except(oldBundles).ToArray();
					sb.AppendFormat("\n[Added]: {0}\n", array.Length);
					foreach(var bundleName in array)
						sb.AppendLine("  > " + bundleName);

					array = oldBundles.Except(newBundles).ToArray();
					sb.AppendFormat("\n[Deleted]: {0}\n", array.Length);
					foreach(var bundleName in array)
						sb.AppendLine("  > " + bundleName);
					

					array = oldBundles
						.Intersect(newBundles)
						.Where(x=>!Hash128.Equals( oldManifest.GetAssetBundleHash(x), newManifest.GetAssetBundleHash(x)))
						.ToArray();
					sb.AppendFormat("\n[Updated]: {0}\n", array.Length);
					foreach(var bundleName in array)
						sb.AppendLine("  > " + bundleName);
				}
				else
				{
					array = newManifest.GetAllAssetBundles();
					sb.AppendFormat("\n[Added]: {0}\n", array.Length);
					foreach(var bundleName in array)
						sb.AppendLine("  > " + bundleName);
				}
				UnityEngine.Debug.Log(sb);


				if (copyToStreamingAssets)
				{
					var copyPath = Path.Combine(Application.streamingAssetsPath, bundleOutputPath);
					Directory.CreateDirectory(copyPath);

					if (Directory.Exists(copyPath))
						FileUtil.DeleteFileOrDirectory(copyPath);
					
					FileUtil.CopyFileOrDirectory(bundleOutputPath, copyPath);
				}
				UnityEngine.Debug.Log(kLogType + "BuildAssetBundles is finished successfuly.");
				return true;
			}
			catch(System.Exception e)
			{
				UnityEngine.Debug.LogError(kLogType + "BuildAssetBundles is failed : " + e.Message);
				return false;
			}
		}

		public bool BuildPlayer(bool autoRunPlayer)
		{
			if (buildAssetBundle && !BuildAssetBundles())
			{
				return false;
			}

			if (buildApplication)
			{
                string outputFolder = GetOutputFolder();

                if (string.IsNullOrEmpty(outputFolder))
                {
                    return false;
                }

                // Exclude directories.
                foreach (var dir in excludeDirectories)
					Util.ExcludeDirectory(dir);

				// Build options.
				BuildOptions opt = developmentBuild ? (BuildOptions.Development & BuildOptions.AllowDebugging) : BuildOptions.None
				                   | (autoRunPlayer ? BuildOptions.AutoRunPlayer : BuildOptions.None);

				// Scenes to build.
				string[] scenesToBuild = EditorBuildSettings.scenes.Where(x => x.enabled).Select(x => x.path).ToArray();
				UnityEngine.Debug.Log(kLogType + "Scenes to build : " + scenesToBuild.Aggregate((a,b)=>a+", "+b));

				
				UnityEngine.Debug.Log(kLogType + "BuildPlayer is started. Defined symbols : " + PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup));
				var report = BuildPipeline.BuildPlayer(scenesToBuild, outputFolder, actualBuildTarget, opt);

				// Revert excluded directories.
				Util.RevertExcludedDirectory();

                switch (report.summary.result)
                {
                    case UnityEditor.Build.Reporting.BuildResult.Succeeded:
                        Debug.Log($"<color=green>Build Succeeded!</color> Build time: {report.summary.totalTime.ToString()}"); 
                        break;
                    default:
                        Debug.LogError("Build failed!");
                        break;
                }
            }
			return true;
		}

		static void Build()
		{
			Util.StartBuild(Util.GetBuilderFromExecuteArgument(), false, false);
		}

		#if UNITY_CLOUD_BUILD
		/// <summary>
		/// Pre-export method for Unity Cloud Build.
		/// </summary>
		static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
		{
			Util.executeArguments[Util.OPT_DEV_BUILD_NUM] = manifest.GetValue("buildNumber", "unknown");
			Util.GetBuilderFromExecuteArgument().ApplySettings();
		}
#endif
	}
}