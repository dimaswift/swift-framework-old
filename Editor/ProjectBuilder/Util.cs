using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System;
using System.Reflection;
using UnityEditor.Callbacks;
using System.Text;


namespace SwiftFramework.EditorUtils
{

	internal class Util : ScriptableSingleton<Util>
	{
		const string CUSTOM_BUILDER_TEMPLATE = "ProjectBuilderTemplate";
		const string EXCLUDE_BUILD_DIR = "__ExcludeBuild";

		public const string OPT_BUILDER = "-builder";
		public const string OPT_CLOUD_BUILDER = "-bvrbuildtarget";
		public const string OPT_APPEND_SYMBOL = "-appendSymbols";
		public const string OPT_OVERRIDE = "-override";

		public const string OPT_DEV_BUILD_NUM = "-devBuildNumber";

		public static readonly Dictionary<string,string> executeArguments = new Dictionary<string, string>();

		public static readonly string projectDir = Environment.CurrentDirectory.Replace('\\', '/');

		public static readonly Type builderType = AppDomain.CurrentDomain.GetAssemblies()
			.SelectMany(x => x.GetTypes())
			.FirstOrDefault(x => x.IsSubclassOf(typeof(ProjectBuilder)))
		                                          ?? typeof(ProjectBuilder);
		
		public static readonly MethodInfo miSetIconForObject = typeof(EditorGUIUtility).GetMethod("SetIconForObject", BindingFlags.Static | BindingFlags.NonPublic);


		public static ProjectBuilder currentBuilder { get { return instance.m_CurrentBuilder; } private set { instance.m_CurrentBuilder = value; } }

		[SerializeField] ProjectBuilder m_CurrentBuilder;

		/// <summary>On finished compile callback.</summary>
		[SerializeField] bool m_BuildAndRun = false;
		[SerializeField] bool m_BuildAssetBundle = false;


		[InitializeOnLoadMethod]
		static void InitializeOnLoadMethod()
		{
			// Get command line options from arguments.
			string argKey = "";
			foreach (string arg in System.Environment.GetCommandLineArgs())
			{
				if (arg.IndexOf('-') == 0)
				{
					argKey = arg;
					executeArguments[argKey] = "";
				}
				else if (0 < argKey.Length)
				{
					executeArguments[argKey] = arg;
					argKey = "";
				}
			}

			// When custom builder script exist, convert all builder assets.
			EditorApplication.delayCall += UpdateBuilderAssets;
		}

		/// <summary>Update builder assets.</summary>
		static void UpdateBuilderAssets()
		{
			MonoScript builderScript = Resources.FindObjectsOfTypeAll<MonoScript>()
				.FirstOrDefault(x => x.GetClass() == builderType);
			
			Texture2D icon = GetAssets<Texture2D>(typeof(ProjectBuilder).Name + " Icon")
				.FirstOrDefault();

			// 
			if (builderType == typeof(ProjectBuilder))
				return;

			// Set Icon
			if (icon && builderScript && miSetIconForObject != null)
			{
				miSetIconForObject.Invoke(null, new object[] { builderScript, icon });
				EditorUtility.SetDirty(builderScript);
			}

			// Update script reference for builders.
			foreach (var builder in GetAssets<ProjectBuilder>())
			{
				// Convert 'm_Script' to custom builder script.
				var so = new SerializedObject(builder);
				so.Update();
				so.FindProperty("m_Script").objectReferenceValue = builderScript;
				so.ApplyModifiedProperties();
			}

			AssetDatabase.Refresh();
		}

		public static IEnumerable<T> GetAssets<T>(string name = "") where T : UnityEngine.Object
		{
			return AssetDatabase.FindAssets(string.Format("t:{0} {1}", typeof(T).Name, name))
				.Select(x => AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(x), typeof(T)) as T);
		}

		public static ProjectBuilder GetBuilderFromExecuteArgument()
		{
			string name;
			var args = executeArguments;

			if(args.TryGetValue(Util.OPT_CLOUD_BUILDER, out name))
			{
				name = name.Replace("-", " ");
			}
			else if (!args.TryGetValue(Util.OPT_BUILDER, out name))
			{
				throw new UnityException(ProjectBuilder.kLogType + "Error : You need to specify the builder as follows. '-builder <builder asset name>'");
			}

			ProjectBuilder builder = GetAssets<ProjectBuilder>(name).FirstOrDefault();

			if (!builder)
			{
				throw new UnityException(ProjectBuilder.kLogType + "Error : The specified builder could not be found. " + name);
			}
			else if (builder.actualBuildTarget != EditorUserBuildSettings.activeBuildTarget)
			{
				throw new UnityException(ProjectBuilder.kLogType + "Error : The specified builder's build target is not " + EditorUserBuildSettings.activeBuildTarget);
			}
			else
			{
				UnityEngine.Debug.Log(ProjectBuilder.kLogType + "Builder selected : " + builder);
			}


			string json;
			if (args.TryGetValue(Util.OPT_OVERRIDE, out json))
			{
				UnityEngine.Debug.Log(ProjectBuilder.kLogType + "Override builder with json as following\n" + json);
				JsonUtility.FromJsonOverwrite(json, builder);
			}
			return builder;
		}

		public static ProjectBuilder CreateBuilderAsset()
		{
			if (!Directory.Exists("Assets/Editor"))
				AssetDatabase.CreateFolder("Assets", "Editor");

			// Open save file dialog.
			string filename = AssetDatabase.GenerateUniqueAssetPath(string.Format("Assets/Editor/Default {0}.asset", EditorUserBuildSettings.activeBuildTarget));
			string path = EditorUtility.SaveFilePanelInProject("Create New Builder Asset", Path.GetFileName(filename), "asset", "", "Assets/Editor");
			if (path.Length == 0)
				return null;

			// Create and save a new builder asset.
			ProjectBuilder builder = ScriptableObject.CreateInstance(builderType) as ProjectBuilder;
			AssetDatabase.CreateAsset(builder, path);
			AssetDatabase.SaveAssets();
			Selection.activeObject = builder;
			return builder;
		}

		/// <summary>
		/// Shows the or create custom builder.
		/// </summary>
		public static void CreateCustomProjectBuilder()
		{
			// Select file name for custom project builder script.
			string path = EditorUtility.SaveFilePanelInProject("Create Custom Project Builder", "CustomProjectBuilder", "cs", "", "Assets/Editor");
			if (string.IsNullOrEmpty(path))
				return;
			
			// Create new custom project builder script from template.
			string templatePath = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets(CUSTOM_BUILDER_TEMPLATE + " t:TextAsset").First());
			typeof(ProjectWindowUtil).GetMethod("CreateScriptAssetFromTemplate", BindingFlags.Static | BindingFlags.NonPublic)
				.Invoke(null, new object[]{ path, templatePath });

			// Ping the script asset.
			AssetDatabase.Refresh();
			EditorGUIUtility.PingObject(AssetDatabase.LoadAssetAtPath<TextAsset>(path));
		}


		public static void RevealOutputInFinder(string path)
		{
			if (InternalEditorUtility.inBatchMode)
				return;

			var parent = Path.GetDirectoryName(path);
			EditorUtility.RevealInFinder(
				(Directory.Exists(path) || File.Exists(path)) ? path : 
				(Directory.Exists(parent) || File.Exists(parent)) ? parent :
				projectDir
			);
		}

		public static void StartBuild(ProjectBuilder builder, bool buildAndRun, bool buildAssetBundle)
		{
			currentBuilder = builder;
			instance.m_BuildAndRun = buildAndRun;
			instance.m_BuildAssetBundle = buildAssetBundle;
			ResumeBuild(true);
			
		}

		public static void ResumeBuild(bool compileSuccessfully)
		{
			bool success = false;
			try
			{
				EditorUtility.ClearProgressBar();
				if (compileSuccessfully && currentBuilder)
				{
					currentBuilder.ApplySettings();
					if(instance.m_BuildAssetBundle)
						success = currentBuilder.BuildAssetBundles();
					else
						success = currentBuilder.BuildPlayer(instance.m_BuildAndRun);
				}
			}
			catch (Exception ex)
			{
				Debug.LogException(ex);
			}

			if (Util.executeArguments.ContainsKey("-batchmode"))
			{
				EditorApplication.Exit(success ? 0 : 1);
			}
		}

		public static void ExcludeDirectory (string dir) {
			DirectoryInfo d = new DirectoryInfo (dir);
			if (!d.Exists)
				return;

			if (!Directory.Exists (EXCLUDE_BUILD_DIR))
				Directory.CreateDirectory (EXCLUDE_BUILD_DIR);
			MoveDirectory (d.FullName, EXCLUDE_BUILD_DIR + "/" + dir.Replace ("\\", "/").Replace ("/", "~~"));

			AssetDatabase.Refresh();
		}

		[InitializeOnLoadMethod]
		public static void RevertExcludedDirectory () {
			DirectoryInfo exDir = new DirectoryInfo (EXCLUDE_BUILD_DIR);
			if (!exDir.Exists)
				return;

			foreach (DirectoryInfo d in exDir.GetDirectories()) 
				MoveDirectory (d.FullName, d.Name.Replace ("~~", "/"));

			foreach (FileInfo f in exDir.GetFiles())
				f.Delete (); 

			exDir.Delete ();
			AssetDatabase.Refresh();
		}

		static void MoveDirectory (string from, string to) {
			Directory.Move (from, to);
			if (File.Exists (from + ".meta"))
				File.Move (from + ".meta", to + ".meta");
		}

	}
}
