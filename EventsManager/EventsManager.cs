﻿using SwiftFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SwiftFramework.EventsManager
{
    public class EventsManager : Module, IEventManager
    {
        public override bool LoadModuleWithAppStart => true;

        public bool IsPointerHandledByUI
        {
            get
            {
                if (Input.touchSupported)
                {
                    return Input.touchCount > 0 && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
                }
                else
                {
                    return UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
                }
            }
        }

    }

}
