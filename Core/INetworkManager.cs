﻿using SwiftFramework.Core.Pooling;

namespace SwiftFramework.Core
{
    public interface INetworkManager : IModule
    {
        IPromise<string> Get(string url, int timeoutSeconds = 5);
    }
}
