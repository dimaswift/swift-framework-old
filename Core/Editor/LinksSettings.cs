﻿using UnityEngine;
namespace SwiftFramework.Core.Editor
{
    public class LinksSettings : ScriptableObject
    {
        public bool generateOnReloadScripts = false;
        public string linksFolder = "Scripts/Links";
        public string linksNamespace = "MyNameSpace";
    }

}
