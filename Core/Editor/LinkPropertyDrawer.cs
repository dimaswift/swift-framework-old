﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;
using System.CodeDom;
using System.CodeDom.Compiler;

namespace SwiftFramework.Core.Editor
{
    public class ResourcesProvider : AssetPostprocessor
    {
        public static event System.Action OnResourcesReloaded = () => { };

        static readonly List<AssetPathPair> allAssets = new List<AssetPathPair>();
        private static readonly List<string> stringBuffer = new List<string>(100);

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            Reload();
        }

        internal class AssetPathPair
        {
            public string Path;
            public Object Asset;

            public AssetPathPair(string path, Object asset)
            {
                Path = path;
                Asset = asset;
            }
        }

        private static void GetResourcesDirectories(List<string> directories)
        {
            var root = Application.dataPath;

            var dirs = Directory.GetDirectories(root);

            foreach (string dir in dirs)
            {
                LoadSubDirs(dir, directories);
            }
        }

        private static void LoadSubDirs(string dir, List<string> directories)
        {
            directories.Add(Path.GetFullPath(dir));
            var subDirs = Directory.GetDirectories(dir);
            foreach (string subDir in subDirs)
            {
                LoadSubDirs(subDir, directories);
            }
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        [MenuItem("SwiftFramework/Links/Reload Assets")]
        public static void Reload()
        {
            stringBuffer.Clear();

            allAssets.Clear();

            GetResourcesDirectories(stringBuffer);

            for (int i = stringBuffer.Count - 1; i >= 0; i--)
            {
                var folder = stringBuffer[i];
                if (folder.EndsWith("Resources"))
                {
                    var subFolders = AssetDatabase.GetSubFolders(ToLocalPath(folder));
                    GetAssetsPaths("", folder);
                    foreach (var subFolder in subFolders)
                    {
                        GetAssetsPaths(Path.GetFileName(subFolder), folder);
                    }
                }
            }

            OnResourcesReloaded();
        }

    
        public static IEnumerable<string> GetResources<T>(string rootFolder)
        {
            foreach (var asset in allAssets)
            {
                if ((string.IsNullOrEmpty(rootFolder) || asset.Path.StartsWith(rootFolder + "/")))
                {
                    GameObject gameObject = asset.Asset as GameObject;
                    if (gameObject != null)
                    {
                        if (typeof(MonoBehaviour).IsAssignableFrom(typeof(T)))
                        {
                            if (gameObject.GetComponent<T>() != null)
                                yield return asset.Path;
                        }
                        else if (asset.Asset is T)
                        {
                            yield return asset.Path;
                        }
                    }
                    else
                    {
                        if (asset.Asset is T)
                            yield return asset.Path;
                    }
                }
            }
        }

        static string ToLocalPath(string absolutePath)
        {
            return absolutePath.Remove(0, Application.dataPath.Length - 6);
        }

        static string ToAbsolutePath(string localPath)
        {
            return Application.dataPath + localPath;
        }

        public static void GetAssetsPaths(string folder, string resourcesFolder)
        {
            var rootFolder = resourcesFolder + "/" + folder;

            if (!Directory.Exists(rootFolder))
                return;
            var assets = Directory.GetFiles(rootFolder);

            foreach (var a in assets)
            {
                if (a.EndsWith(".meta"))
                    continue;
                var p = Path.GetFullPath(a);
                var assetPath = "Assets" + p.Remove(0, Application.dataPath.Length);
                var ext = Path.GetExtension(assetPath);
                assetPath = assetPath.Remove(assetPath.Length - ext.Length, ext.Length);

                var asset = AssetDatabase.LoadMainAssetAtPath(assetPath + ext);

                assetPath = Path.GetFileName(assetPath);

                allAssets.Add(new AssetPathPair(string.IsNullOrEmpty(folder) ? assetPath : folder + "/" + assetPath, asset));
            }

            var fullFolder = "Assets" + resourcesFolder.Remove(0, Application.dataPath.Length) + "/" + folder;

            foreach (var sub in AssetDatabase.GetSubFolders(fullFolder))
            {
                var subName = Path.GetFileName(sub);
                GetAssetsPaths(folder + "/" + subName, resourcesFolder);
            }
        }
    }

    public abstract class LinkPropertyDrawer<V, L> : PropertyDrawer
    {
        public string ResourcesFolderName
        {
            get
            {
                foreach (var a in typeof(L).GetCustomAttributes<LinkFolderAttribute>())
                {
                    return a.folder;
                }
                return "";
            }
        }

        public virtual Texture2D Icon => null;

        private static readonly List<string> paths = new List<string>();
        private static string[] pathsArray = new string[0];

        private void UpdatePaths()
        {
            paths.Clear();
            foreach (var path in ResourcesProvider.GetResources<V>(ResourcesFolderName))
            {
                paths.Add(path);
            }
            paths.Insert(0, Link.NULL);
            pathsArray = new string[paths.Count];
            for (int i = 0; i < paths.Count; i++)
            {
                if (paths[i] == Link.NULL)
                {
                    pathsArray[i] = "None";
                    continue;
                }
                pathsArray[i] = ResourcesFolderName.Length > 0 ? paths[i].Remove(0, ResourcesFolderName.Length + 1) : paths[i];
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            const float labelWidth = 100f;
            EditorGUI.LabelField(new Rect(position.x, position.y, labelWidth, position.height), label);
            EditorGUI.BeginProperty(position, new GUIContent(), property);
        
            position.x += labelWidth;

            position.width -= labelWidth;

            var indent = EditorGUI.indentLevel;

            EditorGUI.indentLevel = 1;
            const float buttomWidth = 50;
            var rect = new Rect(position.x, position.y, position.width - buttomWidth * 2, position.height);

            var currentPath = property.FindPropertyRelative("Path");

            if (paths.Count == 0)
            {
                ResourcesProvider.OnResourcesReloaded -= UpdatePaths;
                ResourcesProvider.OnResourcesReloaded += UpdatePaths;
                UpdatePaths();
            }

            var path = currentPath.stringValue;

            if (string.IsNullOrEmpty(path))
            {
                path = Link.NULL;
            }

            var currentIndex = paths.FindIndex(f => f == path);
          
            var buttonRect = new Rect(position.x + position.width - buttomWidth * 2, position.y, buttomWidth, position.height);

            if (currentIndex < 0)
            {
                rect.width += buttomWidth;
                buttonRect.x += buttomWidth;
                EditorGUI.HelpBox(rect, $"{ResourcesFolderName}/{path}", MessageType.Error);
                if (GUI.Button(buttonRect, "Reset"))
                {
                    ResourcesProvider.Reload();
                    currentIndex = 0;
                    currentPath.stringValue = paths[0];
                }
                
                return;
            }

            if(path != Link.NULL)
            {
                var obj = Resources.Load(path);

                if (obj != null)
                {
                    if (GUI.Button(buttonRect, "Select"))
                        Selection.activeObject = obj;
                    buttonRect.x += buttomWidth;
                    if (GUI.Button(buttonRect, "Ping"))
                        EditorGUIUtility.PingObject(obj);
                }
                else
                {
                    EditorGUI.HelpBox(rect, $"Object with link not found {path}", MessageType.Error);

                    if (paths.Count > 0 && GUI.Button(buttonRect, "Reset"))
                    {
                        currentIndex = 0;
                        foreach (var p in paths)
                        {
                            if (Resources.Load(p, typeof(V)) || (Resources.Load<GameObject>(p) != null && Resources.Load<GameObject>(p).GetComponent<V>() != null))
                            {
                                currentPath.stringValue = p;
                                break;
                            }
                            currentIndex++;
                        }
                    }
                    else
                    {
                        EditorGUI.indentLevel = indent;
                        EditorGUI.EndProperty();
                        return;
                    }
                }
            }
            else
            {
                rect.width = position.width;
            }

  
            var selectedIndex = EditorGUI.Popup(rect, currentIndex, pathsArray);

            if(selectedIndex != currentIndex)
            {
                currentPath.stringValue = paths[selectedIndex].StartsWith("/") ? paths[selectedIndex].Remove(0, 1) : paths[selectedIndex];
            }


            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}



