﻿using UnityEditor;
using UnityEngine;

namespace SwiftFramework.Core.Editor
{
    [CustomPropertyDrawer(typeof(ModuleLink))]
    public class ModuleLinkDrawer : LinkPropertyDrawer<Module, ModuleLink>
    {
    }

    [CustomPropertyDrawer(typeof(AudioClipLink))]
    public class AudioClipLinkDrawer : LinkPropertyDrawer<AudioClip, AudioClipLink>
    {
    }
}
