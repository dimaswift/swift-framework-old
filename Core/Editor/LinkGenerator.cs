﻿using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SwiftFramework.Core.Editor
{
    public class LinkGenerator : UnityEditor.Editor
    {
        private static LinksSettings settings;

        private static LinksSettings Settings
        {
            get
            {
                if(settings == null)
                {
                    string path = $"Assets/{nameof(LinksSettings)}.asset";
                    settings = AssetDatabase.LoadAssetAtPath<LinksSettings>(path);
                    if(settings == null)
                    {
                        settings = CreateInstance<LinksSettings>();
                        settings.name = nameof(LinksSettings);
                        AssetDatabase.CreateAsset(settings, path);
                    }
                }
                return settings;
            }
        }

        [MenuItem("SwiftFramework/Links/Settings")]
        public static void SelectSettings()
        {
            Selection.activeObject = Settings;
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        private static void AutoGenerate()
        {
            if (Settings.generateOnReloadScripts && EditorApplication.isCompiling == false)
            {
                GenerateLinkClasses(false);
            }
        }
        
        [MenuItem("SwiftFramework/Links/Generate Links")]
        public static void GenerateLinks()
        {
            GenerateLinkClasses(true);
        }

        private static void GenerateLinkClasses(bool force)
        {
            List<System.Type> types = new List<System.Type>();

            foreach (var a in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in a.DefinedTypes)
                {
                    if (type.GetCustomAttribute<GenerateLinkAttribute>() != null)
                    {
                        types.Add(type);
                    }
                }
            }

            CodeCompileUnit linkFile = new CodeCompileUnit();
            CodeCompileUnit linkDrawerFile = new CodeCompileUnit();

            CodeNamespace linksClass = new CodeNamespace(Settings.linksNamespace);
            CodeNamespace drawersClass = new CodeNamespace(Settings.linksNamespace + ".Editor");

            drawersClass.Imports.Add(new CodeNamespaceImport("SwiftFramework.Core.Editor"));
            drawersClass.Imports.Add(new CodeNamespaceImport("UnityEditor"));

            linksClass.Imports.Add(new CodeNamespaceImport("SwiftFramework.Core"));
            linksClass.Imports.Add(new CodeNamespaceImport("System"));

            if(types.Count == 0)
            {
                return;
            }

            foreach (var type in types)
            {
                linksClass.Imports.Add(new CodeNamespaceImport(type.Namespace));
                drawersClass.Imports.Add(new CodeNamespaceImport(type.Namespace));
     
                GenerateLinkAttribute attr = type.GetCustomAttribute<GenerateLinkAttribute>();

                string linkName = attr.customName != null ? attr.customName : $"{type.Name}Link";
                var linkClass = new CodeTypeDeclaration(linkName);
                linkClass.IsClass = true;
                string linkType = type.IsInterface ? "LinkToInterface" : "LinkTo";
              
                var baseLinkClass = new CodeTypeReference($"{linkType}<{type.Name}>", CodeTypeReferenceOptions.GenericTypeParameter);
                linkClass.BaseTypes.Add(baseLinkClass);

                linkClass.TypeAttributes = TypeAttributes.Public;
                linkClass.CustomAttributes.Add(new CodeAttributeDeclaration("Serializable"));

                if(attr.folder != null)
                {
                    linkClass.CustomAttributes.Add(new CodeAttributeDeclaration("LinkFolderAttribute", new CodeAttributeArgument(new CodePrimitiveExpression(attr.folder))));
                }

                linksClass.Types.Add(linkClass);


                var linkDrawerClass = new CodeTypeDeclaration($"{linkName}Drawer");

                linkDrawerClass.BaseTypes.Add(new CodeTypeReference($"LinkPropertyDrawer<{type.Name}, {linkName}>"));

                var drawerAttr = new CodeAttributeDeclaration("CustomPropertyDrawer");

                drawerAttr.Arguments.Add(new CodeAttributeArgument(new CodeTypeOfExpression(new CodeTypeReference($"{settings.linksNamespace}.{linkName}"))));

                linkDrawerClass.CustomAttributes.Add(drawerAttr);

                drawersClass.Types.Add(linkDrawerClass);
            }

            linkFile.Namespaces.Add(linksClass);
            linkDrawerFile.Namespaces.Add(drawersClass);


            SaveToDisc(linkFile, Path.Combine(Application.dataPath, Settings.linksFolder, "Links.cs"), force);
            SaveToDisc(linkDrawerFile, Path.Combine(Application.dataPath, Settings.linksFolder, "Editor/LinkDrawers.cs"), force);
        }

        private static void SaveToDisc(CodeCompileUnit code, string path, bool force)
        {
            CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
            CodeGeneratorOptions options = new CodeGeneratorOptions();
            options.BracingStyle = "C";
            
            string tmpLinksScript = Path.GetTempFileName();

            StreamWriter sourceWriter = new StreamWriter(tmpLinksScript);
            provider.GenerateCodeFromCompileUnit(code, sourceWriter, options);
            sourceWriter.Close();

            bool updated = false;

            if (force || File.Exists(path) == false || File.ReadAllText(tmpLinksScript) != File.ReadAllText(path))
            {
                File.WriteAllText(path, File.ReadAllText(tmpLinksScript));
                updated = true;
            }

            File.Delete(tmpLinksScript);

            if (updated || force)
            {
                AssetDatabase.Refresh();
            }
        }
    }

}
