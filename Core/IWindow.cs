﻿using UnityEngine;

namespace SwiftFramework.Core
{
    public interface IWindow
    {
        void Show();
        void Hide();
        bool IsFullScreen { get; }
        bool IsShown { get; }
        RectTransform RectTransform { get; }
        IPromise HidePromise { get; }
    }
}
