using UnityEngine;
using System.Numerics;
using System;
using System.Collections.Generic;

namespace SwiftFramework.Core
{
    [Serializable]
    public struct BigNumber : ISerializationCallbackReceiver
    {
        [SerializeField] private string stringValue;

        private BigInteger? bigValue;

        public BigInteger Value
        {
            get
            {
                if (bigValue.HasValue == false)
                {
                    if (string.IsNullOrEmpty(stringValue))
                    {
                        bigValue = 0;
                        stringValue = bigValue.Value.ToString();
                        return 0;
                    }
                    if (BigInteger.TryParse(stringValue, out BigInteger v))
                    {
                        bigValue = v;
                    }
                    else
                    {
                        Debug.LogError($"Cannot parse big int {stringValue}");
                        bigValue = 0;
                    }
                }
                return bigValue.Value;
            }
            set
            {
                if(this.bigValue != value)
                {
                    this.bigValue = value;
#if UNITY_EDITOR
                    stringValue = value.ToString();
#endif
                }
            }
        }

        public override string ToString()
        {
            return Value.ToPrettyString();
        }

        public void OnBeforeSerialize()
        {
            if(bigValue.HasValue)
            {
                stringValue = bigValue.ToString();
            }
        }

        public void OnAfterDeserialize()
        {
            BigInteger res = new BigInteger();
            if (BigInteger.TryParse(stringValue, out res))
            {
                bigValue = res;
            }
            else bigValue = 0;
        }

        public BigNumber(string numberString)
        {
            if (BigInteger.TryParse(numberString, out BigInteger v) == false)
            {
                Debug.LogError($"Cannot parse big int {numberString}");
                numberString = "0";
            }
            bigValue = v;
            stringValue = numberString;
        }

        public BigNumber(BigInteger number)
        {
            stringValue = number.ToString();
            bigValue = number;
        }

        public BigNumber(int number)
        {
            stringValue = number.ToString();
            bigValue = number;
        }

        public static implicit operator BigNumber(int value)
        {
            return new BigNumber(value);
        }

        public static implicit operator BigNumber(uint value)
        {
            return new BigNumber(value);
        }

        public static implicit operator BigNumber(BigInteger value)
        {
            return new BigNumber(value);
        }

        public static implicit operator BigNumber(string value)
        {
            return new BigNumber(value);
        }

        public static BigNumber operator +(BigNumber v1, BigInteger v2)
        {
            v1.Value += v2;
            return v1;
        }

        public static BigNumber operator -(BigNumber v1, BigInteger v2)
        {
            v1.Value -= v2;
            return v1;
        }

        public static BigNumber operator *(BigNumber v1, BigInteger v2)
        {
            v1.Value *= v2;
            return v1;
        }

        public static BigNumber operator /(BigNumber v1, BigInteger v2)
        {
            v1.Value /= v2;
            return v1;
        }
    }

    public static class BigNumberExtentions
    {
        private static readonly Dictionary<BigInteger, string> cachedBigIntegers = new Dictionary<BigInteger, string>(100);

        private static string GetStringModifier(int numberOfThousands)
        {
            string res = "";
            switch (numberOfThousands)
            {
                case 1:
                    break;
                case 2:
                    res = "K";
                    break;

                case 3:
                    res = "M";
                    break;

                case 4:
                    res = "B";
                    break;

                case 5:
                    res = "T";
                    break;

                default:
                    char symbol = Convert.ToChar((int)'a' + (numberOfThousands - 6));
                    if (symbol > 'z')
                    {
                        symbol = Convert.ToChar((int)'A' + (numberOfThousands - ((int)'z' - (int)'a' + 7)));
                        res = DuplicateSymbol(symbol, 3);
                    }
                    else
                    {
                        res = DuplicateSymbol(symbol, 2);
                    }
                    break;
            }

            return res;
        }

        private static string DuplicateSymbol(char symbol, int numberOfTimes)
        {
            string result = string.Empty;
            for (int i = 0; i < numberOfTimes; i++)
            {
                result = string.Format("{0}{1}", result, symbol);
            }
            return result;
        }

        public static string ToPrettyFracturedString(this BigInteger value, int symbolsAfterComa = 1)
        {
            if (cachedBigIntegers.ContainsKey(value))
            {
                return cachedBigIntegers[value];
            }

            var str = value.ToString();

            var length = str.Length;

            if (length < 4)
            {
                return str;
            }

            var integerPartLength = length % 3;
            if (integerPartLength == 0)
                integerPartLength = 3;

            var numberOfThousands = Mathf.CeilToInt(length / 3.0f);

            var integerPart = str.Substring(0, integerPartLength);

            var fractionalPart = str.Substring(integerPartLength, symbolsAfterComa);

            var fractional = int.Parse(fractionalPart);

            string res = fractional == 0 ? $"{integerPart}{GetStringModifier(numberOfThousands)}"
                : $"{integerPart},{fractionalPart}{GetStringModifier(numberOfThousands)}";

            cachedBigIntegers.Add(value, res);

            return res;

        }

        public static string ToPrettyString(this BigInteger value)
        {
            if (cachedBigIntegers.ContainsKey(value))
            {
                return cachedBigIntegers[value];
            }

            var str = value.ToString();

            var length = str.Length;

            if (length < 4)
            {
                return str;
            }

            var integerPartLength = length % 3;
            if (integerPartLength == 0)
                integerPartLength = 3;

            var numberOfThousands = Mathf.CeilToInt(length / 3.0f);

            var integerPart = str.Substring(0, integerPartLength);
            var fractionalPart = str.Substring(integerPartLength, 2);

            var fractional = int.Parse(fractionalPart);
            string res;

            res = fractional == 0 ? $"{integerPart}{GetStringModifier(numberOfThousands)}"
                : $"{integerPart},{fractionalPart}{GetStringModifier(numberOfThousands)}";

            cachedBigIntegers.Add(value, res);

            return res;
        }

        public static string ToFracturedPrettyString(this BigInteger value)
        {
            if (cachedBigIntegers.ContainsKey(value))
            {
                return cachedBigIntegers[value];
            }

            var str = value.ToString();

            var length = str.Length;

            if (length < 4)
            {
                cachedBigIntegers.Add(value, str);
                return str;
            }

            var integerPartLength = length % 3;
            if (integerPartLength == 0)
                integerPartLength = 3;

            var numberOfThousands = Mathf.CeilToInt(length / 3.0f);

            var integerPart = str.Substring(0, integerPartLength);
            var fractionalPart = str.Substring(integerPartLength, 1);

            var fractional = int.Parse(fractionalPart);
            string res;
            res = fractional == 0 ? $"{integerPart},0{GetStringModifier(numberOfThousands)}"
                : $"{integerPart},{fractionalPart}{GetStringModifier(numberOfThousands)}";

            cachedBigIntegers.Add(value, res);

            return res;
        }

        public static BigInteger MultiplyByFloat(this BigInteger bigInt, float value, int precision = 1000)
        {
            if (value == 0)
                return 0;
            var multiplierPercent = (int)(value * precision);
            bigInt *= multiplierPercent;
            return bigInt / precision;
        }

        public static BigInteger MultiplyByDouble(this BigInteger bigInt, double value, int precision = 1000)
        {
            if (value == 0)
                return 0;
            var multiplierPercent = (int)(value * precision);
            bigInt *= multiplierPercent;
            return bigInt / precision;
        }

        public static BigInteger DivideByFloat(this BigInteger bigInt, float value, int precision = 1000)
        {
            var big = bigInt * precision;
            var division = new BigInteger(value * precision);
            if (division == 0)
                division = 1;
            big /= division;

            return big;
        }
    }
}
