﻿using UnityEngine;

namespace SwiftFramework.Core
{
    public interface IViewFactory : IModule
    {
        T CreateView<T>() where T : class, IView;
        T CreateView<T>(T source) where T : class, IView;
        T GetOrCreateView<T>() where T : class, IView;
    }
}
