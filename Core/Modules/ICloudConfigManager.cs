﻿namespace SwiftFramework.Core
{
    public interface ICloudConfigManager : IModule
    {
        IPromise<T> LoadConfig<T>() where T : new();
    }
}
