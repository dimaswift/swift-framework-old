﻿using System;

namespace SwiftFramework.Core
{
    public interface IInAppPurchaseManager : IModule
    {
        IPromise<bool> Buy(string productId);
        IPromise<bool> RestorePurchases();
        string GetPriceString(string productId);
        string GetCurrencyCode(string productId);
        decimal GetPrice(string productId);
        bool IsSubscribed(string productId);

        bool IsAlreadyPurchased(string productId);

        event Action<string> OnItemPurchased;
    }
}
