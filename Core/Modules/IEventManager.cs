﻿namespace SwiftFramework.Core
{
    public interface IEventManager : IModule
    {
        bool IsPointerHandledByUI { get; }


    }

}
