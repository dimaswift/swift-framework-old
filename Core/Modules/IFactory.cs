﻿namespace SwiftFramework.Core
{
    public interface IModuleFactory
    {
        T CreateModule<T>() where T : IModule;
    }
}