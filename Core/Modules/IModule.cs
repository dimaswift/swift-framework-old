﻿namespace SwiftFramework.Core
{
    public interface IModule
    {
        IPromise LoadModule();
        IPromise InitModule(IApp app);
        bool LoadModuleWithAppStart { get; }
        void Unload();
    }
}
