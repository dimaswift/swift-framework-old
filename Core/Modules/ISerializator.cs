﻿using System;

namespace SwiftFramework.Core
{
    public interface ISerializator : IModule
    {
        T Deserialize<T>();

        object Deserialize(Type type);

        void Serialize<T>(T data, bool overwrite = true);

        bool Exists<T>();

        void Delete<T>();
    }
}
