﻿namespace SwiftFramework.Core
{
    public interface IAnalyticsManager : IModule
    {
        void LogEvent(string eventName, params (string key, object value)[] args);
        void LogPurchase(string productId, string currencyCode, decimal price, string transactionId);
        void LogRewardedVideoStarted(string placementId);
        void LogRewardedVideoSuccess(string placementId);
    }
}
