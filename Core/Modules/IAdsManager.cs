﻿using System;

namespace SwiftFramework.Core
{
    public interface IAdsManager : IModule
    {
        float GetTimeSinceAdClosed();
        bool IsShowingRewardedAd();
        event Action<string> OnRewardedShowStarted;
        event Action OnRewardedAdLoaded;
        event Action<string> OnRewardedSuccessfully;
        event Action<string> OnRewardedClosed;
        bool IsRewardedReady();
        IPromise<bool> ShowRewarded(string placementId);
        bool ShowInterstitial(string placementId);
        bool IsInterstitialReady();

        void TryShowInterstitial();
        void ResetInterstitialCounter();
    }
}
