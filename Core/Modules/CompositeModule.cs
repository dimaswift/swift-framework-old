﻿using System.Collections.Generic;

namespace SwiftFramework.Core
{
    public abstract class CompositeModule : Module
    {
        private readonly List<IModule> subModules = new List<IModule>();

        protected override IPromise GetLoadPromise()
        {
            Promise loadPromise = Promise.Create();

            GetComponentsInChildren(subModules);

            subModules.RemoveAll(m => m == GetComponent<IModule>());

            LoadNextModule(0, loadPromise);

            return loadPromise;
        }

        protected IEnumerable<T> GetSubmodules<T>() where T : class, IModule
        {
            foreach (var subModule in subModules)
            {
                if (subModule is T)
                {
                    yield return subModule as T;
                }
            }
        }

        protected T GetSubmodule<T>() where T : class, IModule
        {
            foreach (var subModule in subModules)
            {
                if (subModule is T)
                {
                    return subModule as T;
                }
            }
            return default;
        }

        protected override IPromise GetInitPromise()
        {
            Promise initPromise = Promise.Create();

            InitNextModule(0, initPromise, App);

            return initPromise;
        }

        protected virtual void OnSubModuleInitialized(IModule subModule) { }

        protected virtual void OnSubModuleLoaded(IModule subModule) { }

        private void LoadNextModule(int current, Promise loadPromise)
        {
            if (current >= subModules.Count)
            {
                loadPromise.Resolve();
                return;
            }
            subModules[current].LoadModule().Then(() =>
            {
                OnSubModuleLoaded(subModules[current]);
                LoadNextModule(++current, loadPromise);
            })
            .Catch(e =>
            {
                LoadNextModule(++current, loadPromise);
            });
        }

        private void InitNextModule(int current, Promise iniPromise, IApp app)
        {
            if (current >= subModules.Count)
            {
                iniPromise.Resolve();
                return;
            }
            subModules[current].InitModule(app).Then(() =>
            {
                OnSubModuleInitialized(subModules[current]);
                InitNextModule(++current, iniPromise, app);
            })
            .Catch(e =>
            {
                InitNextModule(++current, iniPromise, app);
            });
        }
    }
}
