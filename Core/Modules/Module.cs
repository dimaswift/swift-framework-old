﻿using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Core
{
    public abstract class Module : MonoBehaviour
    {
        private Promise loadPromise = null;
        private Promise initPromise = null;

        protected IApp App { get; private set; }

        public virtual bool LoadModuleWithAppStart => true;

        protected bool Initialized => initPromise != null && initPromise.CurrentState == PromiseState.Resolved;

        public IPromise InitPromise => initPromise;

        public IPromise InitModule(IApp app)
        {
            App = app;
            if (initPromise != null)
            {
                return initPromise;
            }

            initPromise = GetInitPromise() as Promise;

            initPromise.Done(() =>
            {
                OnInit();
            });

            return initPromise;
        }

        public IPromise LoadModule()
        {
            if(loadPromise != null)
            {
                return loadPromise;
            }
            loadPromise = GetLoadPromise() as Promise;
            loadPromise.Done(OnLoad);
            return loadPromise;
        }

        protected virtual void OnInit() { }

        protected virtual void OnLoad() { }

        protected virtual IPromise GetLoadPromise()
        {
            return Promise.Resolved();
        }

        protected virtual IEnumerable<IModule> GetDependncies()
        {
            yield break;
        }

        protected virtual IPromise GetInitPromise()
        {
            return Promise.Resolved();
        }

        public virtual void Unload()
        {
            Destroy(gameObject);
        }

        protected void LogNotEnabledWarning(string defineSymbol)
        {
            Debug.LogWarning($"{name} is disabled! Add {defineSymbol} define symbol to enable it!");
        }

    }
}
