﻿using UnityEngine;

namespace SwiftFramework.Core
{
    public class DummyModule<T> : IModule
    {
        public bool LoadModuleWithAppStart => false;

        public IPromise InitModule(IApp app)
        {
            return Promise.Resolved();
        }

        public IPromise LoadModule()
        {
            Debug.LogWarning($"Module {typeof(T).Name} not implemented!");
            return Promise.Resolved();
        }

        public void Unload()
        {
            
        }
    }
}
