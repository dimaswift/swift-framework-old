﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

namespace SwiftFramework.Core
{
    public interface IIdleGame : IModule
    {
        void Tick(float delta);
        IStatefulEvent<BigInteger> SoftCurrency { get; }
        IStatefulEvent<BigInteger> HardCurrency { get; }
        IStatefulEvent<BigInteger> ProductionSpeed { get; }
        int FactoryCount { get; }
    }
}
