﻿using System;
using UnityEngine;

namespace SwiftFramework.Core
{
    public interface ILocalizationManager : IModule
    {
        SystemLanguage CurrentLanguage { get; }
        string GetText(string key);
        string GetText(string key, params object[] args);
        event Action OnLanguageChanged;
        void SetLanguage(SystemLanguage language);
    }
}
