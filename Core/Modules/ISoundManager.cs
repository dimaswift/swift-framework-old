﻿using UnityEngine;

namespace SwiftFramework.Core
{
    public interface ISoundManager : IModule
    {
        void PlayOnce(AudioClip clip, float volumeScale = 1f);
        void PlayLoop(AudioClip clip, float volumeScale = 1f);
        void StopLoop(AudioClip clip);
        void SetMuted(bool muted);
    }
}
