﻿using System.Collections.Generic;

namespace SwiftFramework.Core
{
    public interface IWindowsManager : IModule
    {
        IWindow Show<T>() where T : IWindow;
        IWindow Show<T, A>(A args) where T : IWindowWithArgs<A>;
        IWindow Hide<T>() where T : IWindow;
        IWindow GetTopWindow();
        IWindow GetBottomWindow();
        IEnumerable<IWindow> GetWindowStack();
        void HideTopFullScreenWindow();
        void HideAll();
        void SetStack(IEnumerable<IWindow> windows);
        void SetStack<W1>() where W1 : IWindow;
        void SetStack<W1, W2>() where W1 : IWindow where W2 : IWindow;
        void SetStack<W1, W2, W3>() where W1 : IWindow where W2 : IWindow where W3 : IWindow;
        void SetStack<W1, W2, W3, W4>() where W1 : IWindow where W2 : IWindow where W3 : IWindow where W4 : IWindow;
        bool IsShowingAnimation();
    }
}
