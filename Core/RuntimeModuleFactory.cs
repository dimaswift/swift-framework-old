﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Core
{
    public class RuntimeModuleFactory : IModuleFactory
    {
        private readonly List<(Type interfaceType, Type implementationType)> modules;

        public RuntimeModuleFactory(params (Type interfaceType, Type implementationType)[] modulePairs)
        {
            modules = new List<(Type interfaceType, Type implementationType)>(modulePairs);
        }

        public RuntimeModuleFactory Add((Type interfaceType, Type implementationType) pair)
        {
            modules.Add(pair);
            return this;
        }

        public T CreateModule<T>() where T : IModule
        {
            foreach (var t in modules)
            {
                if (t.interfaceType == typeof(T))
                {
                    return new GameObject(t.implementationType.Name).AddComponent(t.implementationType).GetComponent<T>();
                }
            }
            return default;
        }
    }
}
