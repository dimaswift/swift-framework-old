﻿using System;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

namespace SwiftFramework.Core
{
    public static class ExtentionMethods
    {
        public static List<T> DeepCopy<T>(this List<T> list) where T : class, IDeepCopy<T>
        {
            List<T> copy = new List<T>(list.Count);
            foreach (var item in list)
            {
                copy.Add(item.DeepCopy());
            }
            return copy;
        }

        public static T Random<T>(this IList<T> list)
        {
            if (list.Count == 0)
            {
                return default;
            }
            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        public static T Random<T>(this T[] list)
        {
            if (list.Length == 0)
            {
                return default;
            }
            return list[UnityEngine.Random.Range(0, list.Length)];
        }

        public static float Remap(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        public static float Remap(this int value, float from1, float to1, float from2, float to2)
        {
            return ((float)value).Remap(from1, to1, from2, to2);
        }

        public static Color SetAlpha(this Color color, float alpha)
        {
            color.a = alpha;
            return color;
        }

        public static void Shuffle<T>(this List<T> list) where T : class
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Shuffle<T>(this T[] list) where T : class
        {
            int n = list.Length;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static float DivideFloat(this BigInteger a, BigInteger b, int precision = 1000)
        {
            if(b == 0)
            {
                return 0;
            }
            var div = BigInteger.Divide(a * precision, b);
            var divInt = (float)div;
            return divInt / precision;
        }

        public static BigInteger BigPow(this float coefficient, BigInteger baseValue, int exponent, int precision = 1000000)
        {
            return baseValue * (BigInteger.Pow((BigInteger)(coefficient * precision), exponent) / BigInteger.Pow(precision, exponent));
        }

        public static IEnumerable<T> GetValues<T>(this IEnumerable<LinkTo<T>> links) where T : UnityEngine.Object
        {
            foreach (var l in links)
            {
                yield return l;
            }
        }
    }
}
