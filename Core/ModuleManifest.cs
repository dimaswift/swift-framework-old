﻿using System.Reflection;
using UnityEngine;

namespace SwiftFramework.Core
{
    [CreateAssetMenu(menuName = "SwiftFramework/Modules/ModuleManifest", fileName = "ModuleManifest")]
    public class ModuleManifest : ScriptableObject, IModuleFactory
    {
        [SerializeField] private ModuleLink adsManager;
        [SerializeField] private ModuleLink analytics;
        [SerializeField] private ModuleLink coroutineManager;
        [SerializeField] private ModuleLink securePrefs;
        [SerializeField] private ModuleLink eventsManager;
        [SerializeField] private ModuleLink cloudConfigManager;
        [SerializeField] private ModuleLink localConfigManager;
        [SerializeField] private ModuleLink localizationManager;
        [SerializeField] private ModuleLink networkManager;
        [SerializeField] private ModuleLink resourcesManager;
        [SerializeField] private ModuleLink serverTime;
        [SerializeField] private ModuleLink inAppManager;
        [SerializeField] private ModuleLink viewFactory;
        [SerializeField] private ModuleLink serializator;
        [SerializeField] private ModuleLink windowsManager;
        [SerializeField] private ModuleLink firebase;
        [SerializeField] private ModuleLink soundManager;

        public T CreateModule<T>() where T : IModule
        {
            GameObject moduleInstancePrefab = null;

            FieldInfo[] fields = GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

            foreach (FieldInfo linkField in fields)
            {
                ModuleLink link = linkField.GetValue(this) as ModuleLink;
            
                if (link != null && link.HasValue && link.Value.GetComponent<T>() != null)
                {
                    moduleInstancePrefab = link.Value.gameObject;
                    break;
                }
            }
            if(moduleInstancePrefab == null)
            {
                Debug.LogError($"Cannot find Unity Module of type {typeof(T).Name}! Add the link to it in LinkedModuleFactory!");
                return default;
            }
           
            GameObject moduleInstanceObject = Instantiate(moduleInstancePrefab);
            DontDestroyOnLoad(moduleInstanceObject.gameObject);
            moduleInstanceObject.name = moduleInstancePrefab.name;
            return moduleInstanceObject.GetComponent<T>();
        }
    }
}
