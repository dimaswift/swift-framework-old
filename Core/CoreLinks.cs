﻿
using UnityEngine;

namespace SwiftFramework.Core
{
    [System.Serializable]
    [LinkFolder("Modules")]
    public class ModuleLink : LinkTo<Module>
    {

    }

    [System.Serializable]
    public class AudioClipLink : LinkTo<AudioClip>
    {

    }
}
