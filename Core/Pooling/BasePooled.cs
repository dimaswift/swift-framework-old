﻿namespace SwiftFramework.Core.Pooling
{
    public abstract class BasePooled : IPooled
    {
        public virtual int InitialPoolCapacity => 100;

        public bool Active { get; set; }

        private IPool pool;

        public void Init(IPool pool)
        {
            this.pool = pool;
        }

        public void ReturnToPool()
        {
            pool.Return(this);
            OnReturnedToPool();
        }

        public void TakeFromPool()
        {
            OnTakenFromPool();
        }

        protected abstract void OnTakenFromPool();
        protected abstract void OnReturnedToPool();

    }
}

