﻿using System;
using System.Collections.Generic;

namespace SwiftFramework.Core.Pooling
{
    public class SimplePool<P> : IPool where P : class, IPooled
    {
        private readonly Stack<P> instances = new Stack<P>();

        private readonly Func<P> instanceHadnler;

        public void Return(IPooled pooledObject)
        {
            instances.Push(pooledObject as P);
        }

        public SimplePool(Func<P> instanceHadnler)
        {
            this.instanceHadnler = instanceHadnler;
        }

        public void WarmUp(int capacity)
        {
            for (int i = 0; i < capacity; i++)
            {
                AddInstanceToPool();
            }
        }

        private void AddInstanceToPool()
        {
            P instance = instanceHadnler();
            instance.Init(this);
            instance.ReturnToPool();
        }

        public T Take<T>() where T : class, IPooled
        {
            if(instances.Count == 0)
            {
                AddInstanceToPool();
            }
            T instance = instances.Pop() as T;
            instance.TakeFromPool();
            return instance;
        }

        public P Take()
        {
            return Take<P>();
        }
    }
}

