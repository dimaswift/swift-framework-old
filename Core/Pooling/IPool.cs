﻿namespace SwiftFramework.Core.Pooling
{
    public interface IPool
    {
        T Take<T>() where T : class, IPooled;
        void Return(IPooled pooledObject);
        void WarmUp(int capacity);
    }
}

