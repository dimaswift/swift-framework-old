﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Core.Pooling
{
    public class Pool : IPool
    {
        private static readonly Dictionary<Type, Pool> pools = new Dictionary<Type, Pool>();

        private readonly Func<IPooled> createInstanceHandler;

        private readonly LinkedList<IPooled> poolStack = new LinkedList<IPooled>();

        private readonly LinkedList<IPooled> activeItems = new LinkedList<IPooled>();

        public Pool(Func<IPooled> createInstanceHandler)
        {
            this.createInstanceHandler = createInstanceHandler;
        }

        public void WarmUp(int capacity)
        {
            for (int i = 0; i < capacity; i++)
            {
                AddInstanceToPool();
            }
        }

        private void AddInstanceToPool()
        {
            IPooled item = createInstanceHandler();
            item.Init(this);
            item.ReturnToPool();
        }

        public IEnumerable<T> GetActiveItems<T>() where T : IPooled
        {
            foreach (var item in activeItems)
            {
                yield return (T) item;
            }
        }

        public IEnumerable<IPooled> GetActiveItems()
        {
            return activeItems;
        }

        public static void Register<T>(Func<T> createInstanceHandler, int capacity) where T : class, IPooled
        {
            Type type = typeof(T);
            if (pools.ContainsKey(type))
            {
                Debug.LogError($"Pool of type {type.Name} is already registered.");
                return;
            }
            Pool pool = new Pool(() => createInstanceHandler());
            pool.WarmUp(capacity);
            pools.Add(type, pool);
        }

        public static T TakeFromPool<T>() where T : class, IPooled
        {
            Type type = typeof(T);
            if(GetPool<T>(out Pool pool) == false)
            {
                return default;
            }
            return pool.Take<T>();
        }

        private static bool GetPool<T>(out Pool pool) where T : class, IPooled
        {
            Type type = typeof(T);
            if (pools.TryGetValue(type, out pool) == false)
            {
                Debug.LogError($"Pool of type {type.Name} is not registered. Use Pool.Register<T>(Func<object> creatorHandler) to register object.");
                return false;
            }
            return true;
        }

        public static void ReturnToPool<T>(T pooledObject) where T : class, IPooled
        {
            if (GetPool<T>(out Pool pool) == false)
            {
                return;
            }
            pool.Return(pooledObject);
        }

        public IPooled Take()
        {
            if (poolStack.Count == 0)
            {
                AddInstanceToPool();
            }
            IPooled item = poolStack.First.Value;
            poolStack.RemoveFirst();
            item.TakeFromPool();
            activeItems.AddFirst(item);
            return item;
        }

        public T Take<T>() where T : class, IPooled
        {
            return Take() as T;
        }

        public void Return(IPooled pooledObject)
        {
            poolStack.AddFirst(pooledObject);
            activeItems.Remove(pooledObject);
        }
    }
}

