﻿using System;

namespace SwiftFramework.Core
{
    public interface ILink
    {
        bool IsEmpty { get; }

        bool HasValue { get; }

        Type ValueType { get; }

        UnityEngine.Object BaseValue { get; }
    }
}
