﻿using System;
using UnityEngine;

namespace SwiftFramework.Core
{
    [Serializable]
    public class Link : ILink, ISerializationCallbackReceiver
    {
        public virtual bool HasValue => Path == NULL || Resources.Load(Path) != null;

        public Type ValueType => string.IsNullOrEmpty(Type) ? null : System.Type.GetType(Type);

        public bool IsEmpty => string.IsNullOrEmpty(Path) || Path == NULL;

        [NonSerialized] protected int cachedHash = 0;

        [NonSerialized] private UnityEngine.Object cachedBaseValue = null;

        [SerializeField] private string Path = NULL;

        [SerializeField] private string Type = null;

        public const string NULL = "null";

        public override bool Equals(object obj)
        {
            if(obj is Link == false)
            {
                return false;
            }
            return ((Link)obj).GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            if(cachedHash == 0)
            {
                Type = GetValueType();
                cachedHash = Path.GetHashCode();
            }
            return cachedHash;
        }

        protected virtual string GetValueType()
        {
            return Type;
        }

        protected string GetPath() { return Path; }

        public void OnBeforeSerialize()
        {
            Type = GetValueType();
        }

        public override string ToString()
        {
            return Path.Replace('/', '-').Replace('\\', '-');
        }

        public string GetName()
        {
            return System.IO.Path.GetFileName(Path);
        }

        public void OnAfterDeserialize()
        {
            
        }

        public virtual UnityEngine.Object BaseValue
        {
            get
            {
                if (cachedBaseValue != null)
                    return cachedBaseValue;
                cachedBaseValue = Resources.Load(Path, System.Type.GetType(GetValueType()));
                return cachedBaseValue;
            }
        }

        public static bool operator == (Link a, Link b)
        {
            return a?.GetHashCode() == b?.GetHashCode();
        }

        public static bool operator != (Link a, Link b)
        {
            return a?.GetHashCode() != b?.GetHashCode();
        }
    }

    [Serializable]
    public class LinkTo<T> : Link where T : UnityEngine.Object
    {
        [NonSerialized] private T cachedObject;

        public override bool HasValue => GetPath() != NULL || Resources.Load<T>(GetPath()) != null;

        protected override string GetValueType()
        {
            return typeof(T).AssemblyQualifiedName;
        }

        public T Value
        {
            get
            {
                if (cachedObject != null)
                    return cachedObject;
                cachedObject = Resources.Load<T>(GetPath());
                return cachedObject;
            }
        }

        public static implicit operator T(LinkTo<T> link) => link?.Value;
    }

    [Serializable]
    public class LinkToInterface<T> : Link
    {
        [NonSerialized] private T cachedObject;

        public override bool HasValue
        {
            get
            {
                if (GetPath() == NULL)
                {
                    return false;
                }
                GameObject obj = Resources.Load<GameObject>(GetPath());

                if (obj == null)
                {
                    return false;
                }

                return obj.GetComponent<T>() != null;
            }
        }

        protected override string GetValueType()
        {
            return typeof(T).AssemblyQualifiedName;
        }

        public T Value
        {
            get
            {
                if (cachedObject != null)
                    return cachedObject;
                GameObject obj = Resources.Load<GameObject>(GetPath());
                if(obj != null)
                {
                    cachedObject = obj.GetComponent<T>();
                }
                return cachedObject;
            }
        }

        public static implicit operator T(LinkToInterface<T> link) => link.Value;
    }

    public class LinkFolderAttribute : Attribute
    {
        public readonly string folder;

        public LinkFolderAttribute(string folder)
        {
            this.folder = folder;
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class GenerateLinkAttribute : Attribute
    {
        public readonly string customName;
        public readonly string folder;

        public GenerateLinkAttribute(string folder = null, string customName = null)
        {
            this.customName = customName;
            this.folder = folder;
        }
    }


}
