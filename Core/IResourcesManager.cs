﻿using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Core
{
    public interface IResourcesManager : IModule
    {
        T Load<T>(string path) where T : Object;

        Object Load(string path);

        void LoadAll<T>(string folder, List<T> values) where T : Object;

        void LoadAll<T>(string folder, List<Object> values) where T : Object;

        bool Exists<T>(string path) where T : Object;

        IPromise<T> LoadAsync<T>(string path) where T : Object;

        IPromise<IEnumerable<T>> LoadAllAsync<T>(string path) where T : Object;
    }
}
