﻿using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.ModuleFactory
{
    public class ResourcesModuleFactory : IModuleFactory
    {
        private readonly string moduleResourcesFolder;

        private readonly List<GameObject> modulePrefabs = new List<GameObject>();

        public ResourcesModuleFactory(string moduleResourcesFolder)
        {
            this.moduleResourcesFolder = moduleResourcesFolder;
            modulePrefabs.AddRange(Resources.LoadAll<GameObject>(moduleResourcesFolder));
        }

        public T CreateModule<T>() where T : IModule
        {
            GameObject modulePrefabObject = null;

            foreach (GameObject prefab in modulePrefabs)
            {
                if (prefab.GetComponent<T>() != null)
                {
                    modulePrefabObject = prefab;
                    break;
                }
            }

            if (modulePrefabObject == null)
            {
                Debug.LogWarning($"Cannot find Unity Module of type {typeof(T).Name}! Put it inside default folder at Resources/{moduleResourcesFolder}");
                return default;
            }

            GameObject moduleInstanceObject = Object.Instantiate(modulePrefabObject);
            Object.DontDestroyOnLoad(moduleInstanceObject.gameObject);
            moduleInstanceObject.name = modulePrefabObject.name;
            return moduleInstanceObject.GetComponent<T>();
        }

    }
}
