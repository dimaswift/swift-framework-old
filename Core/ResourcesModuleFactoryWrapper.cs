﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.ModuleFactory
{
    public class ResourcesModuleFactoryWrapper : MonoBehaviour, IModuleFactory
    {
        [SerializeField] private string moduleResourcesFolder = "Modules";

        public T CreateModule<T>() where T : IModule
        {
            if(factory == null)
            {
                factory = new ResourcesModuleFactory(moduleResourcesFolder);
            }
            return factory.CreateModule<T>();
        }

        private ResourcesModuleFactory factory;
    }
}
