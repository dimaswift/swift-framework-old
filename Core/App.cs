﻿using SwiftFramework.ModuleFactory;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace SwiftFramework.Core
{
    public enum ModuleState
    {
        Unloaded, Loading, Loaded,
    }

    public abstract class App : IApp
    {
        public static bool Initialized => initPromise.CurrentState == PromiseState.Resolved;

        protected static Promise<IApp> initPromise = Promise<IApp>.Create();

        public bool Loaded { get; protected set; }

        public static IApp Core { get; protected set; }

        public abstract IPromise<IApp> ReadyPromise { get; }

        public abstract ISerializator Serializator { get; }

        public abstract IResourcesManager ResourcesManager { get; }

        public abstract IConfigManager ConfigManager { get; }

        public abstract ICloudConfigManager CloudConfigManager { get; }

        public abstract ILocalizationManager Local { get; }

        public abstract IWindowsManager WindowsManager { get; }

        public abstract IViewFactory ViewFactory { get; }

        public abstract IEventManager Events { get; }

        public abstract IAdsManager AdsManager { get; }

        public abstract IInAppPurchaseManager IAP { get; }

        public abstract IServerTime ServerTime { get; }

        public abstract INetworkManager Net { get; }

        public abstract ICoroutineManager Coroutine { get; }

        public abstract IAnalyticsManager Analytics { get; }

        public abstract ISecurePrefs SecurePrefs { get; }

        public abstract ISoundManager Sound { get; }

        public abstract T GetModule<T>() where T : IModule;

        protected App()
        {
            Core = this;
        }
    }

    public class App<M> : App where M : App<M>, new()
    {
        private static M main = null;

        public static M Main
        {
            get
            {
                if(main == null)
                {
                    throw new InvalidOperationException($"{typeof(M).Name} is not created. Call {typeof(M).Name}.Create() first!");
                }
                if (main.Loaded == false)
                {
                    throw new InvalidOperationException($"{typeof(M).Name} is still loading! You can check if it's initialized using {typeof(M).Name}.Initialized property.");
                }
                return main;
            }
        }

        public static void Unload()
        {
            if(main == null)
            {
                return;
            }
            foreach (var module in main.modules)
            {
                module.Unload();
            }
            PromiseTimer.CancelAll();
            main = null;
            Core = null;
            initPromise = Promise<IApp>.Create();
        }

        public static IPromise<IApp> Create(IModuleFactory moduleFactory)
        {
            if(main == null)
            {
                main = new M();
                Core = main;
                return main.Init(moduleFactory);
            }
            return InitPromise;
        }

        public static IPromise<IApp> InitPromise
        {
            get
            {
                return initPromise;
            }
        }

        public ModuleState ModuleState { get; private set; }

        public override IPromise<IApp> ReadyPromise => readyPromise;
        public override ISerializator Serializator => GetCachedModule(ref serializator);
        public override IResourcesManager ResourcesManager => GetCachedModule(ref resourcesManager);
        public override IConfigManager ConfigManager => GetCachedModule(ref configManager);
        public override ICloudConfigManager CloudConfigManager => GetCachedModule(ref cloudConfigManager);
        public override ILocalizationManager Local => GetCachedModule(ref localizationManager);
        public override IWindowsManager WindowsManager => GetCachedModule(ref windowsManager);
        public override IViewFactory ViewFactory => GetCachedModule(ref viewFactory);
        public override IEventManager Events => GetCachedModule(ref eventsManager);
        public override IAdsManager AdsManager => GetCachedModule(ref adsManager);
        public override IInAppPurchaseManager IAP => GetCachedModule(ref inAppPurchaseManager);
        public override IServerTime ServerTime => GetCachedModule(ref serverTimeManager);
        public override INetworkManager Net => GetCachedModule(ref networkManager);
        public override ICoroutineManager Coroutine => GetCachedModule(ref coroutine);
        public override IAnalyticsManager Analytics => GetCachedModule(ref analyticsManager);
        public override ISecurePrefs SecurePrefs => GetCachedModule(ref securePrefs);
        public override ISoundManager Sound => GetCachedModule(ref sound);

        private IModuleFactory moduleFactory;
        private ISerializator serializator;
        private IResourcesManager resourcesManager;
        private IConfigManager configManager;
        private ICloudConfigManager cloudConfigManager;
        private ILocalizationManager localizationManager;
        private IWindowsManager windowsManager;
        private IViewFactory viewFactory;
        private IEventManager eventsManager;
        private IAdsManager adsManager;
        private IInAppPurchaseManager inAppPurchaseManager;
        private IServerTime serverTimeManager;
        private INetworkManager networkManager;
        private ICoroutineManager coroutine;
        private IAnalyticsManager analyticsManager;
        private ISecurePrefs securePrefs;
        private ISoundManager sound;

        private readonly List<IModule> modules = new List<IModule>();

        private bool initializing = false;

        private readonly Promise<IApp> readyPromise = Promise<IApp>.Create();

        public IPromise<IApp> Init(IModuleFactory moduleFactory)
        {
            this.moduleFactory = moduleFactory;
            if (initializing)
            {
                return initPromise;
            }

            initializing = true;
            LoadModules(initPromise).Done(() =>
            {
                Loaded = true;
                InitModules().Done(() => 
                {
                    OnInit();
                    initPromise.Resolve(this);
                    readyPromise.Resolve(this);
                });
            });
            return initPromise;
        }

        protected virtual void OnInit()
        {

        }

        private IPromise LoadModules(Promise<IApp> progressPromise)
        {
            if (ModuleState != ModuleState.Unloaded)
            {
                return Promise.Resolved();
            }

            ModuleState = ModuleState.Loading;

            List<PropertyInfo> moduleProps = new List<PropertyInfo>(typeof(App<M>).GetProperties());

            moduleProps.AddRange(typeof(M).GetProperties());

            foreach (PropertyInfo moduleProp in moduleProps)
            {
                if(typeof(IModule).IsAssignableFrom(moduleProp.PropertyType) == false)
                {
                    continue;
                }

                object moduleObject = moduleProp.GetValue(this);

                if(moduleObject is IModule && modules.Contains((IModule)moduleObject) == false)
                {
                    modules.Add((IModule)moduleObject);
                }
            }

            Promise loadPromise = Promise.Create();

            LoadNextModule(0, loadPromise, progressPromise);

            return loadPromise;
        } 

        private IPromise InitModules()
        {
            Promise initPromise = Promise.Create();
            InitNextModule(0, initPromise);
            return initPromise;
        }

        private void InitNextModule(int current, Promise promise)
        {
            if (current >= modules.Count)
            {
                promise.Resolve();
                return;
            }

            Action initNext = () =>
            {
                current++;
                promise.ReportProgress((float)current / modules.Count);
                InitNextModule(current, promise);
            };
            IModule moduleToInit = modules[current];
            try
            {
                moduleToInit.InitModule(this).Finally(initNext);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError($"Cannot init module { moduleToInit.GetType().Name}. Exception was thrown. Skipping...");
                UnityEngine.Debug.LogException(e);
                initNext();
            }
          
        }

        private void LoadNextModule(int current, Promise loadingPromise, Promise<IApp> progressPromise)
        {
            if(current >= modules.Count)
            {
                loadingPromise.Resolve();
                return; 
            }

            IModule currentModule = modules[current];

            Action loadNext = () =>
            {
                current++;
                progressPromise.ReportProgress((float)current / modules.Count);
                LoadNextModule(current, loadingPromise, progressPromise);
            };

            if (currentModule.LoadModuleWithAppStart == false)
            {
                loadNext();
                return;
            }

            Action<float> progress = p =>
            {
                if (p != 0)
                    progressPromise.ReportProgress((float)current / modules.Count * p);
            };
            try
            {
                UnityEngine.Debug.Log($"{currentModule.GetType().Name} loading started...");
                Promise.Race(PromiseTimer.WaitFor(10), currentModule.LoadModule().Progress(progress))
                .Done(() => 
                {
                    UnityEngine.Debug.Log($"{currentModule.GetType().Name} loaded");
                    loadNext();
                });
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError($"Cannot load module {currentModule.GetType().Name}. Exception was thrown. Skipping...");
                UnityEngine.Debug.LogException(e);
                loadNext();
            }
        }

        protected T GetCachedModule<T>(ref T cachedModule) where T : IModule
        {
            if (cachedModule == null)
            {
                cachedModule = moduleFactory.CreateModule<T>();
            } 
            return cachedModule;
        }

        public override T GetModule<T>()
        {
            foreach (IModule module in modules)
            {
                if(module is T)
                {
                    return (T) module;
                }
            }
            return default;
        }
    }
}
