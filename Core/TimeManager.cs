﻿using System;

namespace SwiftFramework.Core
{
    public interface IServerTime : IModule
    {
        IPromise<long> GetUnixNow();
        IStatefulEvent<long> Now { get; }
        IPromise<DateTime> GetNow();
    }
}
