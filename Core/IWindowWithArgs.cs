﻿namespace SwiftFramework.Core
{
    public interface IWindowWithArgs<A> : IWindow
    {
        void Show(A args);
        void SetArgs(A args);
    }
}
