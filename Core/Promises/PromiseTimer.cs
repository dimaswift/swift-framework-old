using System;
using System.Collections.Generic;

namespace SwiftFramework.Core
{

    public class PromiseCancelledException : Exception
    {
        /// <summary>
        /// Just create the exception
        /// </summary>
        public PromiseCancelledException()
        {

        }

        /// <summary>
        /// Create the exception with description
        /// </summary>
        /// <param name="message">Exception description</param>
        public PromiseCancelledException(String message) : base(message)
        {

        }
    }

    /// <summary>
    /// A class that wraps a pending promise with it's predicate and time data
    /// </summary>
    internal class PredicateWait
    {
        private const int Capacity = 64;

        private static readonly Stack<PredicateWait> pool = new Stack<PredicateWait>(Capacity);

        static PredicateWait()
        {
            for (int i = 0; i < Capacity; i++)
            {
                pool.Push(new PredicateWait());
            }
        }

        private PredicateWait()
        {

        }

        public static PredicateWait Create()
        {
            lock (pool)
            {
                if(pool.Count > 0)
                {
                    return pool.Pop();
                }
                return new PredicateWait();
            }
        }

        ~PredicateWait()
        {
            predicate = null;
            timeStartedUnscaled = 0;
            pendingPromise = null;
            timeData = new TimeData();
            frameStarted = 0;
            lock (pool)
            {
                pool.Push(this);
            }
        }

        /// <summary>
        /// Predicate for resolving the promise
        /// </summary>
        public Func<TimeData, bool> predicate;

        /// <summary>
        /// The time the promise was started
        /// </summary>
        public float timeStartedUnscaled;

        public float timeStarted;

        /// <summary>
        /// The pending promise which is an interface for a promise that can be rejected or resolved.
        /// </summary>
        public IPendingPromise pendingPromise;

        /// <summary>
        /// The time data specific to this pending promise. Includes elapsed time and delta time.
        /// </summary>
        public TimeData timeData;

        /// <summary>
        /// The frame the promise was started
        /// </summary>
        public int frameStarted;
    }

    /// <summary>
    /// Time data specific to a particular pending promise.
    /// </summary>
    public struct TimeData
    {
        /// <summary>
        /// The amount of time that has elapsed since the pending promise started running
        /// </summary>
        public float elapsedTimeUnscaled;


        public float elapsedTime;
        /// <summary>
        /// The amount of time since the last time the pending promise was updated.
        /// </summary>
        public float unscaledDeltaTime;

        public float deltaTime;

        /// <summary>
        /// The amount of times that update has been called since the pending promise started running
        /// </summary>
        public int elapsedUpdates;
    }

    public class PromiseTimer : UnityEngine.MonoBehaviour
    {
        private static PromiseTimer instance;

        private static PromiseTimer Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new UnityEngine.GameObject("PromiseTimer").AddComponent<PromiseTimer>();
                    DontDestroyOnLoad(instance.gameObject);
                }
                return instance;
            }
        }

        /// <summary>
        /// The current running total for time that this PromiseTimer has run for
        /// </summary>
        private float curentTimeUnscaled;
        private float curentTime;


        /// <summary>
        /// The current running total for the amount of frames the PromiseTimer has run for
        /// </summary>
        private int curFrame;

        /// <summary>
        /// Currently pending promises
        /// </summary>
        private readonly LinkedList<PredicateWait> waiting = new LinkedList<PredicateWait>();

        /// <summary>
        /// Resolve the returned promise once the time has elapsed
        /// </summary>
        public static IPromise WaitForUnscaled(float seconds)
        {
            return WaitUntil(t => t.elapsedTimeUnscaled >= seconds);
        }

        public static IPromise WaitFor(float seconds)
        {
            return WaitUntil(t => t.elapsedTime >= seconds);
        }

        public static IPromise WaitForNextFrame()
        {
            return WaitUntil(t => t.elapsedTimeUnscaled >= 0);
        }


        public static void ResolveAfterOneFrame(Promise promise)
        {
            WaitUntil(t => t.elapsedUpdates >= 1).Done(promise.Resolve);
        }

        public static IPromise WaitForTrue(Func<bool> predicate)
        {
            return WaitUntil((t) => predicate());
        }

        public static IPromise WaitWhile(Func<TimeData, bool> predicate)
        {
            return WaitUntil(t => !predicate(t));
        }



        public static IPromise WaitUntil(Func<TimeData, bool> predicate)
        {
            PromiseTimer timer = Instance;
            var promise = Promise.Create();

            var wait = PredicateWait.Create();

            wait.timeStartedUnscaled = timer.curentTimeUnscaled;
            wait.timeStarted = timer.curentTime;
            wait.pendingPromise = promise;
            wait.timeData = new TimeData();
            wait.predicate = predicate;
            wait.frameStarted = timer.curFrame;

            timer.waiting.AddLast(wait);

            return promise;
        }

        public static void CancelAll()
        {
            Instance.waiting.Clear();
        }

        public static bool Cancel(IPromise promise)
        {
            if (promise == null)
            {
                return false;
            }

            Promise p = promise as Promise;

            if(p.CurrentState != PromiseState.Pending)
            {
                return false;
            }

            PromiseTimer timer = Instance;
            var node = timer.FindInWaiting(promise);

            if (node == null)
            {
                return false;
            }

            timer.waiting.Remove(node);

            return true;
        }

        private LinkedListNode<PredicateWait> FindInWaiting(IPromise promise)
        {
            for (var node = waiting.First; node != null; node = node.Next)
            {
                if (node.Value.pendingPromise.Id.Equals(promise.Id))
                {
                    return node;
                }
            }

            return null;
        }

        private void Update()
        {
            curentTimeUnscaled += UnityEngine.Time.unscaledDeltaTime;
            curentTime += UnityEngine.Time.deltaTime;
            curFrame += 1;
            var node = waiting.First;
            while (node != null)
            {
                var wait = node.Value;

                if(wait == null)
                {
                    waiting.RemoveFirst();
                    return;
                }

                var newUnscaledElapsedTime = curentTimeUnscaled - wait.timeStartedUnscaled;
                var newElapsedTime = curentTime - wait.timeStarted;

                wait.timeData.unscaledDeltaTime = newUnscaledElapsedTime - wait.timeData.elapsedTimeUnscaled;

                wait.timeData.deltaTime = newElapsedTime - wait.timeData.elapsedTime;

                wait.timeData.elapsedTimeUnscaled = newUnscaledElapsedTime;

                wait.timeData.elapsedTime = newElapsedTime;

                var newElapsedUpdates = curFrame - wait.frameStarted;

                wait.timeData.elapsedUpdates = newElapsedUpdates;

                bool result = wait.predicate(wait.timeData);
                
                if (result)
                {
                    wait.pendingPromise.Resolve();

                    node = RemoveNode(node);
                }
                else
                {
                    node = node.Next;
                }
            }
        }

        /// <summary>
        /// Removes the provided node and returns the next node in the list.
        /// </summary>
        private LinkedListNode<PredicateWait> RemoveNode(LinkedListNode<PredicateWait> node)
        {
            var currentNode = node;
            node = node.Next;

            waiting.Remove(currentNode);

            return node;
        }
    }
}

