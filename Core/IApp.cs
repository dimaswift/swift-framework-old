﻿namespace SwiftFramework.Core
{
    public interface IApp
    {
        IPromise<IApp> ReadyPromise { get; }
        ISerializator Serializator { get; }
        IResourcesManager ResourcesManager { get; }
        IConfigManager ConfigManager { get; }
        ICloudConfigManager CloudConfigManager { get; }
        ILocalizationManager Local { get; }
        IWindowsManager WindowsManager { get; }
        IViewFactory ViewFactory { get; }
        IEventManager Events { get; }
        IAdsManager AdsManager { get; }
        IInAppPurchaseManager IAP { get; }
        IServerTime ServerTime { get; }
        INetworkManager Net { get; }
        ICoroutineManager Coroutine { get; }
        IAnalyticsManager Analytics { get; }
        ISecurePrefs SecurePrefs { get; }
        ISoundManager Sound { get; }
        T GetModule<T>() where T : IModule;
    }
}

