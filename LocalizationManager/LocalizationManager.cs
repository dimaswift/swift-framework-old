﻿using UnityEngine;
using SwiftFramework.Core;
using System;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.ComponentModel;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace SwiftFramework.Localization
{
    public class LocalizationManager : Module, ILocalizationManager
    {
        [SerializeField] private string publishedGoogleSheetUrl = null;
        [SerializeField] private SystemLanguage fallbackLanguage = SystemLanguage.English;
        [SerializeField] private Extention extention = Extention.TSV;

        public SystemLanguage CurrentLanguage { get; private set; } = SystemLanguage.English;

        private readonly Promise downloadPromise = Promise.Create();

        private Dictionary<SystemLanguage, Dictionary<string, string>> dict = new Dictionary<SystemLanguage, Dictionary<string, string>>();

        private const string fileName = "localization";

        public enum Extention
        {
            TSV, CSV
        }

        private string localSheetPath => $"{Application.persistentDataPath}/{fileName}.csv";

        [ContextMenu("Download To Resources")]
        public void DownloadToResources()
        {
#if UNITY_EDITOR
            string resourcesFolder = Application.dataPath + "/Resources/";
            if (Directory.Exists(resourcesFolder) == false)
            {
                Directory.CreateDirectory(resourcesFolder);
            }
            DownloadSheet(publishedGoogleSheetUrl, $"{resourcesFolder}/{fileName}.csv").Then(() =>
            {
                Debug.Log("<color=green>Localization downloaded!</color>");
            })
            .Catch(e => Debug.LogError($"Localization download failed: {e.Message}"));
#endif

        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("SwiftFramework/Localization/Download To Resources")]
        private static void EditorDownload()
        {
            bool downloaded = false;
            foreach (var guid in UnityEditor.AssetDatabase.FindAssets("t:GameObject"))
            {
                var prefab = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(guid));
                if(prefab.GetComponent<LocalizationManager>() != null)
                {
                    prefab.GetComponent<LocalizationManager>().DownloadToResources();
                    downloaded = true;
                }
            }
            if(downloaded == false)
            {
                Debug.LogError("LocalizationManager not found among prefabs! Create one firts");
            }
        }
#endif

        public void SetLanguage(SystemLanguage language)
        {
            CurrentLanguage = language;
            OnLanguageChanged();
        }

        public event Action OnLanguageChanged = () => { };

        public string GetText(string key)
        {
            string result = GetText(key, CurrentLanguage, out bool success);

            if(success == false)
            {
                Debug.LogWarning($"Using fallback language for key <b>{key}</b>!");
                result = GetText(key, fallbackLanguage, out success);
            }

            return result;
        }

        private string GetText(string key, SystemLanguage lang, out bool success)
        {
            if (dict.TryGetValue(lang, out Dictionary<string, string> keys) == false)
            {
                Debug.LogWarning($"{CurrentLanguage} is not localized!");
                success = false;
                return key;
            }

            if (keys.TryGetValue(key, out string result) == false)
            {
                Debug.LogWarning($"Localization key <b>{key}</b> for <b>{lang}</b> is not localized!");
                success = false;
                return key;
            }

            if (string.IsNullOrEmpty(result))
            {
                success = false;
            }

            success = true;

            return result;
        }

        public string GetText(string key, params object[] args)
        {
            try
            {
                return string.Format(GetText(key), args);
            }
            catch (Exception)
            {
                Debug.LogWarning($"Invalid format for localization key {key}!");
                return GetText(key);
            }
        }

        private IPromise DownloadSheet(string url, string filePath)
        {
            INetworkManager networkManager = GetComponent<INetworkManager>();
            if(networkManager == null)
            {
                return Promise.Rejected(new InvalidOperationException($"INetworkManager not found. Add component that implements INetworkManager to {name}"));
            }
            Promise promise = Promise.Create();
            networkManager.Get(url, 2).Then(body =>
            {
                File.WriteAllText(filePath, body);
                promise.Resolve();
            })
            .Catch(e => promise.Reject(e));
            return promise;
        }

        protected override IPromise GetLoadPromise()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                downloadPromise.Resolve();
                ParseLocal();
                return downloadPromise;
                
            }

            DownloadSheet(publishedGoogleSheetUrl, localSheetPath).Then(() => 
            {
                ParseGoogleSheet(File.ReadAllLines(localSheetPath));
                downloadPromise.Resolve();
            })
            .Catch(e => 
            {
                Debug.LogError($"Couldn't download localization: {e?.Message}");
                ParseLocal();
                downloadPromise.Resolve();
            });

            return downloadPromise;
        }

        private void ParseLocal()
        {
            if (File.Exists(localSheetPath))
            {
                ParseGoogleSheet(File.ReadAllLines(localSheetPath));
            }
            else
            {
                string[] rows = Resources.Load<TextAsset>(fileName)?.text.Split('\n');
                if(rows == null)
                {
                    Debug.LogError($"Cannot parse file from resources! Should be inside Resources/{fileName}.csv");
                    return;
                }
                ParseGoogleSheet(rows);
            }
        }


        private void ParseGoogleSheet(string[] rows)
        {
            char separator;

            switch (extention)
            {
                case Extention.TSV:
                    separator = '\t';
                    break;
                case Extention.CSV:
                    separator = ',';
                    break;
                default:
                    separator = ',';
                    break;
            }

            if(rows.Length == 0)
            {
                Debug.LogError("Invalid google sheet!");
                return;
            }

            var langs = rows[0].Split(separator);

            for (int i = 0; i < langs.Length; i++)
            {
                langs[i] = langs[i].Replace((char)160, (char)32);
            }

            for (int i = 1; i < rows.Length; i++)
            {
                var columns = rows[i].Split(separator);
                var key = columns[0];
                for (int j = 1; j < columns.Length; j++)
                {
                    var lang = langs[j];

                    if (Enum.TryParse(lang, out SystemLanguage parsedLang) == false)
                    {
                        Debug.LogError($"Cannot parse language {lang}");
                    }
                    else
                    {
                        if (dict.ContainsKey(parsedLang) == false)
                        {
                            dict.Add(parsedLang, new Dictionary<string, string>());
                        }

                        var keys = dict[parsedLang];

                        if(keys.ContainsKey(key) == false && string.IsNullOrEmpty(columns[j]) == false)
                        {
                            keys.Add(key, columns[j]);
                        }
                    }
                }
            }
        }
    }
}
