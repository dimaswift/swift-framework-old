﻿using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.ResourcesManager
{
    public class ResourcesManager : Module, IResourcesManager
    {
        public bool Exists<T>(string path) where T : Object
        {
            return Resources.Load<T>(path) != null;
        }

        public T Load<T>(string path) where T : Object
        {
            return Resources.Load<T>(path);
        }

        public Object Load(string path)
        {
            return Resources.Load(path);
        }

        public void LoadAll<T>(string path, List<T> values) where T : Object
        {
            values.Clear();
            values.AddRange(Resources.LoadAll<T>(path));
        }

        public void LoadAll<T>(string folder, List<Object> values) where T : Object
        {
            values.Clear();
            values.AddRange(Resources.LoadAll<T>(folder));
        }

        public IPromise<IEnumerable<T>> LoadAllAsync<T>(string path) where T : Object
        {
            Promise<IEnumerable<T>> result = Promise<IEnumerable<T>>.Create();

            result.Resolve(Resources.LoadAll<T>(path));

            return result;
        }

        public IPromise<T> LoadAsync<T>(string path) where T : Object
        {
            Promise<T> result = Promise<T>.Create();

            result.Resolve(Resources.Load<T>(path));

            return result;
        }
    }
}
