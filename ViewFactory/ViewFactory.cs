﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Views
{
    public class ViewFactory : Module, IViewFactory
    {
        [SerializeField] private string viewsResourcesFolder = "Views";
        [SerializeField] private bool skipFrameAfterInstantiatingView = false;

        private const int CAPACITY = 128;

        private readonly List<GameObject> views = new List<GameObject>(CAPACITY);

        private readonly Dictionary<IView, Pool> pools = new Dictionary<IView, Pool>(CAPACITY);

        private readonly Dictionary<Type, Pool> typedPools = new Dictionary<Type, Pool>(CAPACITY);

        public T CreateView<T>(T source) where T : class, IView
        {
            if(source == null)
            {
                Debug.LogError($"Cannot create view of type {typeof(T)} with null source!");
                return default;
            }
            if (pools.TryGetValue(source, out Pool pool) == false)
            {
                pool = CreatePool(source);
            }
            T view = pool.Take<T>();
            return view;
        }

        public T CreateView<T>() where T : class, IView
        {
            Type type = typeof(T);

            if(typedPools.TryGetValue(type, out Pool pool))
            {
                T view = pool.Take<T>();
                return view;
            }

            foreach (var view in views)
            {
                T viewPrefab = view.GetComponent<T>();

                if (viewPrefab != null)
                {
                    pool = new Pool(() => Create<T>(view));
                    typedPools.Add(type, pool);
                    T v = pool.Take<T>();
                    return v;
                }
            }

            Debug.LogError($"Cannot find View of type {typeof(T)}. Not found.");

            return default;
        }

        private Pool CreatePool<T>(T source) where T : class, IView
        {
            Pool pool = new Pool(() => Create<T>(source.GetRoot()));
            pools.Add(source, pool);
            return pool;
        }

        private void Update()
        {
            float delta = Time.deltaTime;
            foreach (var pool in pools)
            {
                foreach (var view in pool.Value.GetActiveItems<IView>())
                {
                    view.Process(delta);
                }
            }
        }

        private T Create<T>(GameObject source) where T : IView
        {
            GameObject instanceObject = Instantiate(source, transform);
            instanceObject.name = source.name;
            T instance = instanceObject.GetComponent<T>();
            return instance;
        }

        private IEnumerator WarmUpRoutine(Promise promise)
        {
            views.AddRange(Resources.LoadAll<GameObject>(viewsResourcesFolder));
            if(views.Count == 0)
            {
                promise.ReportProgress(1);
                promise.Resolve();
                yield break;
            }
            int viewIndex = 0;
            foreach (var view in views)
            {
                IView viewPrefab = view.GetComponent<IView>();
                if (viewPrefab == null)
                {
                    Debug.LogWarning($"View object {view.name} does not have component that implements IView interface. Ignoring...");
                    continue;
                }

                Type type = null;
                foreach (var inter in viewPrefab.GetType().GetInterfaces())
                {
                    if (typeof(IView).IsAssignableFrom(inter))
                    {
                        type = inter;
                        break;
                    }
                }

                if (type == null)
                {
                    Debug.LogWarning($"View object {view.name} should implement interface that inherits IView<T> or IView interface! Ignoring...");
                    continue;
                }

                IView viewInterface = view.GetComponent<IView>();

                if (viewInterface != null && pools.ContainsKey(viewInterface) == false)
                {
                    Pool pool = new Pool(() => Create<IView>(view.gameObject));

                    if (skipFrameAfterInstantiatingView)
                    {
                        for (int i = 0; i < viewPrefab.InitialPoolCapacity; i++)
                        {
                            pool.WarmUp(1);
                            float progress = ((float)viewIndex) / views.Count;
                            progress += (1f / views.Count) * ((float)i / viewPrefab.InitialPoolCapacity);
                            promise.ReportProgress(progress);
                            yield return new WaitForEndOfFrame();
                        }
                    }
                    else
                    {
                        promise.ReportProgress(((float)viewIndex + 1) / views.Count);
                        pool.WarmUp(viewPrefab.InitialPoolCapacity);
                    }
 
                    pools.Add(viewInterface, pool);

                    if(typedPools.ContainsKey(view.GetType()) == false)
                    {
                        typedPools.Add(view.GetType(), pool);
                    }
                }

                viewIndex++;
            }
            promise.Resolve();
        }

        protected override IPromise GetLoadPromise()
        {
            Promise promise = Promise.Create();
            StartCoroutine(WarmUpRoutine(promise));
            return promise;
        }

        public T GetOrCreateView<T>() where T : class, IView
        {
            if (typedPools.TryGetValue(typeof(T), out Pool pool))
            {
                foreach (var item in pool.GetActiveItems<T>())
                {
                    return item;
                }
            }
            return CreateView<T>();
        }
    }

}
