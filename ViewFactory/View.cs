﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using UnityEngine;

namespace SwiftFramework.Views
{
    public class View : MonoBehaviour, IPooled, IView
    {
        [SerializeField] private int poolCapacity = 1;

        public int InitialPoolCapacity => poolCapacity;

        public bool Active { get; set; }

        private IPool pool;

        public void Init(IPool pool)
        {
            this.pool = pool;
        }

        public virtual void TakeFromPool()
        {
            gameObject.SetActive(true);
            Active = true;
        }

        public virtual void ReturnToPool()
        {
            gameObject.SetActive(false);
            pool.Return(this);
            Active = false;
        }

        public virtual void Process(float deltaTime) { }

        public GameObject GetRoot()
        {
            return gameObject;
        }
    }
}
