﻿using SwiftFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SwiftFramework.Sound
{
    public class SoundManager : Module, ISoundManager
    {
        [SerializeField] private int maxActivePlayerCount = 10;
        [SerializeField] private int startPlayersAmount = 3;

        private bool muted;

        private AudioSource oneShotPlayer;

        protected override IPromise GetLoadPromise()
        {
            for (int i = 0; i < startPlayersAmount; i++)
            {
                players.Add(gameObject.AddComponent<AudioSource>());
            }
            oneShotPlayer = gameObject.AddComponent<AudioSource>();
            oneShotPlayer.loop = false;
            return Promise.Resolved();
        }

        private readonly List<AudioSource> players = new List<AudioSource>();

        private readonly Dictionary<AudioClip, AudioSource> activeLoops = new Dictionary<AudioClip, AudioSource>();

        public void PlayLoop(AudioClip clip, float volumeScale = 1)
        {
            if(IsValid(clip) == false)
            {
                return;
            }
            if (activeLoops.ContainsKey(clip))
            {
                Debug.LogWarning($"Clip {clip} already playing!");
                return;
            }
            AudioSource freePlayer = GetFreePlayer();
            if(freePlayer == null)
            {
                Debug.LogWarning($"Can't get free audio player! Players count: {players.Count}!");
                return;
            }
            freePlayer.clip = clip;
            freePlayer.volume = volumeScale;
            freePlayer.loop = true;
            freePlayer.Play();
            activeLoops.Add(clip, freePlayer);
        }

        private bool IsValid(AudioClip clip)
        {
            if (clip == null)
            {
                Debug.LogError($"Trying to play null audio clip!");
                return false;
            }
            return true;
        }

        public void PlayOnce(AudioClip clip, float volumeScale = 1)
        {
            if (IsValid(clip) == false)
            {
                return;
            }
            oneShotPlayer.PlayOneShot(clip, volumeScale);
        }

        public void StopLoop(AudioClip clip)
        {
            if (IsValid(clip) == false)
            {
                return;
            }
            if (activeLoops.ContainsKey(clip) == false)
            {
                Debug.LogWarning($"Can stop {clip}. Is not playing!");
                return;
            }

            activeLoops[clip].Stop();

            activeLoops.Remove(clip);
        }

        private AudioSource GetFreePlayer()
        {
            foreach (var player in players)
            {
                if(player.isPlaying == false)
                {
                    return player;
                }
            }
            if(players.Count >= maxActivePlayerCount)
            {
                Debug.LogWarning($"Can't get free audio player! Players count: {players.Count}!");
                return null;
            }
            AudioSource newPlayer = gameObject.AddComponent<AudioSource>();
            newPlayer.mute = muted;
            players.Add(newPlayer);
            return newPlayer;
        }

        public void SetMuted(bool muted)
        {
            this.muted = muted;
            oneShotPlayer.mute = muted;
            foreach (var player in players)
            {
                player.mute = muted;
            }
        }
    }

}
