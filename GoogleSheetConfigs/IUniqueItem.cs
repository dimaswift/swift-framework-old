﻿namespace SwiftFramework.GoogleSheetConfigs
{
    public interface IUniqueItem
    {
        string Id { get; }
    }
}