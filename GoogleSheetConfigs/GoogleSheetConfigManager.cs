﻿using SwiftFramework.Core;
using UnityEngine;
using System.Collections.Generic;

namespace SwiftFramework.GoogleSheetConfigs
{
    public class GoogleSheetConfigManager : Module, ICloudConfigManager
    {
        private readonly List<Object> configsBuffer = new List<Object>();

        public IPromise<T> LoadConfig<T>() where T : new() 
        {
            Promise<T> result = Promise<T>.Create();
            configsBuffer.Clear();
            App.ResourcesManager.LoadAll<Config<T>>("", configsBuffer);

            if (configsBuffer.Count == 0)
            {
                result.Reject(GenerateException<T>());
                return result;
            }

            Config<T> config = configsBuffer[0] as Config<T>;

            config.StartDownloading(success =>
            {
                if (success)
                {
                    result.Resolve(config.Data);
                }
                else result.Reject(new System.OperationCanceledException("Cannot download config!"));
            });

            return result;
        }

        private System.Exception GenerateException<T>()
        {
            return new System.NullReferenceException($"Cannot load google sheet config of type {typeof(T).Name}!");
        }
    }
}
