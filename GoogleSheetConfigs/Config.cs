﻿using UnityEngine;

namespace SwiftFramework.GoogleSheetConfigs
{
    public class Config<T> : SheetParserContainer where T : new() 
    {
        [SerializeField] private T data;

        public T Data => data;
        
        protected override void ParseSheet(string[] rows)
        {
            data = SheetParser.ParseToObject(data, rows, GetSeparator(), GetCustomParsers());
        }
    }
}

