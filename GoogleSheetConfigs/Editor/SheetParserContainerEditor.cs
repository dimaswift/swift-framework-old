﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SwiftFramework.GoogleSheetConfigs.Editor
{
	[CustomEditor(typeof(SheetParserContainer), true)]
	public class SheetParserContainerEditor : UnityEditor.Editor
	{
		private DateTime downloadTime;
        private DateTime downloadStartTime;
        private bool downloadedSuccessfully;
		
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			var d = target as SheetParserContainer;
			GUI.enabled = !d.IsDownloading;
			if (GUILayout.Button(d.IsDownloading ? "Downloading..." : "Download"))
			{
				d.StartDownloading(OnDownloaded);
				d.IsDownloading = true;
				downloadStartTime = DateTime.Now;
			}

			if (!d.IsDownloading)
			{
				GUI.enabled = true;
				if ((DateTime.Now - downloadTime).TotalSeconds < 3)
				{
					if (downloadedSuccessfully)
						EditorGUILayout.HelpBox("Successfully downloaded!", MessageType.Info);
					else EditorGUILayout.HelpBox("Error while downloading!", MessageType.Error);
				}
			}
			else
			{
				if ((DateTime.Now - downloadStartTime).TotalSeconds > 10)
				{
					OnDownloaded(false);
				}
			}
			
		}

		private void OnDownloaded(bool succ)
		{
			EditorDispatcher.Dispatch(() => 
			{
				var d = target as SheetParserContainer;
				d.IsDownloading = false;
				downloadTime = DateTime.Now; 
				downloadedSuccessfully = succ;
			});
		}
	}

}
