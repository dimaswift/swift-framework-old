﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace SwiftFramework.GoogleSheetConfigs.Editor
{
    public class EditorCoroutineRunner
    {    
        public static void KillAllCoroutines()
        {
            EditorUtility.ClearProgressBar();
            uiCoroutineState = null;
            coroutineStates.Clear();
            finishedThisUpdate.Clear();
        }

        private static readonly List<EditorCoroutineState> coroutineStates = new List<EditorCoroutineState>();
        private static readonly List<EditorCoroutineState> finishedThisUpdate = new List<EditorCoroutineState>();
        private static EditorCoroutineState uiCoroutineState;
    
       
        public static EditorCoroutine StartCoroutine(IEnumerator coroutine)
        {
            return StoreCoroutine(new EditorCoroutineState(coroutine));
        }
    
        public static EditorCoroutine StartCoroutineWithUI(IEnumerator coroutine, string title, bool isCancelable = false)
        {
            if (uiCoroutineState != null)
            {
                Debug.LogError("EditorCoroutineRunner only supports running one coroutine that draws a GUI! [" + title + "]");
                return null;
            }
            uiCoroutineState = new EditorCoroutineState(coroutine, title, isCancelable);
            return StoreCoroutine(uiCoroutineState);
        }

        
        private static EditorCoroutine StoreCoroutine(EditorCoroutineState state)
        {
            if (coroutineStates.Count == 0)
                EditorApplication.update += Runner;
    
            coroutineStates.Add(state);
    
            return state.editorCoroutineYieldInstruction;
        }
    
        public static void UpdateUILabel(string label)
        {
            if (uiCoroutineState != null && uiCoroutineState.showUi)
            {
                uiCoroutineState.label = label; 
            }
        }
    

        public static void UpdateUIProgressBar(float percent)
        {
            if (uiCoroutineState != null && uiCoroutineState.showUi)
            {
                uiCoroutineState.percentComplete = percent;
            }
        }
    
        public static void UpdateUI(string label, float percent)
        {
            if (uiCoroutineState != null && uiCoroutineState.showUi)
            {
                uiCoroutineState.label = label ;
                uiCoroutineState.percentComplete = percent;
            }
        }

        private static void Runner()
        {
            for (int i = 0; i < coroutineStates.Count; i++)
            {
                TickState(coroutineStates[i]);
            }
    
            for (int i = 0; i < finishedThisUpdate.Count; i++)
            {
                coroutineStates.Remove(finishedThisUpdate[i]);
    
                if (uiCoroutineState == finishedThisUpdate[i])
                {
                    uiCoroutineState = null;
                    EditorUtility.ClearProgressBar();
                }
            }
            finishedThisUpdate.Clear();
    
            if (coroutineStates.Count == 0)
            {
                EditorApplication.update -= Runner;
            }
        }

        private static void TickState(EditorCoroutineState state)
        {
            if (state.IsValid)
            {
                state.Tick();
    
                if (state.showUi && uiCoroutineState == state)
                {
                    uiCoroutineState.UpdateUI();
                }
            }
            else
            {
                finishedThisUpdate.Add(state);
            }
        }

        private static bool KillCoroutine(ref EditorCoroutine coroutine, List<EditorCoroutineState> states)
        {
            foreach (EditorCoroutineState state in states)
            {
                if (state.editorCoroutineYieldInstruction == coroutine) 
                {
                    states.Remove (state);
                    coroutine = null;
                    return true;
                }
            }
            return false;
        }
    
        public static void KillCoroutine(ref EditorCoroutine coroutine)
        {
            if (uiCoroutineState.editorCoroutineYieldInstruction == coroutine)
            {
                uiCoroutineState = null;
                coroutine = null;
                EditorUtility.ClearProgressBar ();
                return;
            }
            if (KillCoroutine (ref coroutine, coroutineStates))
                return;
    
            if (KillCoroutine (ref coroutine, finishedThisUpdate))
                return;
        }   
    }
    
    class EditorCoroutineState
    {
        public bool IsValid
        {
            get { return coroutine != null; }
        }
        public readonly EditorCoroutine editorCoroutineYieldInstruction;
        public readonly bool showUi;
        public string label;
        public float percentComplete;

        private object current;
        private Type currentType;
        private float timer; 
        private IEnumerator coroutine;
        private EditorCoroutine nestedCoroutine; 
        private DateTime lastUpdateTime;
        private bool canceled;
        private readonly string title;
        private readonly bool cancelable;
        
        public EditorCoroutineState(IEnumerator coroutine)
        {
            this.coroutine = coroutine;
            editorCoroutineYieldInstruction = new EditorCoroutine();
            showUi = false;
            lastUpdateTime = DateTime.Now;
        }
    
        public EditorCoroutineState(IEnumerator coroutine, string title, bool isCancelable)
        {
            this.coroutine = coroutine;
            editorCoroutineYieldInstruction = new EditorCoroutine();
            showUi = true;
            cancelable = isCancelable;
            this.title = title;
            label = "initializing....";
            percentComplete = 0.0f;
    
            lastUpdateTime = DateTime.Now;
        }
    
        public void Tick()
        {
            if (coroutine != null)
            {
              
                if (canceled)
                {
                    Stop();
                    return;
                }

                bool isWaiting = false;
                var now = DateTime.Now;
                if (current != null) 
                {
                    if (currentType == typeof(WaitForSeconds))
                    {
                        var delta = now - lastUpdateTime;
                        timer -= (float)delta.TotalSeconds;
    
                        if (timer > 0.0f)
                        {
                            isWaiting = true;
                        }
                    }
                    else if (currentType == typeof(WaitForEndOfFrame) || currentType == typeof(WaitForFixedUpdate))
                    {
                        isWaiting = false;
                    }
                    else if (currentType == typeof(UnityEngine.Networking.UnityWebRequest))
                    {
                        var www = current as UnityEngine.Networking.UnityWebRequest;
                        if (!www.isDone)
                        {
                            isWaiting = true;
                        }
                    }
                    else if (currentType.IsSubclassOf(typeof(CustomYieldInstruction)))
                    {
                        var yieldInstruction = current as CustomYieldInstruction;
                        if (yieldInstruction.keepWaiting)
                        {
                            isWaiting = true;
                        }
                    }
                    else if (currentType == typeof(EditorCoroutine))
                    {
                        var editorCoroutine = current as EditorCoroutine;
                        if (!editorCoroutine.HasFinished)
                        {
                            isWaiting = true;
                        }
                    }
                    else if (typeof(IEnumerator).IsAssignableFrom(currentType))
                    {
                        if (nestedCoroutine == null)
                        {
                            nestedCoroutine = EditorCoroutineRunner.StartCoroutine(current as IEnumerator);
                            isWaiting = true;
                        }
                        else
                        {
                            isWaiting = !nestedCoroutine.HasFinished;
                        }
    
                    }
                    else if (currentType == typeof(Coroutine))
                    {
                        Debug.LogError("Nested Coroutines started by Unity's defaut StartCoroutine method are not supported in editor! please use EditorCoroutineRunner.Start instead. Canceling.");
                        canceled = true;
                    } 
                    else
                    {
                        Debug.LogError("Unsupported yield (" + currentType + ") in editor coroutine!! Canceling.");
                        canceled = true;
                    }
                }
                lastUpdateTime = now;

                if (canceled)
                {
                    Stop();
                    return;
                }
    
                if (!isWaiting)
                {
                    bool update = coroutine.MoveNext();
    
                    if (update)
                    {
                        current = coroutine.Current;
                        if (current != null)
                        {
                            currentType = current.GetType();
    
                            if (currentType == typeof(WaitForSeconds))
                            {
                                var wait = current as WaitForSeconds;
                                FieldInfo m_Seconds = typeof(WaitForSeconds).GetField("m_Seconds", BindingFlags.NonPublic | BindingFlags.Instance);
                                if (m_Seconds != null)
                                {
                                    timer = (float)m_Seconds.GetValue(wait);
                                }
                            }
                            else if (currentType == typeof(EditorStatusUpdate))
                            {
                                var updateInfo = current as EditorStatusUpdate;
                                if (updateInfo.HasLabelUpdate)
                                {
                                    label = updateInfo.Label;
                                }
                                if (updateInfo.HasPercentUpdate)
                                {
                                    percentComplete = updateInfo.PercentComplete;
                                }
                            }
                        }
                    }
                    else
                    {
                        Stop();
                    }
                }
            }
        }
    
        private void Stop()
        {
            coroutine = null;
            editorCoroutineYieldInstruction.HasFinished = true;
        }
    
        public void UpdateUI()
        {
            if (cancelable)
            {
                canceled = EditorUtility.DisplayCancelableProgressBar(title, label, percentComplete);
                if (canceled)
                    Debug.Log("CANCLED");
            }
            else
            {
                EditorUtility.DisplayProgressBar(title, label, percentComplete);
            }
        }
    }

    public class EditorStatusUpdate : CustomYieldInstruction
    {
        public readonly string Label;
        public readonly float PercentComplete;
    
        public readonly bool HasLabelUpdate;
        public readonly bool HasPercentUpdate;
    
        public override bool keepWaiting
        {
            get
            {
                return false;
            }
        }

        public EditorStatusUpdate(string label, float percent)
        {
            HasPercentUpdate = true;
            PercentComplete = percent;
    
            HasLabelUpdate = true;
            Label = label;
        }
    }
    
    public class EditorCoroutine : YieldInstruction
    {
        public bool HasFinished;
    }
}
