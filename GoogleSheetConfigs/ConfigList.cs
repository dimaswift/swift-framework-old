﻿using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.GoogleSheetConfigs
{
    public abstract class ConfigList<T> : SheetParserContainer where T : class, new()
    {
        [SerializeField] private List<T> data;

        public List<T> GetItems()
        {
            return data;
        }
        
        protected override void ParseSheet(string[] rows)
        {
            if(data == null)
                data = new List<T>();
            SheetParser.ParseToList(data, rows, GetSeparator(),  GetCustomParsers());
            SetFileDirty();
        }
    }
}

