﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.GoogleSheetConfigs
{
    public abstract class ConfigDictionary<T> : SheetParserContainer where T : class, IUniqueItem, new() 
    {
        public List<T> Items
        {
            get { return items; }
        }
        
        [NonSerialized] private Dictionary<string, T> dict;
        [SerializeField] private List<T> items;

        
        public T this[string id]
        {
            get
            {
                if(dict == null)
                    FillDictionary();
                T value;
                if (!dict.TryGetValue(id, out value))
                {
                    Debug.LogError("Item with Id: " + id +$" not found in the config {name}");
                    return default(T);
                }
                return value;
            }
        }

        public bool Contains(string id)
        {
            if(dict == null)
                FillDictionary();
            return dict.ContainsKey(id);
        }
        
        public T GetById(string id)
        {
            return this[id];
        }
        
        public virtual void OnItemsDownloaded(List<T> items) { }

        protected override void ParseSheet(string[] rows)
        {
            if(items == null)
                items = new List<T>();
            SheetParser.ParseToList(items, rows, GetSeparator(),  GetCustomParsers());
            OnItemsDownloaded(items);
            FillDictionary();
        }
             
        private void FillDictionary()
        {
            dict = new Dictionary<string, T>(Items.Count);
            Items.RemoveAll(i => i == null || i.Id == null);
            foreach (var item in Items)
            {
                if (dict.ContainsKey(item.Id))
                {
                    Debug.LogError("Cannot add item to the dictionary: <color='white'>" +item.Id + "</color>. Such Id already added!");
                    continue;
                }
                dict.Add(item.Id, item);
            }
        }
    }
}

