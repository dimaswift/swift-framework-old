﻿using System;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace SwiftFramework.GoogleSheetConfigs
{
    public abstract class SheetParserContainer : DownloadableObject
    {
        [SerializeField] private string url = "";

        private string FilePath
        {
            get
            {
                return  Application.persistentDataPath + "/" + name;
            }
        }

        public bool Downloaded { get; set; }
        
        private Action<bool> downloadCallback;

        private readonly ICustomStringParser[] defaultParsers = { };
        
        private bool MyRemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            if (sslPolicyErrors != SslPolicyErrors.None) 
            {
                for (int i=0; i<chain.ChainStatus.Length; i++) 
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown) 
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan (0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build ((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }



        public override void StartDownloading(Action<bool> downloadCallback)
        {
            this.downloadCallback = downloadCallback;
           
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
                    var myWebClient = new WebClient();
                    var path = FilePath;
					myWebClient.DownloadFileAsync(new Uri(url), path);
                    myWebClient.DownloadFileCompleted += OnDownloaded;
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                    OnDownloaded(null, null);
                }
            }
            else
            {
                Debug.LogError("No internet!");
                OnDownloaded(null, null);
            }
        }

        protected virtual ICustomStringParser[] GetCustomParsers()
        {
            return defaultParsers;
        }
        
        protected abstract void ParseSheet(string[] rows);

        protected char GetSeparator()
        {
            return url.EndsWith("tsv") ? '\t' : ',';
        }
        
        public void ParseLocalFile()
        {
            if(!File.Exists(FilePath))
                return;
            var cvsText = File.ReadAllLines(FilePath).ToArray();
            ParseSheet(cvsText);
            SetFileDirty();
        }

        public void CheckDownload()
        {
            if (Downloaded)
            {
                ParseLocalFile();
            }
        }
        
        public void OnDownloaded(object sender, AsyncCompletedEventArgs e)
        {
            Downloaded = e != null && e.Error == null;

            if(e?.Error != null)
                Debug.LogError(e.Error);
            
            if (!Application.isEditor || Application.isPlaying)
            {
                Dispatcher.RunOnMainThread(() =>
                {
                    CheckDownload();
                    downloadCallback(Downloaded);
                    Downloaded = false;
                });
                return;
            }


            if (downloadCallback != null)
                downloadCallback.Invoke(Downloaded);
            
            CheckDownload();
            
        }

        protected void SetFileDirty()
        {
#if UNITY_EDITOR
                
            UnityEditor.EditorUtility.SetDirty(this);
            UnityEditor.AssetDatabase.SaveAssets();
#endif
        }
    }
}

