﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
#if ENABLE_FACEBOOK
using Facebook.Unity;
#endif

namespace SwiftFramework.Facebook
{
    public class FacebookManager : Module, IAnalyticsManager
    {
        private readonly Dictionary<string, object> paramsBuffer = new Dictionary<string, object>(10);

        protected override IPromise GetLoadPromise()
        {
            Promise loadPromise = Promise.Create();

#if ENABLE_FACEBOOK
            FB.Init(() => 
            {
                loadPromise.Resolve();
            });
#else
            Debug.LogWarning("Facebook is disabled! Add ENABLE_FACEBOOK define symbol to enable it!");
            loadPromise.Resolve();
#endif

            return Promise.Race(PromiseTimer.WaitForUnscaled(1f), loadPromise);
        }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {
#if ENABLE_FACEBOOK
            if (FB.IsInitialized == false)
            {
                return;
            }
            paramsBuffer.Clear();
            foreach (var pair in args)
            {
                if(paramsBuffer.ContainsKey(pair.key) == false)
                {
                    paramsBuffer.Add(pair.key, pair.value);
                }
            }
            FB.LogAppEvent(eventName, 1, paramsBuffer);
#endif
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
#if ENABLE_FACEBOOK
            if(FB.IsInitialized == false)
            {
                return;
            }
            var parameters = new Dictionary<string, object>();
            parameters.Add("transactionID", transactionId);
            parameters.Add("productId", productId);
            FB.LogPurchase((float)price, currencyCode, parameters);

        
#endif
        }

        public void LogRewardedVideoStarted(string placementId)
        {
#if ENABLE_FACEBOOK
            if (FB.IsInitialized == false)
            {
                return;
            }
            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.ContentID, placementId },
            };
            FB.LogAppEvent("rewarded_ad_started", 1, parameters);
#endif
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
#if ENABLE_FACEBOOK
            if (FB.IsInitialized == false)
            {
                return;
            }
           
            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.ContentID, placementId }
            };
            FB.LogAppEvent(AppEventName.ViewedContent, 1, parameters);
#endif
        }
    }

}
