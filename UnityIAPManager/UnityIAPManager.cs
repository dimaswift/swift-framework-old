﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;


#if UNITY_PURCHASING && ENABLE_IAP
using UnityEngine.Purchasing.Security;
using UnityEngine.Purchasing;
#endif

namespace SwiftFramework.UnityIAPManager
{
    public class UnityIAPManager : Module, IInAppPurchaseManager
#if UNITY_PURCHASING && ENABLE_IAP
        , IStoreListener
#endif
    {
        [SerializeField] private ProductDefinition[] products = { };
        [SerializeField] private bool enableValidation = false;
        public override bool LoadModuleWithAppStart => true;
        private readonly List<Product> alreadyPurchasedProducts = new List<Product>();

#if UNITY_PURCHASING && ENABLE_IAP

        private const string defaultCurrencyCode = "USD";

        private IStoreController storeController;
        private IExtensionProvider storeExtensionProvider;
        private Promise<bool> pendingPurchasePromise;
        private Promise initPromise;

        public event Action<string> OnItemPurchased = IDeepCopy => { };

        public Product GetProduct(string id)
        {
            return storeController != null && storeController.products != null ? storeController.products.WithID(id) : null;
        }

        public IPromise Init(IEnumerable<ProductDefinition> products)
        {
            initPromise = Promise.Create();

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            
            foreach (var product in products)
            {
                builder.AddProduct(product.id, product.consumable ? ProductType.Consumable : ProductType.NonConsumable);
            }

            UnityPurchasing.Initialize(this, builder);

            return initPromise;
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            PromiseTimer.WaitForNextFrame().Done(() =>
            {
                initPromise.Resolve();
                storeController = controller;
                storeExtensionProvider = extensions;
            });
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.LogError($"Unity AIP initialization failed: {error}");
            initPromise.Resolve();
        }

        public IPromise<bool> Purchase(string productId)
        {
            pendingPurchasePromise = Promise<bool>.Create();

            if (Initialized == false)
            {
                Debug.LogError($"Unity IAP is not initialized!");
                pendingPurchasePromise.Resolve(false);
                return pendingPurchasePromise;
            }

            Product product = GetProduct(productId);

            if (product == null)
            {
                Debug.LogError($"Product with id {productId} not found!");
                pendingPurchasePromise.Resolve(false);
                return pendingPurchasePromise;
            }

            storeController.InitiatePurchase(productId);

            return pendingPurchasePromise;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (pendingPurchasePromise != null)
            {
                pendingPurchasePromise.Resolve(false);
            }
            Debug.LogError(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            if (pendingPurchasePromise == null)
            {
                Debug.LogError($"Purchase completed without initiating it manually: {args.purchasedProduct.definition.id}");
                alreadyPurchasedProducts.Add(args.purchasedProduct);
                OnItemPurchased(args.purchasedProduct.definition.id);
                return PurchaseProcessingResult.Complete;
            }

            bool isValid = true;

#if UNITY_ANDROID
            if (enableValidation && Application.isEditor == false)
            {
                var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
                try
                {
                    var result = validator.Validate(args.purchasedProduct.receipt);
                    Debug.Log("Receipt is valid. Contents:");
                    foreach (IPurchaseReceipt productReceipt in result)
                    {
                        Debug.Log(productReceipt.productID);
                        Debug.Log(productReceipt.purchaseDate);
                        Debug.Log(productReceipt.transactionID);
                    }
                }
                catch (IAPSecurityException e)
                {
                    Debug.Log($"Invalid receipt: {e?.Message}");
                    isValid = false;
                }
#endif
            }

            if (isValid)
            {
                pendingPurchasePromise.Resolve(true);
                OnItemPurchased(args.purchasedProduct.definition.id);
                App.Analytics.LogPurchase(args.purchasedProduct.definition.id, args.purchasedProduct.metadata.isoCurrencyCode, args.purchasedProduct.metadata.localizedPrice, args.purchasedProduct.transactionID);
            }
            else
            {
                OnPurchaseFailed(args.purchasedProduct, PurchaseFailureReason.SignatureInvalid);
            }

            return isValid ? PurchaseProcessingResult.Complete : PurchaseProcessingResult.Pending;
        }

#endif

        [Serializable]
        public struct ProductDefinition
        {
            public string id;
            public bool consumable;
        }

        public string GetPriceString(string productId)
        {
#if UNITY_PURCHASING && ENABLE_IAP
            Product product = GetProduct(productId);
            if (product == null)
            {
                return "";
            }
            return product.metadata.localizedPriceString;
#else
            LogPurchasingDisabled();
            return default;
#endif
        }

        public decimal GetPrice(string productId)
        {
#if UNITY_PURCHASING && ENABLE_IAP
            Product product = GetProduct(productId);
            if (product == null)
            {
                return 0;
            }
            return product.metadata.localizedPrice;
#else
            LogPurchasingDisabled();
            return default;
#endif
        }

        private void LogPurchasingDisabled()
        {
            Debug.LogWarning("Unity purchasing is disabled! Add define symbol ENABLE_IAP in the project settings!");
        }

        public string GetCurrencyCode(string productId)
        {
#if UNITY_PURCHASING && ENABLE_IAP
            Product product = GetProduct(productId);
            if (product == null)
            {
                return "";
            }
            return product.metadata.isoCurrencyCode;

#else
            LogPurchasingDisabled();
            return default;
#endif
        }


        public IPromise<bool> RestorePurchases()
        {
            Promise<bool> result = Promise<bool>.Create();
#if UNITY_PURCHASING && ENABLE_IAP
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
#if UNITY_IPHONE
                IAppleExtensions apple = storeExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions(success => 
                {
                    result.Resolve(success);
                });
#endif
            }
            else
            {
                result.Resolve(false);
            }
#else
            LogPurchasingDisabled();
            result.Resolve(false);
#endif

            return result;
        }

        protected override IPromise GetLoadPromise()
        {
#if UNITY_PURCHASING && ENABLE_IAP
            Init(products);
            return Promise.Resolved();
#else
            LogPurchasingDisabled();
            return Promise.Resolved();
#endif
        }

        public bool IsSubscribed(string productId)
        {
#if UNITY_PURCHASING && ENABLE_IAP
            if (storeController != null)
            {
                Product product = GetProduct(productId);
                if(product == null)
                {
                    return false;
                }
                return product.hasReceipt;
            }
            return false;
#else
            return false;
#endif
        }

        public bool IsAlreadyPurchased(string productId)
        {
            foreach (var item in alreadyPurchasedProducts)
            {
                if(item.definition.id == productId)
                {
                    return true;
                }
            }

            Product product = GetProduct(productId);
            if(product != null)
            {
                return product.hasReceipt;
            }
            return false;
        }

        public IPromise<bool> Buy(string productId)
        {
#if UNITY_PURCHASING && ENABLE_IAP
            return Purchase(productId);
#else
            LogPurchasingDisabled();
            return Promise<bool>.Resolved(true);
#endif
        }
    }
}