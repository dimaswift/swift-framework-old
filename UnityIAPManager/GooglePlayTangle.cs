#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("uAqJqriFjoGiDsAOf4WJiYmNiIvCCl2UTS98/WCa8bfDeReYGXy61oK5lyjd0Jz6JZu1scNOylmFzy8A8WAVJgJVJU7a/cZeipQDTJIehVrZNKjZTM/Aye6J0UNxQgzIi7+OnepzUoZycWJmy9qz/OJ/Sf6yNcUU1xzlK5mbKFVVweA3XVQXslNOGWgN+G+D5Z7YyWELvEdDisWkzmKzqsTotjLBsbwNE9szSg4Fn+B8Zyj3B86xyaYqLamObeS3ufZYa8ZCivSON+g/oi9TQymU4JY/ME1dqX6DYAqJh4i4ComCigqJiYgDAxfxNHKSIT0r1ZjSDr5vBpAqwhL3g7JzlVoG/7SfEMZ2gcA5M4YcLMNSKI3cyuDrReFFej/9m4qLiYiJ");
        private static int[] order = new int[] { 0,7,2,10,9,5,6,12,13,9,11,12,13,13,14 };
        private static int key = 136;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
