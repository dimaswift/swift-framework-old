#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("WMzzCLGfTK7RJiyzVfaYfi+flafoyd7bjdzSy9KZqKi0vfiRtrv26ai0vfibvaqssb6xu7mssbe2+Jmtsb6xu7mssbe2+JmtrLC3qrGsoene6Nfe243Fy9nZJ9zd6NvZ2Sfoxai0vfiKt7es+JuZ6MbP1eju6Ozq+Lm2vPi7vaqssb6xu7mssbe2+Kjr7oLouunT6NHe243c3svajYvpy/i3vvissL34rLC9tvi5qKi0sbu50IboWtnJ3tuNxfjcWtnQ6FrZ3Oivr/a5qKi0vfa7t7X3uaiotL27uWnogDSC3OpUsGtXxQa9qye/hr1kx0kDxp+IM901hqFc9TPueo+UjTTuQZT1oG81VEMEK69DKq4Kr+iXGXAEpvrtEv0NAdcOswx6/PvJL3l0ZiyrQzYKvNcToZfsAHrmIaAnsxD+6Pze243c08vFmaiotL34m72qrM7ozN7bjdzby9WZqKi0vfiKt7es6FrcY+ha23t429rZ2trZ2ujV3tHd2Nta2dfY6FrZ0tpa2dnYPElx0aq5u6yxu734q6y5rL21vbasq/boWtnY3tHyXpBeL7u83dnoWSro8t6h+Lmrq621vav4ubu7vaisuba7vd80peFbU4v4C+AcaWdCl9KzJ/Mk1d7R8l6QXi/V2dnd3djbWtnZ2ISssb6xu7msvfi6ofi5tqH4qLmqrLzt+82TzYHFa0wvLkRGF4hiGYCIir20sbm2u734t7b4rLCxq/i7vaqssLeqsayh6c7ozN7bjdzby9WZqPaYfi+flafQhujH3tuNxfvcwOjOouha2a7o1t7bjcXX2dkn3Nzb2tnc3svajYvpy+jJ3tuN3NLL0pmoqG/DZUua/MryH9fFbpVEhrsQk1jP8l6QXi/V2dnd3djouunT6NHe242nmXBAIQkSvkT8s8kIe2M8w/Ibx+3q6ezo6+6Cz9Xr7ejq6OHq6ezo9Pi7vaqssb6xu7msvfiot7Sxu6GBf93RpM+YjsnGrAtvU/vjn3sNt7q0vfirrLm2vLmqvPisvaq1q/i50PPe2d3d39rZzsawrKyoq+L3969t4nUs19bYStNp+c72rA3k1QO6zp2mx5SziE6ZURysutPIW5lf61JZV6tZuB7Dg9H3SmognJAouOBGzS2RAK5H68y9ea9MEfXa29nY2Xta2bS9+JG2u/bp/uj83tuN3NPLxZmoTUai1HyfU4MMzu/rExzXlRbMsQnHXVtdw0Hln+8qcUOYVvQMaUjKAHN7qUqfi40Zd/eZayAjO6gVPnuUAe6nGV+NAX9BYeqaIwANqUameYr36Fkb3tDz3tnd3d/a2uhZbsJZaxHBqi2F1g2nh0Mq/dtijVeVhdUp5f6/+FLrsi/VWhcGM3v3IYuyg7y/V9Bs+C8TdPT4t6hu59noVG+bF9dF5SvzkfDCECYWbWHWAYbEDhPl/DozCW+oB9edOf8SKbWgNT9tz88Yu+uvL+Lf9I4zAtf51gJiq8GXbVPBUQYhk7Qt33P66NowwOYgiNELtrz4u7e2vLGssbe2q/i3vvitq73e243F1tzO3MzzCLGfTK7RJiyzVfibmeha2fro1d7R8l6QXi/V2dnZiHJSDQI8JAjR3+9ora35");
        private static int[] order = new int[] { 32,9,4,11,29,6,45,8,41,33,36,26,15,48,54,50,36,50,32,54,46,34,40,51,47,39,46,38,45,31,45,31,59,33,50,41,44,52,42,41,51,41,55,55,46,59,49,47,58,52,54,56,54,56,56,57,56,57,59,59,60 };
        private static int key = 216;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
