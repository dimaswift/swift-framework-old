﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.CameraDragController
{
    public class CameraDragController : Module, ICameraController
    {
        [SerializeField] private float lerpSpeed = 10;

        private Transform camTransform;
        private Camera mainCamera;

        private bool locked = false;

        private Vector3 dragStartPosition = default;
        private Vector3 dragStartMousePosition = default;

        public Camera Main
        {
            get
            {
                if (mainCamera == null)
                {
                    camTransform = mainCamera.transform;
                    mainCamera = Camera.main;
                }
                return mainCamera;
            }
        }

        public override bool LoadModuleWithAppStart => false;

        protected override void OnInit()
        {
            mainCamera = Camera.main;
            camTransform = mainCamera.transform;
        }

        public void Lock()
        {
            locked = true;
        }

        public void Release()
        {
            locked = false;
        }

        public Vector3 GetWorldPoint(Vector3 screenPoint, float z)
        {
            Vector3 pos = camTransform.position;
            pos.z = z;
            Plane plane = new Plane(camTransform.forward, pos);
            Ray ray = Main.ScreenPointToRay(screenPoint);
            plane.Raycast(ray, out float enter);
            return ray.GetPoint(enter);
        }

        private void Update()
        {
            if(Initialized == false)
            {
                return;
            }
            if(locked == false)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    dragStartPosition = camTransform.position;
                    dragStartMousePosition = GetWorldPoint(Input.mousePosition, 0);
                }
                if (Input.GetMouseButton(0))
                {
                    var mousePos = GetWorldPoint(Input.mousePosition, 0);
                    camTransform.position = Vector3.Lerp(camTransform.position, dragStartPosition + (dragStartMousePosition - mousePos), Time.unscaledDeltaTime * lerpSpeed);
                }
            }
        }
    }

}
