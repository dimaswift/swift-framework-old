﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.AnalyticsHub
{
    public class AnalyticsHub : CompositeModule, IAnalyticsManager
    {
        protected override void OnInit()
        {
            base.OnInit();
            App.AdsManager.OnRewardedShowStarted += AdsManager_OnRewardedShowStarted;
            App.AdsManager.OnRewardedSuccessfully += AdsManager_OnRewardedSuccessfully;
        }

        private void AdsManager_OnRewardedSuccessfully(string placementId)
        {
            LogRewardedVideoSuccess(placementId);
        }

        private void AdsManager_OnRewardedShowStarted(string placementId)
        {
            LogRewardedVideoStarted(placementId);
        }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {
            Debug.Log($"ANALYTICS EVENT: {eventName}"); 
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogEvent(eventName, args);
            }
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
            Debug.Log($"ANALYTICS PURCHASE EVENT: {productId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogPurchase(productId, currencyCode, price, transactionId);
            }
        }

        public void LogRewardedVideoStarted(string placementId)
        {
            Debug.Log($"ANALYTICS REWARDED STARTED: {placementId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogRewardedVideoStarted(placementId);
            }
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
            Debug.Log($"ANALYTICS REWARDED SUCCESS: {placementId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogRewardedVideoSuccess(placementId);
            }
        }
    }
}
