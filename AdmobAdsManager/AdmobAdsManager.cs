﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;

#if ENABLE_ADMOB
using GoogleMobileAds.Api;
#endif

namespace SwiftFramework.AdmobAdsManager
{
    public class AdmobAdsManager : Module, IAdsManager
    {
        [Serializable]
        public class AdsConfig
        {
            public string rewardedAdUnitId;
            public string interstitialUnitId;
            public string bannerUnitId;
        }

        [SerializeField] private AdsConfig androidConfig = new AdsConfig();
        [SerializeField] private AdsConfig iOSConfig = new AdsConfig();

        private readonly AdsConfig androidTestConfig = new AdsConfig()
        {
            rewardedAdUnitId = "ca-app-pub-3940256099942544/5224354917",
            bannerUnitId = "ca-app-pub-3940256099942544/6300978111",
            interstitialUnitId = "ca-app-pub-3940256099942544/1033173712"
        };

        private readonly AdsConfig iOSTestConfig = new AdsConfig()
        {
            rewardedAdUnitId = "ca-app-pub-3940256099942544/1712485313",
            bannerUnitId = "ca-app-pub-3940256099942544/2934735716",
            interstitialUnitId = "ca-app-pub-3940256099942544/4411468910"
        };

        [SerializeField] private int rewardedCacheSize = 3;
        [SerializeField] private int interstitialCacheSize = 3;
        [SerializeField] private int interstitialShowFrequency = 2;
        [SerializeField] private float loadDelayAfterLoadingFailure = 5;
        [SerializeField] private bool useTestAds = true;


        private float adCloseTime;
        public event Action<string> OnRewardedShowStarted = p => { };
        public event Action<string> OnRewardedSuccessfully = p => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedClosed = p => { };

#if ENABLE_ADMOB

        private readonly List<RewardedAd> rewardedAdsCache = new List<RewardedAd>();
        private readonly List<InterstitialAd> interstitialAdsCache = new List<InterstitialAd>();
        private Promise<bool> rewardedShowPromise;
        private string currentPlacementId;
        private bool rewardEarned;
        private int interstitialShowAttemptCounter;
        public bool IsShowingRewardedAd()
        {
            return rewardedShowPromise != null;
        }

        protected override IPromise GetLoadPromise()
        {
            var loadPromise = Promise.Create();

            if (Application.isEditor)
            {
                loadPromise.Resolve();
                return loadPromise;
            }

            MobileAds.Initialize(initStatus =>
            {
                Debug.Log("Admob adapters fully initialized");
            });

            TryAddRewardedToCache();

            TryAddInterstitialToCache();

            loadPromise.Resolve();

            return loadPromise;
        }

        private void TryAddRewardedToCache()
        {
            if(rewardedAdsCache.Count >= rewardedCacheSize)
            {
                return;
            }

            RewardedAd rewardedAd = new RewardedAd(GetConfig().rewardedAdUnitId);

            rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;

            rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;

            rewardedAd.OnAdOpening += HandleRewardedAdOpening;

            rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;

            rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;

            rewardedAd.OnAdClosed += HandleRewardedAdClosed;

            AdRequest request = new AdRequest.Builder().Build();

            rewardedAd.LoadAd(request);

            rewardedAdsCache.Add(rewardedAd);

            Debug.Log($"Adding rewarded ad to the cache... Current cache size: {rewardedAdsCache.Count}");
        }

        private void TryAddInterstitialToCache()
        {
            if (interstitialAdsCache.Count >= interstitialCacheSize)
            {
                return;
            }
            InterstitialAd ad = new InterstitialAd(GetConfig().interstitialUnitId);

            ad.OnAdLoaded += HandleInterstitialAdLoaded;

            ad.OnAdClosed += HandleInterstitialAdClosed;

            ad.OnAdFailedToLoad += HandleInterstitialAdFailedToLoad;

            AdRequest request = new AdRequest.Builder().Build();

            ad.LoadAd(request);

            interstitialAdsCache.Add(ad);

            Debug.Log($"Admob Ads Manager: Adding interstitial ad to the cache... Current cache size: {interstitialAdsCache.Count}");
        }

        private void HandleInterstitialAdClosed(object sender, EventArgs e)
        {
            PromiseTimer.WaitForNextFrame().Done(() =>
            {
                adCloseTime = Time.realtimeSinceStartup;
                TryAddInterstitialToCache();
            });
        }

        private void HandleRewardedAdClosed(object sender, EventArgs e)
        {
            PromiseTimer.WaitForNextFrame().Done(() =>
            {
                adCloseTime = Time.realtimeSinceStartup;
                if (rewardedShowPromise != null)
                {
                    if (rewardEarned)
                    {
                        rewardedShowPromise.Resolve(true);
                        OnRewardedSuccessfully(currentPlacementId);
                    }
                    else
                    {
                        rewardedShowPromise.Resolve(false);
                    }
                    OnRewardedClosed(currentPlacementId);
                    rewardEarned = false;
                    rewardedShowPromise = null;
                    TryAddRewardedToCache();
                }
            });  
        }

        private void HandleUserEarnedReward(object sender, Reward e)
        {
            rewardEarned = true;
        }

        private void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs e)
        {
            PromiseTimer.WaitForNextFrame().Done(() =>
            {
                if (rewardedShowPromise != null)
                {
                    OnRewardedClosed(currentPlacementId);
                    rewardedShowPromise.Resolve(false);
                    rewardedShowPromise = null;
                }
            });
        }

        private void HandleRewardedAdOpening(object sender, EventArgs e)
        {
            
        }

        private void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs e)
        {
            Debug.LogError($"Admob Ads Manager: Rewarded ad failed to load: {e?.Message}");
            rewardedAdsCache.RemoveAll(ad => ad.IsLoaded() == false);
            PromiseTimer.WaitForUnscaled(loadDelayAfterLoadingFailure).Done(TryAddRewardedToCache);
        }

        private void HandleRewardedAdLoaded(object sender, EventArgs e)
        {
            OnRewardedAdLoaded();
            TryAddRewardedToCache();
        }

        private AdsConfig GetConfig()
        {
#if UNITY_ANDROID
            return useTestAds ? androidTestConfig : androidConfig;
#elif UNITY_IPHONE
            return useTestAds ? iOSTestConfig : iOSConfig;
#else
            return new AdsConfig()
            {
                rewardedAdUnitId = "unexpected_platform",
                bannerUnitId = "unexpected_platform",
                interstitialUnitId = "unexpected_platform"
            };
#endif
        }

        private void HandleInterstitialAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            Debug.LogError($"Admob Ads Manager: Failed to load interstitial ad: {e?.Message}");
            PromiseTimer.WaitForUnscaled(loadDelayAfterLoadingFailure).Done(TryAddInterstitialToCache);
        }

        private void HandleInterstitialAdLoaded(object sender, EventArgs e)
        {
            Debug.LogError($"Admob Ads Manager: Loaded intersitial. Cache size: {interstitialAdsCache.Count}");
        }

        public IPromise<bool> ShowRewarded(string placementId)
        {
            rewardedShowPromise = Promise<bool>.Create();
            if (IsRewardedReady() == false)
            {
                rewardedShowPromise.Resolve(false);
                return rewardedShowPromise;
            }

            rewardEarned = false;
            currentPlacementId = placementId;
       
            if (Application.isEditor)
            {
                rewardedShowPromise.Resolve(true);
                OnRewardedClosed(placementId);
                return rewardedShowPromise;
            }
            OnRewardedShowStarted(currentPlacementId);
            for (int i = rewardedAdsCache.Count - 1; i >= 0; i--)
            {
                var ad = rewardedAdsCache[i];
                if (ad.IsLoaded())
                {
                    ad.Show();
                    rewardedAdsCache.RemoveAt(i);
                    break;
                }
            }

            Debug.Log($"Admob Ads Manager: Showing rewarded from cache. Remaining cache size: {rewardedAdsCache.Count}");
            return rewardedShowPromise;
        }

        private void OnApplicationPause(bool paused)
        {
            if(paused == false)
            {
                PromiseTimer.WaitFor(.5f).Done(() =>
                {
                    if(rewardedShowPromise != null)
                    {
                        rewardedShowPromise.Resolve(true);
                        rewardedShowPromise = null;
                    }
                });
            }
        }

        public bool IsRewardedReady()
        {
            if (Application.isEditor)
            {
                return true;
            }

            for (int i = 0; i < rewardedAdsCache.Count; i++)
            {
                if (rewardedAdsCache[i].IsLoaded())
                {
                    return true;
                }
            }

            return false;
        }

        public float GetTimeSinceAdClosed()
        {
            return Time.realtimeSinceStartup - adCloseTime;
        }

        public bool ShowInterstitial(string placementId)
        {
            for (int i = interstitialAdsCache.Count - 1; i >= 0; i--)
            {
                InterstitialAd ad = interstitialAdsCache[i];
                if (ad.IsLoaded())
                {
                    ad.Show();
                    interstitialAdsCache.RemoveAt(i);
                    TryAddInterstitialToCache();
                    return true;
                }
            }
            return false;
        }

        public bool IsInterstitialReady()
        {
            foreach (InterstitialAd ad in interstitialAdsCache)
            {
                if (ad.IsLoaded())
                {
                    return true;
                }
            }

            return false;
        }

        public void TryShowInterstitial()
        {
            interstitialShowAttemptCounter++;
            if(interstitialShowAttemptCounter >= interstitialShowFrequency)
            {
                if(ShowInterstitial(""))
                {
                    interstitialShowAttemptCounter = 0;
                }
            }
        }

        public void ResetInterstitialCounter()
        {
            interstitialShowAttemptCounter = 0;
        }
#else

        private const string defineSymbol = "ENABLE_ADMOB";

        public bool IsRewardedReady()
        {
            LogNotEnabledWarning(defineSymbol);
            return true;
        }

        public float GetTimeSinceAdClosed()
        {
            return float.MaxValue;
        }

        public bool IsShowingRewardedAd()
        {
            LogNotEnabledWarning(defineSymbol);
            return false;
        }

        public IPromise<bool> ShowRewarded(string placementId)
        {
            LogNotEnabledWarning(defineSymbol);
            return Promise<bool>.Resolved(true);
        }

        protected override IPromise GetLoadPromise()
        {
            LogNotEnabledWarning(defineSymbol);
            return base.GetLoadPromise();
        }

        public bool ShowInterstitial(string placementId)
        {
            LogNotEnabledWarning(defineSymbol);
            return false;
        }

        public bool IsInterstitialReady()
        {
            LogNotEnabledWarning(defineSymbol);
            return false;
        }

        public void TryShowInterstitial()
        {
            
        }

        public void ResetInterstitialCounter()
        {
            
        }
#endif
    }
}