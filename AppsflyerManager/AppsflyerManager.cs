﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Appsflyer
{
    public class AppsflyerManager : Module, IAnalyticsManager
    {
        [SerializeField] private string devKey = null;

        protected override IPromise GetLoadPromise()
        {
            Promise result = Promise.Create();
#if ENABLE_APPSFLYER
            AppsFlyer.setAppsFlyerKey(devKey);

            if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
                AppsFlyer.setAppID(devKey);
                AppsFlyer.trackAppLaunch();
            }
            else if(Application.platform == RuntimePlatform.Android)
            {
                AppsFlyer.setAppID(Application.identifier);
                gameObject.AddComponent<AppsFlyerTrackerCallbacks>();
                AppsFlyer.init(devKey, name);
            }
#else
            Debug.LogWarning("Appsflyer is disabled! Add ENABLE_APPSFLYER define symbol in project settings to enable it!");
#endif
            result.Resolve();
            return result;
        }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {

        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
#if ENABLE_APPSFLYER
            var eventData = new Dictionary<string, string>();
            eventData.Add(AFInAppEvents.CONTENT_ID, productId);
            eventData.Add(AFInAppEvents.PRICE, price.ToString());
            eventData.Add(AFInAppEvents.CURRENCY, currencyCode);
            eventData.Add(AFInAppEvents.REVENUE, price.ToString());
            AppsFlyer.trackRichEvent(AFInAppEvents.PURCHASE, eventData);
#endif
        }

        public void LogRewardedVideoStarted(string placementId)
        {
           
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
#if ENABLE_APPSFLYER
            var eventData = new Dictionary<string, string>();
            eventData.Add("af_adrev_ad_type", placementId);
            eventData.Add(AFInAppEvents.CURRENCY, "USD");
            eventData.Add(AFInAppEvents.REVENUE, "0.01");
            AppsFlyer.trackRichEvent("af_ad_view", eventData);
#endif
        }
    }
}
