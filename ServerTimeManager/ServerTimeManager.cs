﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;

namespace SwiftFramework.ServerTime
{
    public class ServerTimeManager : Module, IServerTime
    {
        public IStatefulEvent<long> Now
        {
            get
            {
                Update();
                return now;
            }
        }

        private long prevNow;
        private long startTime;

        private readonly StatefulEvent<long> now = new StatefulEvent<long>();
   
        private float startTimeOffset;

        protected override IPromise GetLoadPromise()
        {
            Promise result = Promise.Create();
            GetUnixNow().Done(now => 
            {
                startTime = now;
                startTimeOffset = Time.realtimeSinceStartup;
                result.Resolve();
            });
            return result;
        }

        private void Update()
        {
            if(Initialized == false)
            {
                return;
            }
            var newNow = startTime + Mathf.FloorToInt(Time.realtimeSinceStartup - startTimeOffset);
            if (prevNow != newNow)
            {
                prevNow = newNow;
                now.SetValue(newNow);
            }
        }

        public IPromise<DateTime> GetNow()
        {
            return Promise<DateTime>.Resolved(DateTime.Now);
        }

        public IPromise<long> GetUnixNow()
        {
            return Promise<long>.Resolved(DateTimeOffset.Now.ToUnixTimeSeconds());
        }
    }
}