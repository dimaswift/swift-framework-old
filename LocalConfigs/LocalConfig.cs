﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.LocalConfigs
{
    public class LocalConfig<T> : ScriptableObject where T : new()
    {
        [SerializeField] private T data = default;

        public T Data => data;
    }

}
