﻿using UnityEngine;
using SwiftFramework.Core;
using System.Collections.Generic;
using System;

namespace SwiftFramework.LocalConfigs
{
    public sealed class LocalConfigManager : Module, IConfigManager
    {
        [SerializeField] private string rootResourcesFolder = "Configs";

        private readonly List<ScriptableObject> configs = new List<ScriptableObject>();

        private readonly Dictionary<Type, object> configsCache = new Dictionary<Type, object>();

        public T GetPureConfig<T>() where T : new()
        {
            var type = typeof(T);
            if (configsCache.ContainsKey(type))
            {
                return (T)configsCache[type];
            }

            LocalConfig<T> config = FindConfig<LocalConfig<T>>() as LocalConfig<T>;

            if (config == null)
            {
                Debug.LogWarning($"Cannot find config of type {type.Name}! Should be inside Resources/{rootResourcesFolder} and inherit LocalConfig<{type.Name}>");
                return new T();
            }

            configsCache.Add(type, config.Data);
            return config.Data;
        }


        private ScriptableObject FindConfig<T>() where T : new()
        {
            return configs.Find(c => c is T) as ScriptableObject;
        }

        protected override IPromise GetInitPromise()
        {
            Promise initPromise = Promise.Create();
            App.ResourcesManager.InitModule(App).Done(() =>
            {
                App.ResourcesManager.LoadAll(rootResourcesFolder, configs);
                initPromise.Resolve();
            });
            return initPromise;
        }

        public T GetConfig<T>() where T : ScriptableObject
        {
            foreach (var config in configs)
            {
                if(config is T)
                {
                    return config as T;
                }
            }
            Debug.LogWarning($"Cannot find config of type {typeof(T).Name}! Should be inside Resources/{rootResourcesFolder}!");
            return null;
        }
    }
}
