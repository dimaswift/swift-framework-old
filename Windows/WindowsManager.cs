﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using SwiftFramework.Core;
using System;
using UnityEngine.UI;

namespace SwiftFramework.Windows
{
    public class WindowsManager : Module, IWindowsManager
    {
        public override bool LoadModuleWithAppStart => true;

        [SerializeField] private string windowsResourcesFolder = "Windows";

        [SerializeField] private AnimationCurve animationScaleCurve = AnimationCurve.Linear(0, 0, 1, 1);
        [SerializeField] private AnimationCurve animationAlphaCurve = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField] private float animationDuration = .5f;

        private readonly Stack<Window> windowStack = new Stack<Window>();

        private readonly List<Window> allWindows = new List<Window>();

        private readonly Dictionary<Type, Window> windowInstances = new Dictionary<Type, Window>();

        private readonly List<Window> windowBuffer = new List<Window>();

        private readonly List<Window> windowShowBuffer = new List<Window>();

        private readonly List<Window> animationShowWindowBuffer = new List<Window>();

        private readonly List<Window> animationHideWindowBuffer = new List<Window>();

        private Container showContainer;

        private Container hideContainer;

        private Coroutine showRoutine;

        private Coroutine hideRoutine;

        private RectTransform viewport;

        private readonly Queue<Action> actionQueue = new Queue<Action>();

        public bool IsShowingAnimation()
        {
            return showRoutine != null || hideRoutine != null;
        }

        public IWindow GetBottomWindow()
        {
            int i = 0;
            foreach (var window in windowStack)
            {
                if (i == windowStack.Count - 1)
                {
                    return window;
                }
                i++;
            }
            return default;
        }

        public IWindow GetTopWindow()
        {
            return windowStack.Count > 0 ? windowStack.Peek() : default;
        }

        protected override IPromise GetInitPromise()
        {
            showContainer = new Container("ShowContainer", transform);
            hideContainer = new Container("HideContainer", transform);
            viewport = GetComponent<RectTransform>();
            hideContainer.rectTransform.sizeDelta = viewport.sizeDelta;
            showContainer.rectTransform.sizeDelta = viewport.sizeDelta;
            App.ResourcesManager.LoadAll(windowsResourcesFolder, allWindows);
            for (int i = 0; i < allWindows.Count; i++)
            {
                Window windowPrefab = allWindows[i];
                Type type = windowPrefab.GetType();

                if (windowInstances.ContainsKey(type))
                {
                    Debug.LogError($"Ignoring window {windowPrefab.name} of type {type.Name}. Already added! Remove the duplicate from Resources/{windowsResourcesFolder}");
                    continue;
                }

                Window window = Instantiate(windowPrefab);
                window.name = windowPrefab.name;

                App.ReadyPromise.Done(a => 
                {
                    try
                    {
                        window.Init(this);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                        Debug.LogError($"Window <b>{window.name}</b> not initialized! Exception was thrown. Ignoring...");
                        Destroy(window.gameObject);
                    }
                });

                window.transform.SetParent(transform);
                window.transform.localScale = Vector3.one;
                window.RectTransform.anchoredPosition = windowPrefab.RectTransform.anchoredPosition;
                window.RectTransform.sizeDelta = windowPrefab.RectTransform.sizeDelta;
                window.SetHidden();
                windowInstances.Add(window.GetType(), window);
            }
            return Promise.Resolved();
        }

        public IEnumerable<IWindow> GetWindowStack()
        {
            foreach (var window in windowStack)
            {
                yield return window;
            }
        }

        public void HideTopFullScreenWindow()
        {
            actionQueue.Enqueue(() =>
            {
                windowBuffer.Clear();

                while (windowStack.Count > 0 && windowStack.Peek().IsFullScreen == false)
                {
                    windowBuffer.Add(windowStack.Pop());
                }

                if (windowStack.Count == 0)
                {
                    Hide(windowBuffer);
                    return;
                }

                Window topFullScreenWin = windowStack.Pop();
                windowBuffer.Add(topFullScreenWin);

                if (windowStack.Count == 0)
                {
                    Hide(windowBuffer);
                    return;
                }

                Hide(windowBuffer);

                windowBuffer.Clear();

                while (windowStack.Count > 0 && windowStack.Peek().IsFullScreen == false)
                {
                    windowBuffer.Add(windowStack.Pop());
                }

                if (windowStack.Count > 0)
                {
                    var win = windowStack.Pop();
                    windowBuffer.Add(win);
                }

                windowBuffer.Reverse();

                foreach (var win in windowBuffer)
                {
                    windowStack.Push(win);
                }

                Show(windowBuffer);
            });

            ProcessActionQueue();

        }

        public IWindow Show<T>() where T : IWindow
        {
            T win = GetWindow<T>();

            Show(win as Window);

            return win;
        }

        public IWindow Show<T, A>(A args) where T : IWindowWithArgs<A>
        {
            T win = GetWindow<T>();

            win.SetArgs(args);

            Show(win as Window);

            return win;
        }

        public void Show(Window window)
        {
            if(window == null)
            {
                Debug.LogError("Cannot show window! Argument is null");
                return;
            }
            if (window.IsFullScreen)
            {
                actionQueue.Enqueue(() =>
                {
                    ShowFullScreen(window);
                });
            }
            else
            {
                actionQueue.Enqueue(() =>
                {
                    ShowSingle(window);
                });
            }
            ProcessActionQueue();
        }

        public void HideAll()
        {
            actionQueue.Enqueue(() =>
            {
                windowBuffer.Clear();

                foreach (var win in windowInstances)
                {
                    if (win.Value.IsShown)
                    {
                        windowBuffer.Add(win.Value);
                    }
                }
                windowStack.Clear();

                Hide(windowBuffer);
            });

            ProcessActionQueue();
        }

        protected override IPromise GetLoadPromise()
        {
            return Promise.Resolved();
        }
        public void SetStack(IEnumerable<IWindow> windows)
        {
            actionQueue.Enqueue(() =>
            {
                windowStack.Clear();
                foreach (var win in windows)
                {
                    windowStack.Push(win as Window);
                }
                ShowCurrentStack();
            });

            ProcessActionQueue();
        }

        public void SetStack<W1>() where W1 : IWindow
        {
            actionQueue.Enqueue(() =>
            {
                windowStack.Clear();

                windowStack.Push(GetWindow<W1>() as Window);

                ShowCurrentStack();
            });

            ProcessActionQueue();
        }

        public void SetStack<W1, W2>()
            where W1 : IWindow
            where W2 : IWindow
        {
            actionQueue.Enqueue(() =>
            {
                windowStack.Clear();

                windowStack.Push(GetWindow<W1>() as Window);
                windowStack.Push(GetWindow<W2>() as Window);

                ShowCurrentStack();

            });

            ProcessActionQueue();
        }

        public void SetStack<W1, W2, W3>()
            where W1 : IWindow
            where W2 : IWindow
            where W3 : IWindow
        {
            actionQueue.Enqueue(() =>
            {
                windowStack.Clear();

                windowStack.Push(GetWindow<W1>() as Window);
                windowStack.Push(GetWindow<W2>() as Window);
                windowStack.Push(GetWindow<W3>() as Window);

                ShowCurrentStack();
            });

            ProcessActionQueue();
        }

        public void SetStack<W1, W2, W3, W4>()
            where W1 : IWindow
            where W2 : IWindow
            where W3 : IWindow
            where W4 : IWindow
        {
            actionQueue.Enqueue(() =>
            {
                windowStack.Clear();

                windowStack.Push(GetWindow<W1>() as Window);
                windowStack.Push(GetWindow<W2>() as Window);
                windowStack.Push(GetWindow<W3>() as Window);
                windowStack.Push(GetWindow<W4>() as Window);

                ShowCurrentStack();
            });

            ProcessActionQueue();
        }

        public IWindow Hide<T>() where T : IWindow
        {
            T win = GetWindow<T>();

            Hide(win as Window);

            return win;
        }

        public void Hide(Window window) 
        {
            if (window.IsFullScreen)
            {
                if (window.IsShown)
                {
                    HideTopFullScreenWindow();
                }
            }
            else
            {
                HidePopUp(window);
            }
        }

        private class Container
        {
            public RectTransform rectTransform;
            public CanvasGroup canvasGroup;

            public Container(string name, Transform parent)
            {
                var obj = new GameObject(name);
                rectTransform = obj.AddComponent<RectTransform>();
                canvasGroup = obj.AddComponent<CanvasGroup>();
                obj.transform.SetParent(parent);
                obj.transform.localScale = Vector3.one;
                obj.transform.localPosition = Vector3.zero;
            }
        }

        private void StartShowAnimation(IList<Window> windows)
        {
            showRoutine = StartCoroutine(AnimationRoutine(showContainer, true, windows));
        }

        private void StartHideAnimation(IList<Window> windows)
        {
            hideRoutine = StartCoroutine(AnimationRoutine(hideContainer, false, windows));
        }

        private void Show(IList<Window> windows)
        {
            StartShowAnimation(windows);
        }

        private void Hide(IList<Window> windows)
        {
            StartHideAnimation(windows);
        }

        private void HideSingle(Window window)
        {
            actionQueue.Enqueue(() => 
            {
                windowBuffer.Clear();
                windowBuffer.Add(window);
                Hide(windowBuffer);
            });
        }

        IEnumerator AnimationRoutine(Container container, bool show, IList<Window> windows)
        {
            if (windows.Count == 0)
            {
                yield break;
            }

            if (show)
            {
                animationShowWindowBuffer.Clear();
                animationShowWindowBuffer.AddRange(windows);
                windows = animationShowWindowBuffer;
            }
            else
            {
                animationHideWindowBuffer.Clear();
                animationHideWindowBuffer.AddRange(windows);
                windows = animationHideWindowBuffer;
            }

            container.rectTransform.localScale = Vector3.one;
            container.rectTransform.SetAsLastSibling();

            if (windows.Count == 1)
            {
                container.rectTransform.position = windows[0].transform.position;
            }
            else
            {
                container.rectTransform.localPosition = Vector3.zero;
            }

            foreach (var win in windows)
            {
                if (show)
                {
                    win.SetShown();
                    win.OnStartShowing();
                }
                else
                {
                    win.OnStartHiding();
                }
                win.RectTransform.SetParent(container.rectTransform, true);
                win.RectTransform.localScale = Vector3.one;
            }

            float t = 0f;


            while (t <= 1)
            {
                var s = animationScaleCurve.Evaluate(show ? t : 1 - t);
                var a = animationAlphaCurve.Evaluate(show ? t : 1 - t);
                container.rectTransform.localScale = Vector3.one * s;
                container.canvasGroup.alpha = a;
                t += Time.unscaledDeltaTime / animationDuration;
                yield return null;
            }

            foreach (var win in windows)
            {
                win.RectTransform.SetParent(transform);
                if (show)
                {
                    win.RectTransform.SetAsLastSibling();
                    win.RectTransform.localScale = Vector3.one;
                    win.OnShown();
                }
                else
                {
                    win.SetHidden();
                    win.OnHidden();
                }
            }

            if (show)
            {
                showRoutine = null;
            }
            else
            {
                hideRoutine = null;
            }
        }

        private void ProcessActionQueue()
        {
            if(IsShowingAnimation() == false && actionQueue.Count > 0)
            {
                actionQueue.Dequeue().Invoke();
            }
        }

        private void ShowSingle(Window window)
        {
            if (window.IsShown)
            {
                return;
            }
     
            windowBuffer.Clear();
            windowBuffer.Add(window);
            windowStack.Push(window);
            Show(windowBuffer);
        }

        public void OnBackClick(Window window)
        {
            if (window.IsFullScreen)
            {
                HideTopFullScreenWindow();
            }
            else
            {
                HidePopUp(window);
                ProcessActionQueue();
            }
        }

        private void ShowFullScreen(Window window)
        {
            if (window.IsShown)
            {
                return;
            }

            windowBuffer.Clear();

            while (windowStack.Count > 0 && windowStack.Peek().IsFullScreen == false)
            {
                windowBuffer.Add(windowStack.Pop());
            }

            if (windowStack.Count > 0)
            {
                windowBuffer.Add(windowStack.Peek());
            }

            Hide(windowBuffer);

            ShowSingle(window);
        }

        private T GetWindow<T>() where T : IWindow
        {
            Type type = typeof(T);
            if (windowInstances.ContainsKey(type) == false)
            {
                Debug.LogError($"Error trying to get window of type {type.Name}. Not found! Put window prefab with script {type.Name} attached to it inside Resources/{windowsResourcesFolder}");
                return default;
            }
            IWindow win = windowInstances[type];
            return (T) win;
        }

        private void HidePopUp(Window window)
        {
            actionQueue.Enqueue(() =>
            {
                if (window.IsShown == false)
                {
                    return;
                }
                windowBuffer.Clear();

                windowBuffer.AddRange(windowStack);

                windowShowBuffer.Clear();
                windowShowBuffer.Add(window);

                Hide(windowShowBuffer);

                windowBuffer.Remove(window);

                windowStack.Clear();

                windowBuffer.Reverse();

                foreach (var win in windowBuffer)
                {
                    windowStack.Push(win);
                }
            });
        }

        private void ShowCurrentStack()
        {
            if(windowStack.Count == 0)
            {
                return;
            }
            windowShowBuffer.Clear();
            windowBuffer.Clear();
            windowShowBuffer.AddRange(windowStack);

            for (int i = 0; i < windowShowBuffer.Count; i++)
            {
                var win = windowShowBuffer[i];
                windowBuffer.Add(win);
                if (win.IsFullScreen)
                {
                    break;
                }
            }

            windowBuffer.Reverse();

            Show(windowBuffer);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if(windowStack.Count > 0)
                {
                    if (windowStack.Peek().IsFullScreen)
                    {
                        if (windowStack.Peek().CanBeClosed)
                        {
                            HideTopFullScreenWindow();
                        }
                    }
                    else
                    {
                        if (windowStack.Peek().CanBeClosed)
                        {
                            HideSingle(windowStack.Pop());
                        }
                    }
                }
            }
            ProcessActionQueue();
        }

    }
}
