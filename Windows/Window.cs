﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using UnityEngine.UI;

namespace SwiftFramework.Windows
{
    [RequireComponent(typeof(RectTransform))]
    public abstract class Window : MonoBehaviour, IWindow
    {
        public IPromise HidePromise => hidePromise;

        public bool IsFullScreen => isFullScreen;

        public bool CanBeClosed => closeButton != null;

        public bool IsShown { get; private set; }

        public RectTransform RectTransform
        {
            get
            {
                if (rectTransform == null)
                {
                    rectTransform = GetComponent<RectTransform>();
                }
                return rectTransform;
            }
        }

        private RectTransform rectTransform = null;
        private WindowsManager windowsManager = null;
        private Promise hidePromise;
        private Vector3 defaultLocalPosition;

        [SerializeField] private bool isFullScreen = false;
        [SerializeField] private Button closeButton = null;

        public void Hide()
        {
            windowsManager.Hide(this);
        }

        public void Show()
        {
            hidePromise = Promise.Create();
            windowsManager.Show(this);
        }

        public virtual void Init(WindowsManager windowsManager)
        {
            defaultLocalPosition = RectTransform.localPosition;
            this.windowsManager = windowsManager;
            if (closeButton != null)
            {
                closeButton.onClick.AddListener(() => windowsManager.OnBackClick(this));
            }
        }

        protected virtual void OnValidate()
        {
#if UNITY_EDITOR
            if(closeButton == null && transform.Find("CloseButton"))
            {
                closeButton = transform.Find("CloseButton").GetComponent<Button>();
            }
#endif
        }

        public virtual void OnStartShowing()
        {
            
        }

        public virtual void OnShown()
        {
            
        }

        public virtual void OnStartHiding()
        {
            
        }

        public virtual void OnHidden()
        {
            
        }

        public void SetShown()
        {
            if (IsFullScreen == false)
            {
                RectTransform.SetAsLastSibling();
            }
            gameObject.SetActive(true);
            IsShown = true;
            RectTransform.localPosition = defaultLocalPosition;
        }

        public void SetHidden()
        {
            gameObject.SetActive(false);
            IsShown = false;
            if(hidePromise != null)
            {
                hidePromise.Resolve();
                hidePromise = null;
            }
        }
    }
}
